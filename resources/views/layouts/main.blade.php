<!doctype html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>{{ $title }}</title>
        <meta name="description" content="{{ $metadescription }}" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="_token" content="{!! csrf_token() !!}" />

        <meta property="og:locale" content="id_ID" />
        <meta property="og:type" content="article" />
        <meta property="og:title" content="{{ $title }}" />
        <meta property="og:url" content="{{ \Request::url() }}" />
        <meta property="og:image" content="{{ $metaimage }}" />
        <meta property="og:site_name" content="{{ getData::getCatalogUsername(myFunction::get_username(),'catalog_title') }}" />
        <meta property="og:description" content="{{ $metadescription }}" />

        <link rel="canonical" href="{{ \Request::url() }}" />

        <link rel="manifest" href="site.webmanifest">
        <!-- <link rel="apple-touch-icon" href="icon.png"> -->
        <!-- Place favicon.ico in the root directory -->

        <link href="https://fonts.googleapis.com/css?family=Poppins:100,200,300,400,500,600,700,800,900&display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css2?family=Dancing+Script:wght@400;500;600;700&display=swap" rel="stylesheet">

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.3/css/bootstrap.min.css" />
        <link rel="stylesheet" type="text/css" href="{{ asset('/css/font-awesome.css?'.time()) }}">
        <link rel="stylesheet" href="{{ asset('/css/templatemo-klassy-cafe.css?'.time()) }}">
        <link rel="stylesheet" href="{{ asset('/css/owl-carousel.css') }}">
        <link rel="stylesheet" href="{{ asset('/css/lightbox.css') }}">
        <link rel="stylesheet" href="{{ asset('/css/custom.css?'.time()) }}">
        <script src="{{ asset('/js/pusher.min.js') }}"></script>
        <script src="{{ asset('/js/echo.js') }}"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.5.13/vue.min.js"></script>
        <script src="https://app.sandbox.midtrans.com/snap/snap.js" data-client-key="{{ getData::getCatalogUsername(myFunction::get_username(),'client_key') }}"></script>
    </head>
    <body>
        <!-- ***** Preloader Start ***** -->
        <div id="preloader" style="background: {{ getData::getCatalogUsername(myFunction::get_username(),'theme_color') }}">
            <div class="jumper">
                <div></div>
                <div></div>
                <div></div>
            </div>
        </div>
        <div class="preloader">
            <div class="jumper">
                <div></div>
                <div></div>
                <div></div>
            </div>
        </div>
        <!-- ***** Preloader End ***** -->

        <!-- ***** Header Area Start ***** -->
        @include('blocks.header')
        <!-- ***** Header Area End ***** -->

        @yield('content')

        <!-- ***** Footer ***** -->
        @include('blocks.footer')
        <!-- ***** Footer Ends ***** -->

        <div class="floatbell" onclick="bellWaiters()">
            <div class="row">
                <div class="col-3">
                    <i class="fa fa-bell"></i>
                </div>
                <div class="col-9">
                    {{ __('lang.panggil') }} <br> {{ __('lang.petugas') }}
                </div>
            </div>
        </div>

        @include('pages.modals.cart')
        @include('pages.modals.cartcustom')
        @include('pages.modals.order')
        @include('pages.modals.bell')
        @include('pages.modals.accessmenu')
        @include('pages.modals.groupaddons')
        @include('pages.modals.addons')
        @include('pages.modals.customer')

        <!-- jQuery -->
        <script src="{{ asset('/js/jquery-2.1.0.min.js') }}"></script>

        <!-- Bootstrap -->
        <script src="{{ asset('/js/popper.js') }}"></script>
        <script src="{{ asset('/js/bootstrap.min.js') }}"></script>

        <!-- Plugins -->
        <script src="{{ asset('/js/owl-carousel.js') }}"></script>
        <script src="{{ asset('/js/accordions.js') }}"></script>
        <script src="{{ asset('/js/datepicker.js') }}"></script>
        <script src="{{ asset('/js/scrollreveal.min.js') }}"></script>
        <script src="{{ asset('/js/waypoints.min.js') }}"></script>
        <script src="{{ asset('/js/jquery.counterup.min.js') }}"></script>
        <script src="{{ asset('/js/imgfix.min.js') }}"></script>
        <script src="{{ asset('/js/slick.js') }}"></script>
        <script src="{{ asset('/js/lightbox.js') }}"></script>
        <script src="{{ asset('/js/isotope.js') }}"></script>

        <script src="https://cdn.jsdelivr.net/npm/bootstrap-input-spinner@1.9.7/src/bootstrap-input-spinner.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.18.0/axios.min.js"></script>
        <!-- Global Init -->
        <script src="{{ asset('/js/custom.js') }}"></script>
        <script>
            $.ajaxSetup({
                headers: {
                    'X-CSRF-Token': $('meta[name=_token]').attr('content')
                }
            });
            window.Echo = new Echo({
                broadcaster: 'pusher',
                key: "04c3b9d0c77c38d69025",
                cluster: "ap1",
                encrypted: true,
            });
            Echo.channel('pushernotif').listen('NotifEvent', function(e) {
                var myorder = "{{ Session::get('myorder') }}";
                $.ajax({
                    url: "{{ url('/cart/status') }}",
                    type: 'GET',
                })
                .done(function(data) {
                    if(data.invoice_number == e.invoice){
                        if(e.status == 'DeleteItem'){
                            showOrder();
                        }else{
                            countCart();
                        }
                    }
                });
            });
            $(function() {
                $("input[type='number']").inputSpinner();
                $('.single-item').slick({
                    infinite: true,
                    dots: false,
                    prevArrow: false,
                    nextArrow: false
                });
                $('#detail-item').slick({
                    infinite: true,
                    dots: false,
                    prevArrow: false,
                    nextArrow: false,
                    autoplay: true,
                    autoplaySpeed: 5000,
                    cssEase: 'linear'
                });
                //closeSession();
                
                replaceColor('#fb5849',"{{ getData::getCatalogUsername(myFunction::get_username(),'theme_color') }}")
            });
            $(window).on('load', function() {
                @if(Session::has('myorder'))
                    countCart();
                @endif
            });

            function closeSession(){
                $.ajax({
                    url: "{{ url('/close-session') }}",
                    type: 'GET',
                })
                .done(function() {
                    $("#qtyTemp").val(0)
                    window.location.reload(true);
                })
                .fail(function() {
                    console.log("error");
                });
            }

            //Get Current Location
            function getLocation() {
                if(navigator.geolocation) {
                    navigator.geolocation.getCurrentPosition(function(position) {
                        var latitude = position.coords.latitude;
                        var longitude = position.coords.longitude;
                        distance("{{ getData::getCatalogUsername(myFunction::get_username(),'lat') }}","{{ getData::getCatalogUsername(myFunction::get_username(),'long') }}",latitude,longitude);
                    });
                } else {
                    Swal.fire("Ops!", "Maaf browser anda tidak support HTML5 Geolocation. Anda dapat menggunakan browser lain atau memperbaharui ke versi terbaru.", "error");
                }
            }
            function distance(lat1, lon1, lat2, lon2, unit='K') {
                if ((lat1 == lat2) && (lon1 == lon2)) {
                    setLocation(lat2,lon2);
                }
                else {
                    var radlat1 = Math.PI * lat1/180;
                    var radlat2 = Math.PI * lat2/180;
                    var theta = lon1-lon2;
                    var radtheta = Math.PI * theta/180;
                    var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
                    if (dist > 1) {
                        dist = 1;
                    }
                    dist = Math.acos(dist);
                    dist = dist * 180/Math.PI;
                    dist = dist * 60 * 1.1515;
                    if (unit=="K") { dist = dist * 1.609344 }
                    if (unit=="N") { dist = dist * 0.8684 }
                    if(dist*1000 > "{{ getData::getCatalogUsername(myFunction::get_username(),'distance') }}"){
                        Swal.fire("Maaf!", "Lokasi anda diluar jangkauan kami, maksimal jarak adalah {{ getData::getCatalogUsername(myFunction::get_username(),'distance') }} meter.", "error");
                        return false;
                    }else{
                        setLocation(lat2,lon2);
                    }
                }
            }
            function setLocation(lat,lon){
                $.ajax({
                    url: "{{ url('/location') }}"+'/'+lat+'/'+lon,
                    type: 'GET',
                })
                .done(function() {
                    Swal.fire("Informasi", "Selamat datang di {{ getData::getCatalogUsername(myFunction::get_username(),'catalog_title') }}.Silahkan untuk memilih kembali menu yang tersedia.", "success").then((result) => {
                      if (result.value) {
                        window.location.reload(true);
                      }
                    });
                    
                    //$('.btn-transaction').show();
                })
                .fail(function() {
                    console.log("error");
                });
            }
            //End

            // Customer
            function setCustomer(param=null){
                $("#param_customer").val(param);
                $("#customer_email").val('');
                $("#customer_name").val('');
                $("#customer_phone").val('');
                $("#wrapemail").removeClass('d-none');
                $("#wrapname").addClass('d-none');
                $(".errormsg").hide();
                $("#customer_phone").prop('readonly', false);
                $("#btnCustomer").text('Selanjutnya');
                $("#myCustomer").modal('show');
            }
            // End

            //Bell
            function bellWaiters(){
                @if(!Session::get('location'))
                    @if(getData::getCatalogUsername(myFunction::get_username(),'customer_data') == 'Y')
                        setCustomer();
                    @else
                        getLocation();
                    @endif
                    return false;
                @endif
                $("#myBell").modal('show');
            }
            function bellTable(){
                if($("#tablebell").val() == ''){
                    Swal.fire("Ops!", "Mohon untuk memasukan nomor meja.", "error");
                    return false;
                }
                // if($("#messagebell").val() == ''){
                //     Swal.fire("Ops!", "Mohon untuk tulis pesan.", "error");
                //     return false;
                // }
                obj = new Object;
                obj.catalog = "{{ getData::getCatalogUsername(myFunction::get_username(),'id') }}";
                obj.position = $("#tablebell").val();
                obj.message = $("#messagebell").val();
                $.ajax({
                    url: "{{ url('/bell') }}",
                    type: 'POST',
                    data:obj
                })
                .done(function(data) {
                    Swal.fire("Informasi", "Anda telah memanggil petugas. Mohon untuk menunggu.", "warning");
                    $("#myBell").modal('hide');
                })
                .fail(function() {
                    console.log("error");
                });
            }
            //End Bell
            function replaceColor(color1, color2) {
                var keys = Object.values(window.getComputedStyle($('html').get(0)));
                var filteredKeys = keys.filter(function (key){return key.indexOf('color') > -1});
                var colors = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(color1).splice(1,3); 
                var rgb = 'rgb(' + colors.map(function (color){return parseInt(color, 16)}).join(', ') + ')';
                $("*").each(function (index, element) {
                    filteredKeys.forEach(function(key) {
                        if ($(element).css(key) == rgb) {
                            $(element).css(key, color2);
                        }
                    });
                });
            }
            
            function accessCatalog(){
                if($("#accesspassword").val() == ''){
                    Swal.fire("Ops!", "Masukan kata sandi.", "error");
                    return false;
                }
                obj = new Object;
                obj.password = $("#accesspassword").val();
                $.ajax({
                    url: "{{ url('/location') }}"+'/404/404',
                    type:'POST',
                    data:obj
                })
                .done(function(data) {
                    if(data == 1){
                        Swal.fire("Informasi", "Selamat datang di {{ getData::getCatalogUsername(myFunction::get_username(),'catalog_title') }}.Silahkan untuk memilih menu yang tersedia.", "success").then((result) => {
                          if (result.value) {
                            window.location.reload(true);
                          }
                        });
                    }else{
                        Swal.fire("Informasi", "Kata sandi tidak sesuai.", "error");
                    }
                })
                .fail(function() {
                    console.log("error");
                });
            }
        </script>

    </body>
</html>
