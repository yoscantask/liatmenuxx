<!doctype html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>{{ $title }}</title>
        <meta name="description" content="{{ $metadescription }}" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="_token" content="{!! csrf_token() !!}" />

        <meta property="og:locale" content="id_ID" />
        <meta property="og:type" content="article" />
        <meta property="og:title" content="{{ $title }}" />
        <meta property="og:url" content="{{ \Request::url() }}" />
        <meta property="og:image" content="{{ $metaimage }}" />
        <meta property="og:site_name" content="{{ getData::getCatalogUsername(myFunction::get_username(),'catalog_title') }}" />
        <meta property="og:description" content="{{ $metadescription }}" />

        <link rel="canonical" href="{{ \Request::url() }}" />

        <link rel="manifest" href="site.webmanifest">
        <!-- <link rel="apple-touch-icon" href="icon.png"> -->
        <!-- Place favicon.ico in the root directory -->

        <link href="https://fonts.googleapis.com/css?family=Poppins:100,200,300,400,500,600,700,800,900&display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css2?family=Dancing+Script:wght@400;500;600;700&display=swap" rel="stylesheet">

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.3/css/bootstrap.min.css" />
        <link rel="stylesheet" type="text/css" href="{{ asset('/css/font-awesome.css?'.time()) }}">
        <link rel="stylesheet" href="{{ asset('/css/templatemo-klassy-cafe.css?'.time()) }}">
        <link rel="stylesheet" href="{{ asset('/css/owl-carousel.css') }}">
        <link rel="stylesheet" href="{{ asset('/css/lightbox.css') }}">
        <link rel="stylesheet" href="{{ asset('/css/custom.css?'.time()) }}">
        <script src="{{ asset('/js/pusher.min.js') }}"></script>
        <script src="{{ asset('/js/echo.js') }}"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.5.13/vue.min.js"></script>
        <script src="https://app.sandbox.midtrans.com/snap/snap.js" data-client-key="{{ getData::getCatalogUsername(myFunction::get_username(),'client_key') }}"></script>
    </head>
    <body>
        <!-- ***** Preloader Start ***** -->
        <div id="preloader" style="background: {{ getData::getCatalogUsername(myFunction::get_username(),'theme_color') }}">
            <div class="jumper">
                <div></div>
                <div></div>
                <div></div>
            </div>
        </div>
        <!-- ***** Preloader End ***** -->

        <!-- ***** Header Area Start ***** -->
        <header class="header-area header-sticky">
            <div class="container">
                <div class="row">
                    <div class="col-12 headerwrap">
                        <a href="{{ url('/') }}" class="logo">
                            <img src="{{ asset('/images'.getData::getCatalogUsername(myFunction::get_username(),'catalog_logo')) }}" alt="{{ getData::getCatalogUsername(myFunction::get_username(),'catalog_title') }}">
                        </a>
                        @if(!Session::has('location'))
                            <div class="floattrack text-center" style="background: {{ getData::getCatalogUsername(myFunction::get_username(),'theme_color') }}">
                                <a href="javascript:void(0)" style="text-transform: uppercase;color:#EEE" data-toggle="modal" data-target="#modalAccess"><i class="fa fa-lock fa-lg mr-1"></i>  Akses Menu</a>
                            </div>
                        @else
                            <div id="wrapcart" class="floattrack text-center d-none">
                                <input type="hidden" id="qtyTemp">
                                <a href="javascript:void(0)" onclick="showCart()" id="cartStatus" style="color: #666"><i class="fa fa-shopping-cart fa-lg mr-2"></i> <span id="countcart"></span></a>
                                <a href="javascript:void(0)" onclick="showCart()" id="orderStatus" class="d-none" style="color: #666"></a>
                            </div>
                        @endif
                    </div>
                </div>
                <div id="demo"></div>
            </div>
        </header>
        <!-- ***** Header Area End ***** -->

        @yield('content')

        <!-- ***** Reservation Us Area Starts ***** -->
        <section class="section" id="reservation">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 offset-md-2 align-self-center">
                        <div class="text-center">
                            <div class="section-heading">
                                <h2>Hubungi Kami</h2>
                            </div>
                            <p>Silahkan hubungi kami melalui nomor telepon dan email dibawah ini, tim kami akan dengan senang hati untuk bisa memberikan respon secepatnya.</p>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="phone">
                                        <i class="fa fa-phone"></i>
                                        <h4>Phone Numbers</h4>
                                        <span>{{ getData::getCatalogUsername(myFunction::get_username(),'phone_contact') }}</span>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="message">
                                        <i class="fa fa-envelope"></i>
                                        <h4>Emails</h4>
                                        <span><a href="#">{{ getData::getCatalogUsername(myFunction::get_username(),'email_contact') }}</a></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- ***** Reservation Area Ends ***** -->

        <!-- ***** Footer Start ***** -->
        <footer style="margin-top: 0">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 col-xs-12">
                        <div class="right-text-content">
                                <ul class="social-icons">
                                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                    <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                                </ul>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="logo">

                            <a href="index.html">{{ getData::getCatalogUsername(myFunction::get_username(),'catalog_title') }}</a>
                        </div>
                    </div>
                    <div class="col-lg-4 col-xs-12">
                        <div class="left-text-content">
                            <p>© Copyright {{ getData::getCatalogUsername(myFunction::get_username(),'catalog_title') }}.
                            <br>Powered by : LiatAja</p>
                        </div>
                    </div>
                </div>
            </div>
        </footer>

        <form id="payment-form" method="post" action="{{ url('/payment/finish') }}">
            <input type="hidden" name="_token" value="{!! csrf_token() !!}">
            <input type="hidden" name="result_type" id="result-type" value="">
            <input type="hidden" name="result_data" id="result-data" value="">
            <input type="hidden" name="payment_method" id="payment_method" value="3">
            <input type="hidden" name="result_position" id="result-position">
            <input type="hidden" name="result_tax" id="result-tax">
        </form>

        <div class="floatbell" onclick="bellWaiters()">
            <div class="row">
                <div class="col-3">
                    <i class="fa fa-bell"></i>
                </div>
                <div class="col-9">
                    Panggil<br>Petugas
                </div>
            </div>
        </div>

        @include('pages.modal')

        <!-- jQuery -->
        <script src="{{ asset('/js/jquery-2.1.0.min.js') }}"></script>

        <!-- Bootstrap -->
        <script src="{{ asset('/js/popper.js') }}"></script>
        <script src="{{ asset('/js/bootstrap.min.js') }}"></script>

        <!-- Plugins -->
        <script src="{{ asset('/js/owl-carousel.js') }}"></script>
        <script src="{{ asset('/js/accordions.js') }}"></script>
        <script src="{{ asset('/js/datepicker.js') }}"></script>
        <script src="{{ asset('/js/scrollreveal.min.js') }}"></script>
        <script src="{{ asset('/js/waypoints.min.js') }}"></script>
        <script src="{{ asset('/js/jquery.counterup.min.js') }}"></script>
        <script src="{{ asset('/js/imgfix.min.js') }}"></script>
        <script src="{{ asset('/js/slick.js') }}"></script>
        <script src="{{ asset('/js/lightbox.js') }}"></script>
        <script src="{{ asset('/js/isotope.js') }}"></script>

        <script src="https://cdn.jsdelivr.net/npm/bootstrap-input-spinner@1.9.7/src/bootstrap-input-spinner.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.18.0/axios.min.js"></script>
        <!-- Global Init -->
        <script src="{{ asset('/js/custom.js') }}"></script>
        <script>
            $.ajaxSetup({
                headers: {
                    'X-CSRF-Token': $('meta[name=_token]').attr('content')
                }
            });
            window.Echo = new Echo({
                broadcaster: 'pusher',
                key: "a01ada789ab34d372572",
                cluster: "ap1",
                encrypted: true,
            });
            Echo.channel('pushernotif').listen('NotifEvent', function(e) {
                var myorder = "{{ Session::get('myorder') }}";
                if(e.invoice == myorder){
                    if(e.status == 'DeleteItem'){
                        showCart();
                    }else{
                        countCart();
                    }
                }
            });
            $(function() {
                $("input[type='number']").inputSpinner();
                $('.single-item').slick({
                    infinite: true,
                    dots: false,
                    prevArrow: false,
                    nextArrow: false
                });
                $('#detail-item').slick({
                    infinite: true,
                    dots: false,
                    prevArrow: false,
                    nextArrow: false,
                    autoplay: true,
                    autoplaySpeed: 5000,
                    cssEase: 'linear'
                });
                //closeSession();
                countCart();
                checkStatus();
                replaceColor('#fb5849',"{{ getData::getCatalogUsername(myFunction::get_username(),'theme_color') }}")
            });

            function directOrder(category,subcategory,item,slug){
                @if(!Session::get('location'))
                    Swal.fire({
                      title: "Pemberitahuan",
                      text: "{{ getData::getCatalogUsername(myFunction::get_username(),'catalog_title') }} ingin mengakses lokasi anda. Klik Ya untuk menyetujui.",
                      icon: 'warning',
                      showCancelButton: true,
                      confirmButtonColor: '#3085d6',
                      cancelButtonColor: '#d33',
                      confirmButtonText: "Ya",
                      cancelButtonText: "Tidak"
                    }).then((result) => {
                      if (result.value) {
                        getLocation();
                      }
                    })
                    return false;
                @endif

                @if(Session::has('myorder') && getData::getCatalogUsername(myFunction::get_username(),'advance_payment') == 'Y')
                    Swal.fire({
                      title: "Informasi",
                      text: "Anda akan membuat Invoice baru.",
                      icon: 'warning',
                      showCancelButton: true,
                      confirmButtonColor: '#3085d6',
                      cancelButtonColor: '#d33',
                      confirmButtonText: "Ya",
                      cancelButtonText: "Tidak"
                    }).then((result) => {
                      if (result.value) {
                        closeSession();
                      }
                    })
                    return false;
                @endif

                if(subcategory != 0){
                    var embeditem ="http://saktiyasin.com/{{ myFunction::get_username() }}"+'/menu/'+category+'/'+subcategory+'/'+slug;
                }else{
                    var embeditem ="http://saktiyasin.com/{{ myFunction::get_username() }}"+'/item/'+category+'/'+slug;
                }
                var text = "Hallo kak ! saya mau pesan "+item;
                @php
                    $wanumb = getData::getCatalogUsername(myFunction::get_username(),'wa_number');
                    $wanumb[0] = " ";
                @endphp
                window.open("https://api.whatsapp.com/send?phone=62{{ $wanumb }}&text="+embeditem+'%0a%0a'+text, '_blank');
            }

            function addCart(category,itemid,item,price,discount){
                @if(!Session::get('location'))
                    Swal.fire({
                      title: "Pemberitahuan",
                      text: "{{ getData::getCatalogUsername(myFunction::get_username(),'catalog_title') }} ingin mengakses lokasi anda. Klik Ya untuk menyetujui.",
                      icon: 'warning',
                      showCancelButton: true,
                      confirmButtonColor: '#3085d6',
                      cancelButtonColor: '#d33',
                      confirmButtonText: "Ya",
                      cancelButtonText: "Tidak"
                    }).then((result) => {
                      if (result.value) {
                        getLocation();
                      }
                    })
                    return false;
                @endif
                @if(Session::has('myorder') && getData::getCatalogUsername(myFunction::get_username(),'advance_payment') == 'Y')
                    Swal.fire({
                      title: "Informasi",
                      text: "Anda akan membuat Invoice baru.",
                      icon: 'warning',
                      showCancelButton: true,
                      confirmButtonColor: '#3085d6',
                      cancelButtonColor: '#d33',
                      confirmButtonText: "Ya",
                      cancelButtonText: "Tidak"
                    }).then((result) => {
                      if (result.value) {
                        closeSession();
                      }
                    })
                    return false;
                @endif
                $("#qty").val(1);
                $("#note").val('');
                $("#category").val('');
                $("#item_id").val('');
                $("#item").val('');
                $("#price").val('');
                $("#discount").val('');
                $("#myModal").modal('show');
                $("#category").val(category);
                $("#item_id").val(itemid);
                $("#item").val(item);
                $("#price").val(price);
                $("#discount").val(discount);
            }
            function saveCart(){
                obj = new Object;
                obj.catalog = "{{ getData::getCatalogUsername(myFunction::get_username(),'id') }}";
                obj.category = $("#category").val();
                obj.item_id = $("#item_id").val();
                obj.item = $("#item").val();
                obj.price = $("#price").val();
                obj.discount = $("#discount").val();
                obj.via = "{{ getData::getCatalogUsername(myFunction::get_username(),'checkout_type') }}";
                obj.qty = $("#qty").val();
                obj.note = $("#note").val();
                $.ajax({
                    url: "{{ url('/cart/save') }}",
                    type: 'POST',
                    data: obj
                })
                .done(function(data) {
                    countCart();
                    $("#myModal").modal('hide');
                })
                .fail(function() {
                    console.log("error");
                })
            }
            function countCart(){
                $.ajax({
                    url: "{{ url('/cart/count') }}",
                    type: 'GET',
                })
                .done(function(data) {
                    $("#qtyTemp").val(data);
                    if(data > 0){
                        $("#wrapcart").removeClass('d-none');
                        $("#countcart").html(data+" Item(s)");
                        $('.directorder').addClass('d-none');
                        $('.menudirectorder').addClass('d-none');
                        //$('.cartorder').removeClass('col-6');
                        $('.cartorder').addClass('col-12');
                    }else{
                        $('.directorder').removeClass('d-none');
                        $('.menudirectorder').removeClass('d-none');
                        $('.cartorder').removeClass('col-12');
                        //$('.cartorder').addClass('col-6');
                        $("#wrapcart").addClass('d-none');
                    }
                    checkStatus();
                })
                .fail(function() {
                    console.log("error");
                })
            }
            function showCart(){
                $.ajax({
                    url: "{{ url('/cart/status') }}",
                    type: 'GET',
                })
                .done(function(data) {
                    if(data.status == 'Completed'){
                        if("{{ getData::getCatalogUsername(myFunction::get_username(),'advance_payment') == 'N' }}"){
                            showLastPayment();
                        }
                    }else{
                        if(parseInt($("#qtyTemp").val()) > 0){
                            $("#myModalEdit").modal('hide');
                            $("#myCart").modal('show');
                            $("#cartList").html('Please wait...');
                            $.ajax({
                                url: "{{ url('/cart/list') }}",
                                type: 'GET',
                            })
                            .done(function(data) {
                                $("#cartList").html(data);
                            })
                            .fail(function() {
                                console.log("error");
                            })
                        }
                    }
                });
            }
            function checkout(){
                if($("#position").val() == ''){
                    Swal.fire("Ops!", "Mohon untuk memasukan nomor meja / nomor kamar.", "error");
                    return false;
                }
                var checkouttype = "{{ getData::getCatalogUsername(myFunction::get_username(),'checkout_type') }}";
                if(checkouttype == 'Whatsapp'){
                    var text = "Hallo kak ! saya mau pesan : %0a%0a Invoice : {{ Session::get('mycart') }} %0a%0a Table/Room : "+$("#position").val();
                    var embeditem = $("#wacontent").html();
                    @php
                        $wanumb = getData::getCatalogUsername(myFunction::get_username(),'wa_number');
                        $wanumb[0] = " ";
                    @endphp
                    window.open("https://wa.me/62{{ $wanumb }}?text="+text+'%0a%0a'+embeditem, '_blank');
                }
                obj = new Object;
                obj.position = $("#position").val();
                @if(getData::getCatalogUsername(myFunction::get_username(),'advance_payment') == 'Y')
                    obj.payment_method = 1;
                @else
                    obj.payment_method = '';
                @endif
                obj.tax = $("#tax").val();
                $.ajax({
                    url: "{{ url('/cart/checkout') }}",
                    type: 'POST',
                    data:obj
                })
                .done(function(data) {
                    $("#myCart").modal('hide');
                    $("#qtyTemp").val(0)
                    @if(getData::getCatalogUsername(myFunction::get_username(),'advance_payment') == 'Y')
                        Swal.fire("Informasi", "Informasi pemesanan anda ada pada pojok kanan atas.", "success").then((result) => {
                          if (result.value) {
                            window.location.reload();
                            countCart();
                          }
                        });
                    @else
                        window.location.reload();
                        countCart();
                    @endif
                })
                .fail(function() {
                    console.log("error");
                })
            }


            function editItem(id){
                $("#myCart").modal('hide');
                $("#id").val('');
                $("#qtyedit").val('');
                $("#noteedit").val('');
                $.ajax({
                    url: "{{ url('/cart/show-item') }}"+'/'+id,
                    type: 'GET',
                })
                .done(function(data) {
                    $("#titleModalEdit").html("Edit "+data.item)
                    $("#id").val(data.id);
                    $("#qtyedit").val(data.qty);
                    $("#noteedit").val(data.note);
                    $("#myModalEdit").modal('show');
                })
                .fail(function() {
                    console.log("error");
                })
            }
            function updateCart(){
                obj = new Object;
                obj.id = $("#id").val();
                obj.qty = $("#qtyedit").val();
                obj.note = $("#noteedit").val();
                $.ajax({
                    url: "{{ url('/cart/update') }}",
                    type: 'POST',
                    data: obj,
                })
                .done(function() {
                    $("#myModalEdit").modal('hide');
                    countCart();
                    showCart();
                })
                .fail(function() {
                    console.log("error");
                });
            }
            function removeItem(id){
                $.ajax({
                    url: "{{ url('/cart/delete-item') }}"+'/'+id,
                    type: 'GET',
                })
                .done(function(data) {
                    $("#myCart").modal('hide');
                    countCart();
                    showCart();
                })
                .fail(function() {
                    console.log("error");
                })
            }
            function cancelOrder(){
                $.ajax({
                    url: "{{ url('/cart/delete') }}",
                    type: 'GET',
                })
                .done(function(data) {
                    $("#myCart").modal('hide');
                    closeSession();
                })
                .fail(function() {
                    console.log("error");
                })
            }
            function checkStatus(){
                $.ajax({
                    url: "{{ url('/cart/status') }}",
                    type: 'GET',
                })
                .done(function(data) {
                    if(data.status == 'Order'){
                        $("#orderStatus").addClass('d-none');
                        $("#cartStatus").removeClass('d-none');
                    }else{
                        $("#cartStatus").addClass('d-none');
                        $("#orderStatus").removeClass('d-none');
                        if(data.status == 'Checkout'){
                            var status = "Mohon tunggu";
                        }
                        else if(data.status == 'Approve'){
                            var status = "Disetujui";
                        }
                        else if(data.status == 'Process'){
                            var status = "Dalam Proses";
                        }
                        else if(data.status == 'Delivered'){
                            var status = "Diantar";
                        }
                        else if(data.status == 'Completed'){
                            if("{{ getData::getCatalogUsername(myFunction::get_username(),'advance_payment') == 'Y' }}"){
                                var status = "Selesai";
                                Swal.fire({
                                    title: 'Informasi',
                                    text: "Terima kasih atas kunjungan anda di {{ getData::getCatalogUsername(myFunction::get_username(),'catalog_title') }}",
                                    icon: 'success',
                                }).then((result) => {
                                    if (result.value) {
                                        window.open("{{ url('/cart/struk/'.Session::get('myorder').'/print') }}", '_blank');
                                        closeSession();
                                    }
                                });
                            }else{
                                if("{{ getData::getInvoice('pending')=='N' }}"){
                                    alert("Ini")
                                    var status = "Selesai";
                                    Swal.fire({
                                        title: 'Informasi',
                                        text: "Terima kasih atas kunjungan anda di {{ getData::getCatalogUsername(myFunction::get_username(),'catalog_title') }}",
                                        icon: 'success',
                                    }).then((result) => {
                                        if (result.value) {
                                            window.open("{{ url('/cart/struk/'.Session::get('myorder').'/print') }}", '_blank');
                                            closeSession();
                                        }
                                    });
                                }else{
                                    alert("itu")
                                    var status = "Bayar Sekarang";
                                    showLastPayment();
                                }
                                
                            }
                        }
                        else if(data.status == 'Cancel'){
                            var status = "Batal [ "+data.note_order+" ]";
                            closeSession();
                        }
                        $("#orderStatus").html("Pesanan anda : "+data.invoice_number +" ( "+status+" )");
                    }
                })
                .fail(function() {
                    console.log("error");
                })
            }
            function closeSession(){
                if("{{ getData::getInvoice('pending')=='N' }}"){
                    alert("Close me")
                    $.ajax({
                        url: "{{ url('/close-session') }}",
                        type: 'GET',
                    })
                    .done(function() {
                        $("#qtyTemp").val(0)
                        window.location.reload();
                    })
                    .fail(function() {
                        console.log("error");
                    });
                }
            }

            //Get Current Location
            function getLocation() {
                if(navigator.geolocation) {
                    navigator.geolocation.getCurrentPosition(function(position) {
                        var latitude = position.coords.latitude;
                        var longitude = position.coords.longitude;
                        distance("{{ getData::getCatalogUsername(myFunction::get_username(),'lat') }}","{{ getData::getCatalogUsername(myFunction::get_username(),'long') }}",latitude,longitude);
                    });
                } else {
                    Swal.fire("Ops!", "Maaf browser anda tidak support HTML5 Geolocation. Anda dapat menggunakan browser lain atau memperbaharui ke versi terbaru.", "error");
                }
            }
            function distance(lat1, lon1, lat2, lon2, unit='K') {
                if ((lat1 == lat2) && (lon1 == lon2)) {
                    setLocation(lat2,lon2);
                }
                else {
                    var radlat1 = Math.PI * lat1/180;
                    var radlat2 = Math.PI * lat2/180;
                    var theta = lon1-lon2;
                    var radtheta = Math.PI * theta/180;
                    var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
                    if (dist > 1) {
                        dist = 1;
                    }
                    dist = Math.acos(dist);
                    dist = dist * 180/Math.PI;
                    dist = dist * 60 * 1.1515;
                    if (unit=="K") { dist = dist * 1.609344 }
                    if (unit=="N") { dist = dist * 0.8684 }
                    if(dist*1000 > "{{ getData::getCatalogUsername(myFunction::get_username(),'distance') }}"){
                        Swal.fire("Maaf!", "Lokasi anda diluar jangkauan kami, maksimal jarak adalah {{ getData::getCatalogUsername(myFunction::get_username(),'distance') }} meter.", "error");
                        return false;
                    }else{
                        setLocation(lat2,lon2);
                    }
                }
            }
            function setLocation(lat,lon){
                $.ajax({
                    url: "{{ url('/location') }}"+'/'+lat+'/'+lon,
                    type: 'GET',
                })
                .done(function() {
                    Swal.fire("Informasi", "Selamat datang di {{ getData::getCatalogUsername(myFunction::get_username(),'catalog_title') }}.Silahkan untuk memilih kembali menu yang tersedia.", "success").then((result) => {
                      if (result.value) {
                        window.location.reload();
                      }
                    });
                    
                    //$('.btn-transaction').show();
                })
                .fail(function() {
                    console.log("error");
                });
            }
            //End

            //Bell
            function bellWaiters(){
                @if(!Session::get('location'))
                    getLocation();
                    return false;
                @endif
                $("#myBell").modal('show');
            }
            function bellTable(){
                if($("#tablebell").val() == ''){
                    Swal.fire("Ops!", "Mohon untuk memasukan nomor meja / kamar.", "error");
                    return false;
                }
                obj = new Object;
                obj.catalog = "{{ getData::getCatalogUsername(myFunction::get_username(),'catalog_title') }}";
                obj.position = $("#tablebell").val();
                $.ajax({
                    url: "{{ url('/bell') }}",
                    type: 'POST',
                    data:obj
                })
                .done(function(data) {
                    Swal.fire("Informasi", "Anda telah memanggil petugas. Mohon untuk menunggu.", "warning");
                    $("#myBell").modal('hide');
                })
                .fail(function() {
                    console.log("error");
                });
            }
            //End Bell
            function replaceColor(color1, color2) {
                var keys = Object.values(window.getComputedStyle($('html').get(0)));
                var filteredKeys = keys.filter(function (key){return key.indexOf('color') > -1});
                var colors = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(color1).splice(1,3); 
                var rgb = 'rgb(' + colors.map(function (color){return parseInt(color, 16)}).join(', ') + ')';
                $("*").each(function (index, element) {
                    filteredKeys.forEach(function(key) {
                        if ($(element).css(key) == rgb) {
                            $(element).css(key, color2);
                        }
                    });
                });
            }
            function confirmation(position){
                $("#myModalEdit").modal('hide');
                $("#myCart").modal('show');
                $("#cartList").html('Please wait...');
                obj = new Object;
                obj.position = position;
                $.ajax({
                    url: "{{ url('/cart/confirmation') }}",
                    type: 'POST',
                    data:obj
                })
                .done(function(data) {
                    $("#cartList").html(data);
                })
                .fail(function() {
                    console.log("error");
                })
            }
            function paymentGateway(){
                $.ajax({
                  url: "{{ url('/payment/snap/') }}"+'/'+$("#grand").val(),
                  success: function(data) {
                    //location = data;
                    console.log('token = '+data);
                    var resultType = document.getElementById('result-type');
                    var resultData = document.getElementById('result-data');
                    function changeResult(type,data){
                      $("#result-type").val(type);
                      $("#result-data").val(JSON.stringify(data));
                      $("#result-position").val($("#position").val());
                      $("#result-tax").val($("#tax").val());
                    }
                    snap.pay(data, {
                      onSuccess: function(result){
                        changeResult('success', result);
                        console.log(result.status_message);
                        console.log(result);
                        window.open("{{ url('/cart/struk/'.Session::get('myorder').'/print') }}", '_blank');
                        $("#qtyTemp").val(0)
                        $("#payment-form").submit();
                      },
                      onPending: function(result){
                        changeResult('pending', result);
                        console.log(result.status_message);
                        $("#qtyTemp").val(0)
                        $("#payment-form").submit();
                      },
                      onError: function(result){
                        changeResult('error', result);
                        console.log(result.status_message);
                        $("#qtyTemp").val(0)
                        $("#payment-form").submit();
                      }
                    });
                  }
                });
            }
            function paymentMethod(){
                if($("#position").val() == ''){
                    Swal.fire("Ops!", "Mohon untuk memasukan nomor meja / nomor kamar.", "error");
                    return false;
                }
                payment = $("#paymentmethod").val();
                if(payment == 1){
                    checkout();
                }
                else if(payment == 2){
                    confirmation($("#position").val());
                }
                else if(payment == 3){
                    paymentGateway();
                }
            }
            function paymentAction(){
                payment = $("#paymentmethod").val();
                if(payment == 2){
                    $("#transferinfo").removeClass('d-none');
                }else{
                    $("#transferinfo").addClass('d-none');
                }
            }
            function showLastPayment(){
                $("#myModalEdit").modal('hide');
                $("#myCart").modal('show');
                $("#cartList").html('Please wait...');
                $.ajax({
                    url: "{{ url('/cart/payment') }}",
                    type: 'GET',
                })
                .done(function(data) {
                    $("#cartList").html(data);
                })
                .fail(function() {
                    console.log("error");
                })
            }
            function accessCatalog(){
                if($("#accesspassword").val() == ''){
                    Swal.fire("Ops!", "Masukan kata sandi.", "error");
                    return false;
                }
                obj = new Object;
                obj.password = $("#accesspassword").val();
                $.ajax({
                    url: "{{ url('/location') }}"+'/404/404',
                    type:'POST',
                    data:obj
                })
                .done(function(data) {
                    if(data == 1){
                        Swal.fire("Informasi", "Selamat datang di {{ getData::getCatalogUsername(myFunction::get_username(),'catalog_title') }}.Silahkan untuk memilih menu yang tersedia.", "success").then((result) => {
                          if (result.value) {
                            window.location.reload();
                          }
                        });
                    }else{
                        Swal.fire("Informasi", "Kata sandi tidak sesuai.", "error");
                    }
                })
                .fail(function() {
                    console.log("error");
                });
            }
        </script>

    </body>
</html>
