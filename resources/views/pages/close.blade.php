@extends('layouts.main')
@section('content')
<!-- ***** Main Banner Area Start ***** -->
<div id="top">{{app()->setLocale(Session::get('locale'))}}
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-4">
				<div class="left-content">
					<div class="inner-content">
						<h4>{{ $getProfile['catalog_title'] }}</h4>
						<h6>THE BEST EXPERIENCE</h6>
						<div class="main-white-button scroll-to-section">
							<a href="#chefs">{{ __('lang.lmk')}}</a>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-8">
				<div class="main-banner header-text">
					<div class="Modern-Slider">
						@foreach(json_decode($getProfile['sliders'],true) as $slider)
						<!-- Item -->
						<div class="item">
							<div class="img-fill">
								<img src="{{ asset('/images/'.getData::getSlider($slider,'sliders_image')) }}" alt="{{ getData::getSlider($slider,'sliders_title') }}" style="width: 100%;height: 100%">
							</div>
						</div>
						<!-- // Item -->
						@endforeach
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- ***** Main Banner Area End ***** -->


<section class="section" id="chefs">
	<div class="container">
    <div class="row">
      <div class="col-md-6 offset-md-3">
        <img src="{{ asset('/images/closed.jpg') }}" class="img-fluid" alt="{{ getData::getCatalogUsername(myFunction::get_username(),'catalog_title') }}" />
      </div>
    </div>
	</div>
</section>

@endsection
