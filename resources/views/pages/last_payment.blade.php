@php
$grand = 0;
$itemgroup= [];
@endphp
<!-- Modal body -->
<div class="modal-body">{{app()->setLocale(Session::get('locale'))}}
	@foreach($item as $item)
	<p><b>{{ $item['category'] }}</b></p>
	<div class="ml-2">
		<div class="table-responsive">
			<table style="width: 100%">
				@php
					$total = 0;
					$totaladdons = 0;
				@endphp
				@foreach(getData::getItemCart($item['category']) as $listitem)
					@php
						$price = ($listitem['price']-$listitem['discount']) * $listitem['qty'];
						$total = $total + $price;
						$itemgroup[]= $listitem['item'].' x '.$listitem['qty'];
					@endphp
				<tr>
					<td style="width: 50%" style="border-top: none" class="align-top">
						<p style="line-height: 35px;font-size: 12px">
							{{ $listitem['item'] }}
						</p>
					</td>
					<td style="width:20%;border-top: none" class="text-center align-top">
						<p style="line-height: 35px;font-size: 12px">
							x {{ $listitem['qty'] }}
						</p>
					</td>
					<td style="width:30%;border-top: none" class="text-right align-top">
						<p style="line-height: 35px;font-size: 12px">
							{{ number_format($price) }}
						</p>
					</td>
				</tr>
				@if(!empty($listitem['note']))
					<tr>
						<td style="width:100%;" class="align-top" colspan="3">
							<small class="text-muted" style="font-size: .7rem;font-weight: bold;">* {{ __('lang.notes')}} : {{ $listitem['note'] }}</small>
						</td>
					</tr>
				@endif
				@if(getData::getInvoiceAddons($listitem['id'])->count() > 0)
					@foreach(getData::getInvoiceAddons($listitem['id']) as $addondata)
						@php
							$priceaddons = $addondata['addon_qty']*getData::addonsPrice($addondata['single_addon'],$addondata['multiple_addon']);
							$totaladdons = $totaladdons+$priceaddons;
						@endphp
						<tr>
							<td style="width: 50%" style="border-top: none;" class="align-top">
								<p style="line-height: 35px;font-size: 12px;padding-left: 30px;color: #999">
									{{ getData::decodeAddons($addondata['single_addon']) }} 
									{{ (!empty($addondata['multiple_addon']) && !empty(getData::decodeAddons($addondata['single_addon'])))?'|':'' }} 
									{{ getData::decodeAddons($addondata['multiple_addon']) }}
								</p>
							</td>
							<td style="width:20%;border-top: none" class="text-center align-top">
								<p style="line-height: 35px;font-size: 12px;color: #999">
									x {{ $addondata['addon_qty'] }}
								</p>
							</td>
							<td style="width:30%;border-top: none" class="text-right align-top">
								<p style="line-height: 35px;font-size: 12px;color: #999">
									{{ number_format($priceaddons) }} 
								</p>
							</td>
						</tr>
					@endforeach
				@endif
				@if(($listitem['qty'] - getData::getInvoiceAddonsSum($listitem['id'])) > 0 && getData::getAddons($listitem['item_id'])->count() > 0)
					<tr>
						<td style="width: 50%" style="border-top: none;" class="align-top">
							<p style="line-height: 35px;font-size: 12px;padding-left: 30px;color: #999">
								No Add Ons
							</p>
						</td>
						<td style="width:20%;border-top: none" class="text-center align-top">
							<p style="line-height: 35px;font-size: 12px;color: #999">
								x {{ $listitem['qty'] - getData::getInvoiceAddonsSum($listitem['id']) }}
							</p>
						</td>
						<td style="width:30%;border-top: none" class="text-right align-top">
							<p style="line-height: 35px;font-size: 12px;color: #999">
								0
							</p>
						</td>
					</tr>
				@endif
				@endforeach
				@php
					$grand = $grand +  $total + $totaladdons;
				@endphp
			</table>
		</div>
	</div>
	@endforeach
	@php
		$gettax = ($grand*getData::getCatalogUsername(myFunction::get_username(),'tax')) /100;
	@endphp
	@if(getData::getCatalogUsername(myFunction::get_username(),'tax') > 0)
	<table style="width: 100%" class="mt-3">
		<tr>
			<td style="width: 50%" style="font-size: 12px;border-top: 1px dashed #CCC;border-bottom: 1px dashed #CCC">
				<p style="line-height: 35px;font-size: 12px">
					( Extra ) PPN {{ getData::getCatalogUsername(myFunction::get_username(),'tax') }}%
				</p>
			</td>
			<td style="width: 50%" style="font-size: 12px;border-top: 1px dashed #CCC;border-bottom: 1px dashed #CCC" class="text-right">
				<p style="line-height: 35px;font-size: 12px">
					{{ number_format($gettax) }}
				</p>
			</td>
		</tr>
	</table>
	@endif
	<input type="hidden" id="tax" value="{{ getData::getCatalogUsername(myFunction::get_username(),'tax') }}">
	<input type="hidden" id="grand" value="{{ $grand }}">
	<table style="width: 100%" class="mt-3">
		<tr>
			<td class="text-right" style="font-size: 12px;border-top: 1px dashed #CCC;border-bottom: 1px dashed #CCC">
				<p style="padding: 15px 0;">
					<b>{{ number_format($grand+$gettax) }}</b>
				</p>
			</td>
		</tr>
	</table>

	
	<div class="form-group mt-3">
		<input type="hidden" id="paymentmethod" name="paymentmethod" value="0">
		<div class="row">
			<div class="col">
				<label>No.Meja</label>
				<input type="text" id="position" name="position" class="form-control" value="{{ $invoice['position'] }}" disabled>
			</div>
		</div>
	</div>

	<div id="transferinfo" style="width: 100%;position: relative;background: #F5F5F5;border: 1px solid #DDD;padding: 10px;" class="d-none">
		<p><b>{{ __('lang.information')}}</b></p>
		<p style="font-size: .7rem">{{ __('lang.ucanpaytransfer')}}<b>{!! getData::getCatalogUsername(myFunction::get_username(),'bank_info') !!}</b>.</p>
	</div>
	<span id="wacontent" style="visibility: hidden;font-size: 0.1px">
		@foreach($itemgroup as $itemselect)
		{{ $itemselect }}%0a
		@endforeach
	</span>
</div>

<!-- Modal footer -->
<div class="modal-footer">
	<button type="button" class="btn btn-primary btn-sm" onclick="paymentMethod()">{{ __('lang.paynow')}}</button>
	<button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">{{ __('lang.payletter')}}</button>
</div>