@extends('layouts.main')

@section('content')
{{app()->setLocale(Session::get('locale'))}}
<section class="section" id="chefs">
	<div class="parallax">
		<div class="container">
			<div class="row pt-5">
				<div class="col-lg-12 text-center">
					<div class="section-heading">
						<h6 style="color: #FFF">{{ $category['category_name'] }}</h6>
						<h2 style="color: #FFF">{{ $subcategory['subcategory_name'] }}</h2>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container" style="margin-top: 120px">

		<div class="row mb-3">
			<div class="col-lg-2">
				<a href="{{ url('/') }}" class="btn mybutton"><i class="fa fa-mail-reply mr-2"></i> Back</a>
			</div>
		</div>

	@if ($getProfile['view_item'] == 'grid')
		<div class="large-screen">
			<div class="row">
				@foreach($detail as $kdetail => $item)
				@if(getData::getCatalogUsername(myFunction::get_username(),'layout') == 'Column')
				<div class="col-lg-3 col-md-6 mb-4">
					<div class="chef-item">
						<a href="{{ url('/menu/'.$category['category_slug'].'/'.$subcategory['subcategory_slug'].'/'.$item['items_slug']) }}">
							<div class="thumb">
								<div class="single-item">
									<img src="{{ asset('/images/'.$item['item_image_primary']) }}" alt="{{ $item['items_name'] }}">
									@if(!empty($item['item_image_one']) and $item['item_image_one'] != $item['item_image_primary'])
									<img src="{{ asset('/images/'.$item['item_image_one']) }}" alt="{{ $item['items_name'] }}">
									@endif
									@if(!empty($item['item_image_two']) and $item['item_image_two'] != $item['item_image_primary'])
									<img src="{{ asset('/images/'.$item['item_image_two']) }}" alt="{{ $item['items_name'] }}">
									@endif
									@if(!empty($item['item_image_three']) and $item['item_image_three'] != $item['item_image_primary'])
									<img src="{{ asset('/images/'.$item['item_image_three']) }}" alt="{{ $item['items_name'] }}">
									@endif
									@if(!empty($item['item_image_four']) and $item['item_image_four'] != $item['item_image_primary'])
									<img src="{{ asset('/images/'.$item['item_image_four']) }}" alt="{{ $item['items_name'] }}">
									@endif
								</div>
							</div>
						</a>
						<div class="down-content">
							<a href="{{ url('/menu/'.$category['category_slug'].'/'.$subcategory['subcategory_slug'].'/'.$item['items_slug']) }}">
								<h4 style="font-size: 1rem">{{ $item['items_name'] }}</h4>
								<div class="mt-3 mb-3">
									<h5 style="font-size: 1rem" class="text-muted">
										<sup>Rp. </sup>
										@if($item['items_discount'] > 0)
										<span class="text-danger" style="text-decoration: line-through;">{{ number_format($item['items_price']) }}</span>
										{{ number_format($item['items_price']-$item['items_discount']) }}
										@else
										{{ number_format($item['items_price']) }}
										@endif
									</h5>
								</div>
							</a>
							<div class="row mt-3">
								@if($item['ready_stock']=='Y')
									@if(getData::getCatalogUsername(myFunction::get_username(),'feature') == 'Full')
										<div class="col-6 directorder_hide" style="display: none">
											<a href="javascript:void(0)" class="btn greenbutton btn-xs btn-transaction" onclick="directOrder('{{ $category['category_slug'] }}','{{ $subcategory['subcategory_slug'] }}','{{ $item['items_name'] }}','{{ $item['items_slug'] }}')" style="font-size: 11px;"><i class="fa fa-whatsapp mr-1"></i>{{ __('lang.order')}}</a>
										</div>
										<div class="col cartorder">
											@if((getData::getAddons($item['id'])->count() > 0))
												<a href="javascript:void(0)" class="btn {{ (Session::get('location'))?'darkbutton':'greybutton' }} btn-xs btn-transaction" onclick="addCartCustom('{{ $category['category_name'] }}','{{ $item['id'] }}','{{ $item['items_name'] }}','{{ $item['items_price'] }}','{{ $item['items_discount'] }}')" style="font-size: 11px;"><i class="fa fa-random mr-1"></i> {{ __('lang.order')}}
												</a>
											@else
												<a href="javascript:void(0)" class="btn {{ (Session::get('location'))?'darkbutton':'greybutton' }} btn-xs btn-transaction" onclick="addCart('{{ $category['category_name'] }}','{{ $item['id'] }}','{{ $item['items_name'] }}','{{ $item['items_price'] }}','{{ $item['items_discount'] }}')" style="font-size: 11px;"><i class="fa fa-shopping-cart mr-1"></i> 
													{{ __('lang.order')}}
												</a>
											@endif
										</div>
									@else
										<div class="col-12">
											<a href="javascript:void(0)" class="btn darkbutton btn-xs btn-transaction" style="font-size: 11px;">Item tersedia</a>
										</div>
									@endif
								@else
								<div class="col-12">
									<a href="javascript:void(0)" class="btn btn-danger btn-xs btn-transaction" style="font-size: 11px;">Item tidak tersedia</a>
								</div>
								@endif
							</div>
						</div>
					</div>
				</div>
				@else
				<div class="col-lg-12 mb-4">
					<div class="row">
						<div class="col-lg-10 col-sm-9">
							<a href="{{ url('/menu/'.$category['category_slug'].'/'.$subcategory['subcategory_slug'].'/'.$item['items_slug']) }}">
								<img src="{{ asset('/images/'.$item['item_image_primary']) }}" alt="{{ $item['items_name'] }}" width="70" align="left" class="mr-2 rounded  d-none d-sm-block">
								<img src="{{ asset('/images/'.$item['item_image_primary']) }}" alt="{{ $item['items_name'] }}" width="100" align="left" class="mr-2 rounded d-block d-sm-none">
								<p class="item dots">
									<span>{{ $item['items_name'] }} <br>
										<small class="text-muted d-none d-sm-block">{!! strip_tags(\Illuminate\Support\Str::limit($item['items_description'],130, $end = '...')) !!}</small>
										<small class="d-block d-sm-none">
											{!! strip_tags(\Illuminate\Support\Str::limit($item['items_description'],130, $end = '...')) !!}<br>
											<span style="font-weight: bold;">
												@if($item['items_discount'] > 0)
												<span class="text-danger" style="text-decoration: line-through;">{{ number_format($item['items_price']) }}</span>
												{{ number_format($item['items_price']-$item['items_discount']) }}
												@else
												{{ number_format($item['items_price']) }}
												@endif
											</span>
										</small>
									</span>
								</p>
								<div style="clear: both;"></div>
							</a>
							<div class="row mt-3">
								<div class="col-md-2 col-6 menudirectorder">
									<a href="javascript:void(0)" class="btn greenbutton btn-xs btn-transaction" onclick="directOrder('{{ $category['category_slug'] }}','{{ $subcategory['subcategory_slug'] }}','{{ $item['items_name'] }}','{{ $item['items_slug'] }}')"><i class="fa fa-whatsapp"></i></a>
								</div>
								<div class="col-md-2 col-6 menucartorder">
									<a href="javascript:void(0)" class="btn darkbutton btn-xs btn-transaction" onclick="addCart('{{ $category['category_name'] }}','{{ $item['items_name'] }}','{{ $item['items_price'] }}','{{ $item['items_discount'] }}')"><i class="fa fa-shopping-cart"></i></a>
								</div>
							</div>
						</div>
						<div class="col-lg-2 col-sm-3">
							<p class="item d-none d-sm-block text-right">
								@if($item['items_discount'] > 0)
								<span class="text-danger" style="text-decoration: line-through;">{{ number_format($item['items_price']) }}</span>
								{{ number_format($item['items_price']-$item['items_discount']) }}
								@else
								{{ number_format($item['items_price']) }}
								@endif
							</p>
						</div>
					</div>
				</div>
				@endif
				@endforeach
			</div>
		</div>

		<div class="small-screen">
			<<div class="row" style="
			height: 290px;
			overflow-x: auto;
			-webkit-overflow-scrolling: touch;
			display: grid;
			grid-auto-flow: column;
			">
				@foreach($detail as $kdetail => $item)
				@if(getData::getCatalogUsername(myFunction::get_username(),'layout') == 'Column')
				<div class="mr-1" style="width: 130px;">
					<div class="chef-item mb-1">
						<a href="{{ url('/menu/'.$category['category_slug'].'/'.$subcategory['subcategory_slug'].'/'.$item['items_slug']) }}">
							<div class="thumb">
								<div class="single-item">
									<img src="{{ asset('/images/'.$item['item_image_primary']) }}" alt="{{ $item['items_name'] }}">
									@if(!empty($item['item_image_one']) and $item['item_image_one'] != $item['item_image_primary'])
									<img src="{{ asset('/images/'.$item['item_image_one']) }}" alt="{{ $item['items_name'] }}">
									@endif
									@if(!empty($item['item_image_two']) and $item['item_image_two'] != $item['item_image_primary'])
									<img src="{{ asset('/images/'.$item['item_image_two']) }}" alt="{{ $item['items_name'] }}">
									@endif
									@if(!empty($item['item_image_three']) and $item['item_image_three'] != $item['item_image_primary'])
									<img src="{{ asset('/images/'.$item['item_image_three']) }}" alt="{{ $item['items_name'] }}">
									@endif
									@if(!empty($item['item_image_four']) and $item['item_image_four'] != $item['item_image_primary'])
									<img src="{{ asset('/images/'.$item['item_image_four']) }}" alt="{{ $item['items_name'] }}">
									@endif
								</div>
							</div>
						</a>
						<div class="down-content">
							<a href="{{ url('/menu/'.$category['category_slug'].'/'.$subcategory['subcategory_slug'].'/'.$item['items_slug']) }}">
								<h4 style="font-size: 12px; height: 35px">{{ $item['items_name'] }}</h4>
								<div class="mb-1">
									<h5 style="font-size: 10px" class="text-muted">
										<sup>Rp. </sup>
										@if($item['items_discount'] > 0)
										<span class="text-danger" style="text-decoration: line-through;">{{ number_format($item['items_price']) }}</span>
										{{ number_format($item['items_price']-$item['items_discount']) }}
										@else
										{{ number_format($item['items_price']) }}
										@endif
									</h5>
								</div>
							</a>
							<div class="row mt-3">
								@if($item['ready_stock']=='Y')
									@if(getData::getCatalogUsername(myFunction::get_username(),'feature') == 'Full')
										<div class="col-6 directorder_hide" style="display: none">
											<a href="javascript:void(0)" class="btn greenbutton btn-xs btn-transaction" onclick="directOrder('{{ $category['category_slug'] }}','{{ $subcategory['subcategory_slug'] }}','{{ $item['items_name'] }}','{{ $item['items_slug'] }}')" style="font-size: 11px;"><i class="fa fa-whatsapp mr-1"></i> {{ __('lang.order')}}</a>
										</div>
										<div class="col cartorder">
											@if((getData::getAddons($item['id'])->count() > 0))
												<a href="javascript:void(0)" class="btn {{ (Session::get('location'))?'darkbutton':'greybutton' }} btn-xs btn-transaction" onclick="addCartCustom('{{ $category['category_name'] }}','{{ $item['id'] }}','{{ $item['items_name'] }}','{{ $item['items_price'] }}','{{ $item['items_discount'] }}')" style="font-size: 11px;"><i class="fa fa-random mr-1"></i> {{ __('lang.order')}}
												</a>
											@else
												<a href="javascript:void(0)" class="btn {{ (Session::get('location'))?'darkbutton':'greybutton' }} btn-xs btn-transaction" onclick="addCart('{{ $category['category_name'] }}','{{ $item['id'] }}','{{ $item['items_name'] }}','{{ $item['items_price'] }}','{{ $item['items_discount'] }}')" style="font-size: 11px;"><i class="fa fa-shopping-cart mr-1"></i> 
													{{ __('lang.order')}}
												</a>
											@endif
										</div>
									@else
										<div class="col-12">
											<a href="javascript:void(0)" class="btn darkbutton btn-xs btn-transaction" style="font-size: 11px;">Item tersedia</a>
										</div>
									@endif
								@else
								<div class="col-12">
									<a href="javascript:void(0)" class="btn btn-danger btn-xs btn-transaction" style="font-size: 11px;">Item tidak tersedia</a>
								</div>
								@endif
							</div>
						</div>
					</div>
				</div>
				@else
				<div class="col-lg-12 mb-4">
					<div class="row">
						<div class="col-lg-10 col-sm-9">
							<a href="{{ url('/menu/'.$category['category_slug'].'/'.$subcategory['subcategory_slug'].'/'.$item['items_slug']) }}">
								<img src="{{ asset('/images/'.$item['item_image_primary']) }}" alt="{{ $item['items_name'] }}" width="70" align="left" class="mr-2 rounded  d-none d-sm-block">
								<img src="{{ asset('/images/'.$item['item_image_primary']) }}" alt="{{ $item['items_name'] }}" width="100" align="left" class="mr-2 rounded d-block d-sm-none">
								<p class="item dots">
									<span>{{ $item['items_name'] }} <br>
										<small class="text-muted d-none d-sm-block">{!! strip_tags(\Illuminate\Support\Str::limit($item['items_description'],130, $end = '...')) !!}</small>
										<small class="d-block d-sm-none">
											{!! strip_tags(\Illuminate\Support\Str::limit($item['items_description'],130, $end = '...')) !!}<br>
											<span style="font-weight: bold;">
												@if($item['items_discount'] > 0)
												<span class="text-danger" style="text-decoration: line-through;">{{ number_format($item['items_price']) }}</span>
												{{ number_format($item['items_price']-$item['items_discount']) }}
												@else
												{{ number_format($item['items_price']) }}
												@endif
											</span>
										</small>
									</span>
								</p>
								<div style="clear: both;"></div>
							</a>
							<div class="row mt-3">
								<div class="col-md-2 col-6 menudirectorder">
									<a href="javascript:void(0)" class="btn greenbutton btn-xs btn-transaction" onclick="directOrder('{{ $category['category_slug'] }}','{{ $subcategory['subcategory_slug'] }}','{{ $item['items_name'] }}','{{ $item['items_slug'] }}')"><i class="fa fa-whatsapp"></i></a>
								</div>
								<div class="col-md-2 col-6 menucartorder">
									<a href="javascript:void(0)" class="btn darkbutton btn-xs btn-transaction" onclick="addCart('{{ $category['category_name'] }}','{{ $item['items_name'] }}','{{ $item['items_price'] }}','{{ $item['items_discount'] }}')"><i class="fa fa-shopping-cart"></i></a>
								</div>
							</div>
						</div>
						<div class="col-lg-2 col-sm-3">
							<p class="item d-none d-sm-block text-right">
								@if($item['items_discount'] > 0)
								<span class="text-danger" style="text-decoration: line-through;">{{ number_format($item['items_price']) }}</span>
								{{ number_format($item['items_price']-$item['items_discount']) }}
								@else
								{{ number_format($item['items_price']) }}
								@endif
							</p>
						</div>
					</div>
				</div>
				@endif
				@endforeach
			</div>
		</div>
	@endif

	@if ($getProfile['view_item'] == 'list')
		<div class="row mb-5">
		@foreach($detail as $kdetail => $item)
				@if(getData::getCatalogUsername(myFunction::get_username(),'layout') == 'Column')
				<div class="col-lg-6 col-md-10 mb-4">
					<div class="col chef-item" style="margin: -1px">
						<div class="row mb-1">
							<a href="{{ url('/menu/'.$category['category_slug'].'/'.$subcategory['subcategory_slug'].'/'.$item['items_slug']) }}">
								<div class="col thumb m-1" style="max-width: 135px">
									<div class="single-item">
										<img src="{{ asset('/images/'.$item['item_image_primary']) }}" alt="{{ $item['items_name'] }}">
										@if(!empty($item['item_image_one']) and $item['item_image_one'] != $item['item_image_primary'])
										<img src="{{ asset('/images/'.$item['item_image_one']) }}" alt="{{ $item['items_name'] }}">
										@endif
										@if(!empty($item['item_image_two']) and $item['item_image_two'] != $item['item_image_primary'])
										<img src="{{ asset('/images/'.$item['item_image_two']) }}" alt="{{ $item['items_name'] }}">
										@endif
										@if(!empty($item['item_image_three']) and $item['item_image_three'] != $item['item_image_primary'])
										<img src="{{ asset('/images/'.$item['item_image_three']) }}" alt="{{ $item['items_name'] }}">
										@endif
										@if(!empty($item['item_image_four']) and $item['item_image_four'] != $item['item_image_primary'])
										<img src="{{ asset('/images/'.$item['item_image_four']) }}" alt="{{ $item['items_name'] }}">
										@endif
									</div>
								</div>
							</a>
							<div class="col mt-2">
								<h5 class="mb-3">{{ $item['items_name'] }}</h4>
								<h6 class="text-muted mb-1">
									<sup>Rp. </sup>
									@if($item['items_discount'] > 0)
									<span class="text-danger" style="text-decoration: line-through; font-size: 11px;">
										{{ number_format($item['items_price']) }}</span>  
										{{ number_format($item['items_price']-$item['items_discount']) }}
									@else
										{{ number_format($item['items_price']) }}
									@endif
								</h5>
								
								@if($item['ready_stock']=='Y')
									@if(getData::getCatalogUsername(myFunction::get_username(),'feature') == 'Full')
										<div class="col-6 directorder_hide" style="display: none">
											<a href="javascript:void(0)" class="btn greenbutton btn-xs btn-transaction" onclick="directOrder('{{ $category['category_slug'] }}','{{ $subcategory['subcategory_slug'] }}','{{ $item['items_name'] }}','{{ $item['items_slug'] }}')" style="font-size: 11px;"><i class="fa fa-whatsapp mr-1"></i> {{ __('lang.order')}}</a>
										</div>
										<div class="col cartorder">
											@if((getData::getAddons($item['id'])->count() > 0))
												<a href="javascript:void(0)" class="btn {{ (Session::get('location'))?'darkbutton':'greybutton' }} btn-xs btn-transaction" onclick="addCartCustom('{{ $category['category_name'] }}','{{ $item['id'] }}','{{ $item['items_name'] }}','{{ $item['items_price'] }}','{{ $item['items_discount'] }}')" style="font-size: 11px;"><i class="fa fa-random mr-1"></i> {{ __('lang.order')}}
												</a>
											@else
												<a href="javascript:void(0)" class="btn {{ (Session::get('location'))?'darkbutton':'greybutton' }} btn-xs btn-transaction" onclick="addCart('{{ $category['category_name'] }}','{{ $item['id'] }}','{{ $item['items_name'] }}','{{ $item['items_price'] }}','{{ $item['items_discount'] }}')" style="font-size: 11px;"><i class="fa fa-shopping-cart mr-1"></i> 
													{{ __('lang.order')}}
												</a>
											@endif
										</div>
									@else
										<div class="col-12">
											<a href="javascript:void(0)" class="btn darkbutton btn-xs btn-transaction" style="font-size: 11px;">Item tersedia</a>
										</div>
									@endif
								@else
								<div class="col-12">
									<a href="javascript:void(0)" class="btn btn-danger btn-xs btn-transaction" style="font-size: 11px;">Item tidak tersedia</a>
								</div>
								@endif
							
							</div>
						</div>
					</div>
				</div>
				@else
				<div class="col-lg-12 mb-4">
					<div class="row">
						<div class="col-lg-10 col-sm-9">
							<a href="{{ url('/menu/'.$category['category_slug'].'/'.$subcategory['subcategory_slug'].'/'.$item['items_slug']) }}">
								<img src="{{ asset('/images/'.$item['item_image_primary']) }}" alt="{{ $item['items_name'] }}" width="70" align="left" class="mr-2 rounded  d-none d-sm-block">
								<img src="{{ asset('/images/'.$item['item_image_primary']) }}" alt="{{ $item['items_name'] }}" width="100" align="left" class="mr-2 rounded d-block d-sm-none">
								<p class="item dots">
									<span>{{ $item['items_name'] }} <br>
										<small class="text-muted d-none d-sm-block">{!! strip_tags(\Illuminate\Support\Str::limit($item['items_description'],130, $end = '...')) !!}</small>
										<small class="d-block d-sm-none">
											{!! strip_tags(\Illuminate\Support\Str::limit($item['items_description'],130, $end = '...')) !!}<br>
											<span style="font-weight: bold;">
												@if($item['items_discount'] > 0)
												<span class="text-danger" style="text-decoration: line-through;">{{ number_format($item['items_price']) }}</span>
												{{ number_format($item['items_price']-$item['items_discount']) }}
												@else
												{{ number_format($item['items_price']) }}
												@endif
											</span>
										</small>
									</span>
								</p>
								<div style="clear: both;"></div>
							</a>
							<div class="row mt-3">
								<div class="col-md-2 col-6 menudirectorder">
									<a href="javascript:void(0)" class="btn greenbutton btn-xs btn-transaction" onclick="directOrder('{{ $category['category_slug'] }}','{{ $subcategory['subcategory_slug'] }}','{{ $item['items_name'] }}','{{ $item['items_slug'] }}')"><i class="fa fa-whatsapp"></i></a>
								</div>
								<div class="col-md-2 col-6 menucartorder">
									<a href="javascript:void(0)" class="btn darkbutton btn-xs btn-transaction" onclick="addCart('{{ $category['category_name'] }}','{{ $item['items_name'] }}','{{ $item['items_price'] }}','{{ $item['items_discount'] }}')"><i class="fa fa-shopping-cart"></i></a>
								</div>
							</div>
						</div>
						<div class="col-lg-2 col-sm-3">
							<p class="item d-none d-sm-block text-right">
								@if($item['items_discount'] > 0)
								<span class="text-danger" style="text-decoration: line-through;">{{ number_format($item['items_price']) }}</span>
								{{ number_format($item['items_price']-$item['items_discount']) }}
								@else
								{{ number_format($item['items_price']) }}
								@endif
							</p>
						</div>
					</div>
				</div>
				@endif
				@endforeach
		</div>
	@endif
	</div>
</section>

<style>
	.large-screen {}
		@media only screen and (max-width: 700px) {
		.large-screen {display: none;}
		}
		@media only screen and (min-width: 700px) {
		.large-screen {display: block;}
		}
	
	.small-screen {}
		@media only screen and (max-width: 700px) {
		.small-screen {display: block;}
		}
		@media only screen and (min-width: 700px) {
		.small-screen {display: none;}
		}
</style>
@endsection
