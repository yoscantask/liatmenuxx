<div id="wrapModalCustomer">
    {{app()->setLocale(Session::get('locale'))}}
    <div class="modal" id="myCustomer">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header">
                    <b class="modal-title">{{ __('lang.guestbook')}}</b>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <form id="myForm" @submit.prevent="submitFormCustomer" method="post" onsubmit="return false;" enctype="multipart/form-data">
                    <input type="hidden" id="param_customer" class="form-control" />
                    <div class="modal-body">
                        <div class="form-group">
                            <label>{{ __('lang.phone')}}</label>
                            <input type="text" id="customer_phone" class="form-control" />
                            <div v-if="formErrors['customer_phone']" class="errormsg alert alert-danger mt-2">
                                @{{ formErrors['customer_phone'][0] }}
                            </div>
                        </div>
                        
                        <div id="wrapname" class="d-none">
                            <div class="form-group">
                                <label>{{ __('lang.name')}}</label>
                                <input type="text" id="customer_name" class="form-control" />
                                <div v-if="formErrors['customer_name']" class="errormsg alert alert-danger mt-2">
                                    @{{ formErrors['customer_name'][0] }}
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Email</label>
                                <input type="text" id="customer_email" class="form-control" />
                                <div v-if="formErrors['customer_email']" class="errormsg alert alert-danger mt-2">
                                    @{{ formErrors['customer_email'][0] }}
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <button type="submit" id="btnCustomer" class="btn btn-primary btn-sm">{{ __('lang.next')}}</button>
                        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">{{ __('lang.close')}}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    new Vue({
        el: "#wrapModalCustomer",
        data() {
            return {
                csrf: "",
                formErrors: {},
                notif: [],
            };
        },
        mounted: function () {
            this.csrf = "{{ csrf_token() }}";
            let self = this;
        },
        methods: {
            submitFormCustomer: function (e) {
                var form = e.target || e.srcElement;
                if($('#btnCustomer').text() == 'Selanjutnya'){
                  var action = "{{ url('/customer') }}";
                }else{
                  var action = "{{ url('/customerupdate') }}";
                }
                var csrfToken = "{{ csrf_token() }}";

                $('.preloader').css('display','block');

                let datas = new FormData();
                datas.append("catalog", "{{ getData::getCatalogUsername(myFunction::get_username(),'id') }}");
                datas.append("customer_email", $("#customer_email").val());
                datas.append("customer_name", $("#customer_name").val());
                datas.append("customer_phone", $("#customer_phone").val());

                axios.post(action, datas, {
                        headers: {
                            "X-CSRF-TOKEN": csrfToken,
                            Accept: "application/json",
                        },
                    })
                    .then((response) => {
                        $("#preloader").animate(
                            { opacity: "0", },100,
                            function () {
                              $('.preloader').css('display','none');
                              if (response.data == "Registered") {
                                $(".errormsg").hide();
                                $("#customer_phone").prop('readonly', true)
                                $("#wrapname").removeClass("d-none");
                                $("#btnCustomer").text("{{ __('lang.finish')}}");
                              } else {
                                if($("#param_customer").val() == 'accessmenu'){
                                  $("#modalAccess").modal('show');
                                }else{
                                  getLocation();
                                }
                                $("#myCustomer").modal("hide");
                              }
                            }
                        );
                    })
                    .catch((error) => {
                      $('.preloader').css('display','none');
                      $(".errormsg").fadeIn();
                      this.formErrors = error.response.data.errors;
                    });
            },
        },
    });
</script>
