<div class="modal" id="myBell">{{app()->setLocale(Session::get('locale'))}}
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <!-- Modal Header -->
      <div class="modal-header">
        <b class="modal-title">{{ __('lang.panggil')}} {{ __('lang.petugas')}}</b>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <div class="form-group">
          <label>{{ __('lang.numtable')}}</label>
          <input type="text" id="tablebell" class="form-control">
        </div>
        <div class="form-group">
          <label>{{ __('lang.order')}} ({{ __('lang.optional')}})</label>
          <textarea id="messagebell" id="messagebell" class="form-control"></textarea>
        </div>
      </div>
      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-primary btn-sm" onclick="bellTable()">{{ __('lang.panggil')}} {{ __('lang.petugas')}}</button>
        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">{{ __('lang.close')}}</button>
      </div>
    </div>
  </div>
</div>