<div class="modal" id="modalAddons">{{app()->setLocale(Session::get('locale'))}}
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <!-- Modal Header -->
      <div class="modal-header">
        <b class="modal-title">Add-ons</b>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <div id="addonsData"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary btn-sm" onclick="accessCatalog()">{{ __('lang.access')}} {{ getData::getCatalogUsername(myFunction::get_username(),'catalog_title') }}</button>
      </div>
    </div>
  </div>
</div>