<!-- Modal Form-->
<div class="modal" id="cartModal">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <b id="titleCart" class="modal-title"></b>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">{{app()->setLocale(Session::get('locale'))}}
                <input type="hidden" id="cartid" />
                <input type="hidden" id="category" />
                <input type="hidden" id="item_id" />
                <input type="hidden" id="item" />
                <input type="hidden" id="price" />
                <input type="hidden" id="discount" />
                <div id="cartQtyModal" class="form-group">
                    <input type="number" id="qty" min="1" max="100" />
                </div>
                <div class="form-group">
                    <label>{{ __('lang.notes')}}</label>
                    <input type="text" id="note" class="form-control" />
                </div>
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" id="btnCart" class="btn btn-primary btn-sm" onclick="saveCart()"></button>
                <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">{{ __('lang.close')}}</button>
            </div>
        </div>
    </div>
</div>
<!-- End -->
<script type="text/javascript">
    function addCart(category,itemid,item,price,discount){
        @if(!Session::get('location'))
          @if(getData::getCatalogUsername(myFunction::get_username(),'customer_data') == 'Y')
              setCustomer();
          @else
              Swal.fire({
                title: "{{ __('lang.notif')}}",
                text: "{{ getData::getCatalogUsername(myFunction::get_username(),'catalog_title') }} {{ __('lang.locaccess')}}",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: "Ya",
                cancelButtonText: "Tidak"
              }).then((result) => {
                if (result.value) {
                  getLocation();
                }
              })
          @endif
          return false;
        @endif

        $.ajax({
            url: "{{ url('/cart/status') }}",
            type: 'GET',
        })
        .done(function(data) {
            if(data.status != 'Order' && data.pending == 'N'){
                Swal.fire({
                  title: "{{ __('lang.information')}}",
                  text: "{{ __('lang.makenewinvoice')}}",
                  icon: 'warning',
                  showCancelButton: true,
                  confirmButtonColor: '#3085d6',
                  cancelButtonColor: '#d33',
                  confirmButtonText: "Ya",
                  cancelButtonText: "Tidak"
                }).then((result) => {
                  if (result.value) {
                    closeSession();
                  }
                })
                return false;
            }else{
                $("#cartQtyModal").removeClass('d-none');
                $("#btnCart").html("{{ __('lang.addtochart')}}");
                $("#titleCart").html("{{ __('lang.amount')}}");
                $("#cartid").val('');
                $("#qty").val(1);
                $("#note").val('');
                $("#category").val('');
                $("#item_id").val('');
                $("#item").val('');
                $("#price").val('');
                $("#discount").val('');
                $("#cartModal").modal('show');
                $("#category").val(category);
                $("#item_id").val(itemid);
                $("#item").val(item);
                $("#price").val(price);
                $("#discount").val(discount);
            }
        });
    }
    function saveCart() {
      if($("#cartid").val() > 0){
        updateCart();
        return false;
      }
      $('.preloader').css('display','block');
      obj = new Object();
      obj.catalog = "{{ getData::getCatalogUsername(myFunction::get_username(),'id') }}";
      obj.category = $("#category").val();
      obj.item_id = $("#item_id").val();
      obj.item = $("#item").val();
      obj.price = $("#price").val();
      obj.discount = $("#discount").val();
      obj.via = "{{ getData::getCatalogUsername(myFunction::get_username(),'checkout_type') }}";
      obj.qty = $("#qty").val();
      obj.note = $("#note").val();
      $.ajax({
          url: "{{ url('/cart/save') }}",
          type: "POST",
          data: obj,
      })
      .done(function (data) {
          countCart();
          showOrder();
          $("#cartModal").modal("hide");
          $('.preloader').css('display','none');
      })
      .fail(function () {
          console.log("error");
      });
    }
    function countCart(){
      $('.preloader').css('display','block');
      $.ajax({
          url: "{{ url('/cart/count') }}",
          type: 'GET',
      })
      .done(function(data) {
          $("#qtyTemp").val(data);
          if(data > 0){
              $("#wrapcart").removeClass('d-none');
              $("#countcart").html(data+" Item(s)");
              $('.directorder').addClass('d-none');
              $('.menudirectorder').addClass('d-none');
              //$('.cartorder').removeClass('col-6');
              $('.cartorder').addClass('col-12');
          }else{
              $('.directorder').removeClass('d-none');
              $('.menudirectorder').removeClass('d-none');
              $('.cartorder').removeClass('col-12');
              //$('.cartorder').addClass('col-6');
              $("#wrapcart").addClass('d-none');
          }
          checkStatus();
          $('.preloader').css('display','none');
      })
      .fail(function() {
          console.log("error");
      })
    }
    function editItem(id){
        $("#cartQtyModal").removeClass('d-none');
        $("#orderModal").modal('hide');
        $("#cartid").val('');
        $("#qty").val(1);
        $("#note").val('');
        $("#category").val('');
        $("#item_id").val('');
        $("#item").val('');
        $("#price").val('');
        $("#discount").val('');
        $("#btnCart").html("Save");
        $.ajax({
            url: "{{ url('/cart/show-item') }}"+'/'+id,
            type: 'GET',
        })
        .done(function(data) {
            $("#titleCart").html("Edit "+data.item);
            $("#cartid").val(data.id);
            $("#qty").val(data.qty);
            $("#note").val(data.note);
            $("#cartModal").modal('show');
        })
        .fail(function() {
            console.log("error");
        })
    }
    function editNote(id){
        $("#orderModal").modal('hide');
        $("#cartid").val('');
        $("#qty").val(1);
        $("#note").val('');
        $("#category").val('');
        $("#item_id").val('');
        $("#item").val('');
        $("#price").val('');
        $("#discount").val('');
        $("#btnCart").html("Save");
        $("#cartQtyModal").addClass('d-none');
        $('.preloader').css('display','block');
        $.ajax({
            url: "{{ url('/cart/show-item') }}"+'/'+id,
            type: 'GET',
        })
        .done(function(data) {
            $("#titleCart").html("Edit {{ __('lang.notes')}} "+data.item);
            $("#cartid").val(data.id);
            $("#qty").val(data.qty);
            $("#note").val(data.note);
            $("#cartModal").modal('show');
            $('.preloader').css('display','none');
        })
        .fail(function() {
            console.log("error");
        })
    }
    function updateCart(){
        $('.preloader').css('display','block');
        obj = new Object;
        obj.id = $("#cartid").val();
        obj.qty = $("#qty").val();
        obj.note = $("#note").val();
        $.ajax({
            url: "{{ url('/cart/update') }}",
            type: 'POST',
            data: obj,
        })
        .done(function() {
            $("#cartModal").modal('hide');
            countCart();
            showOrder();
            $('.preloader').css('display','none');
        })
        .fail(function() {
            console.log("error");
        });
    }
</script>
