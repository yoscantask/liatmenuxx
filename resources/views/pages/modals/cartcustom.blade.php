<!-- Modal Form-->
<div class="modal" id="cartModalCustom">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <b id="titleCartCustom" class="modal-title"></b>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">{{app()->setLocale(Session::get('locale'))}}
                <input type="hidden" id="cartidcustom" />
                <input type="hidden" id="categorycustom" />
                <input type="hidden" id="itemidcustom" />
                <input type="hidden" id="itemcustom" />
                <input type="hidden" id="pricecustom" />
                <input type="hidden" id="discountcustom" />
                <input type="hidden" id="notecustom" class="form-control" />
                <div class="form-group">
                    <label>{{ __('lang.amount')}}</label>
                    <input type="number" id="qtycustom" min="1" max="100" readonly />
                </div>
                <div class="form-group">
                    <label>Add-Ons ( Opsional )</label>
                    <div id="addonsItem" style="font-size: 11px;">
                      
                    </div>
                </div>
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" id="btnCartCustom" class="btn btn-primary btn-sm" onclick="saveCartCustom()"></button>
                <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">{{ __('lang.close')}}</button>
            </div>
        </div>
    </div>
</div>
<!-- End -->
<script type="text/javascript">
    function addCartCustom(category,itemid,item,price,discount){
      @if(!Session::get('location'))
          @if(getData::getCatalogUsername(myFunction::get_username(),'customer_data') == 'Y')
              setCustomer();
          @else
              Swal.fire({
                title: "{{ __('lang.notif')}}",
                text: "{{ getData::getCatalogUsername(myFunction::get_username(),'catalog_title') }} {{ __('lang.locaccsess')}}",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: "Ya",
                cancelButtonText: "Tidak"
              }).then((result) => {
                if (result.value) {
                  getLocation();
                }
              })
          @endif
          return false;
      @endif

      $.ajax({
          url: "{{ url('/cart/status') }}",
          type: 'GET',
      })
      .done(function(data) {
          if(data.status != 'Order' && data.pending == 'N'){
              Swal.fire({
                title: "{{ __('lang.information')}}",
                text: "{{ __('lang.makenewinvoice')}}",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: "Ya",
                cancelButtonText: "Tidak"
              }).then((result) => {
                if (result.value) {
                  closeSession();
                }
              })
              return false;
          }else{
              $("#addonsItem").html("{{ __('lang.pliswait')}}")
              $("#btnCartCustom").html("{{ __('lang.addtochart')}}");
              $("#titleCartCustom").html("Custom Item");
              $("#cartidcustom").val('');
              $("#qtycustom").val(1);
              $("#notecustom").val('');
              $("#categorycustom").val('');
              $("#itemidcustom").val('');
              $("#itemcustom").val('');
              $("#pricecustom").val('');
              $("#discountcustom").val('');
              $("#cartModalCustom").modal('show');
              $("#categorycustom").val(category);
              $("#itemidcustom").val(itemid);
              $("#itemcustom").val(item);
              $("#pricecustom").val(price);
              $("#discountcustom").val(discount);
              //Addons
              $.ajax({
                url: "{{ url('/cart/addons') }}"+'/'+itemid+'/0',
                type: 'GET',
              })
              .done(function(data) {
                $('input[type=radio]').prop('checked',false);
                $('input[type=checkbox]').prop('checked',false);
                $("#addonsItem").html(data)
              })
              .fail(function() {
                console.log("error");
              });
              //End
          }
      });
    }
    function saveCartCustom() {
      var arraddonsmultiple=[];
      $("input:checkbox[name*=addons]:checked").each(function(){
          arraddonsmultiple.push($(this).val());
      });
      var arraddonssingle=[];
      $("input[type='radio']:checked").each(function(){
          arraddonssingle.push($(this).val());
      });
      // alert(arraddonsmultiple);
      // alert(arraddonssingle);

      obj = new Object();
      obj.catalog = "{{ getData::getCatalogUsername(myFunction::get_username(),'id') }}";
      obj.category = $("#categorycustom").val();
      obj.item_id = $("#itemidcustom").val();
      obj.item = $("#itemcustom").val();
      obj.price = $("#pricecustom").val();
      obj.discount = $("#discountcustom").val();
      obj.via = "{{ getData::getCatalogUsername(myFunction::get_username(),'checkout_type') }}";
      obj.qty = $("#qtycustom").val();
      obj.note = $("#notecustom").val();
      obj.arraddonsmultiple = arraddonsmultiple;
      obj.arraddonssingle = arraddonssingle;
      $.ajax({
          url: "{{ url('/cart/save') }}",
          type: "POST",
          data: obj,
      })
      .done(function (data) {
          $('input[type=radio]').prop('checked',false);
          $('input[type=checkbox]').prop('checked',false);
          //window.location.reload(true);
          countCart();
          showOrder();
          $("#cartModalCustom").modal("hide");
      })
      .fail(function () {
          console.log("error");
      });
    }
</script>
