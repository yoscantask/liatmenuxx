<div class="modal" id="orderModal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">{{app()->setLocale(Session::get('locale'))}}
                <b class="modal-title">{{ __('lang.orderlist')}}</b>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div>
                <div id="orderList" style="padding: 10px;"></div>
                <form id="payment-form" method="post" action="{{ url('/payment/finish') }}">
                    <input type="hidden" name="_token" value="{!! csrf_token() !!}" />
                    <input type="hidden" name="result_type" id="result-type" value="" />
                    <input type="hidden" name="result_data" id="result-data" value="" />
                    <input type="hidden" name="payment_method" id="payment_method" value="3" />
                    <input type="hidden" name="result_position" id="result-position" />
                    <input type="hidden" name="result_tax" id="result-tax" />
                </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function showOrder() {
        $('.preloader').css('display','block');
        $.ajax({
            url: "{{ url('/cart/status') }}",
            type: "GET",
        }).done(function (data) {
            setTimeout(function() {
                if (data.status == "Completed") {
                    if ("{{ getData::getCatalogUsername(myFunction::get_username(),'advance_payment') == 'N' }}" && "{{ getData::getInvoice('pending')=='Y' }}") {
                        if (data.payment_method > 0) {
                            if (parseInt($("#qtyTemp").val()) > 0) {
                                $("#cartModal").modal("hide");
                                $("#orderModal").modal("show");
                                $("#orderList").html("{{ __('lang.pliswait')}}");
                                $.ajax({
                                    url: "{{ url('/cart/list') }}",
                                    type: "GET",
                                })
                                    .done(function (data) {
                                        $("#orderList").html(data);
                                        $('.preloader').css('display','none');
                                    })
                                    .fail(function () {
                                        console.log("error");
                                    });
                            }
                        } else {
                            showLastPayment();
                        }
                    }
                } else {
                    if (parseInt($("#qtyTemp").val()) > 0) {
                        $("#cartModal").modal("hide");
                        $("#orderModal").modal("show");
                        $("#orderList").html("{{ __('lang.pliswait')}}");
                        $.ajax({
                            url: "{{ url('/cart/list') }}",
                            type: "GET",
                        })
                            .done(function (data) {
                                $("#orderList").html(data);
                                $('.preloader').css('display','none');
                            })
                            .fail(function () {
                                console.log("error");
                            });
                    }
                }
            }, 1000);
        });
    }
    function removeItem(id) {
        Swal.fire({
            title: "{{ __('lang.confirm')}}",
            text: "{{ __('lang.areusuretodeleteitem')}}",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: "#3085d6",
            cancelButtonColor: "#d33",
            confirmButtonText: "Ya",
            cancelButtonText: "Tidak",
        }).then((result) => {
            if (result.value) {
                $('.preloader').css('display','block');
                $.ajax({
                    url: "{{ url('/cart/delete-item') }}" + "/" + id,
                    type: "GET",
                })
                    .done(function (data) {
                        $("#orderModal").modal("hide");
                        countCart();
                        showOrder();
                        $('.preloader').css('display','none');
                    })
                    .fail(function () {
                        console.log("error");
                    });
            }
        });
    }
    function removeAdd(detail, group) {
        Swal.fire({
            title: "{{ __('lang.confirm')}}",
            text: "{{ __('lang.areusuretodeleteitem')}}",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: "#3085d6",
            cancelButtonColor: "#d33",
            confirmButtonText: "Ya",
            cancelButtonText: "Tidak",
        }).then((result) => {
            if (result.value) {
                $('.preloader').css('display','block');
                $.ajax({
                    url: "{{ url('/cart/delete-addon') }}" + "/" + detail + "/" + group,
                    type: "GET",
                })
                    .done(function (data) {
                        $('.preloader').css('display','none');
                        //window.location.reload(true);
                        $("#orderModal").modal("hide");
                        countCart();
                        showOrder();
                    })
                    .fail(function () {
                        console.log("error");
                    });
            }
        });
    }
    function cancelOrder() {
        Swal.fire({
            title: "{{ __('lang.confirm')}}",
            text: "{{ __('lang.areusuretocancelorder')}}",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: "#3085d6",
            cancelButtonColor: "#d33",
            confirmButtonText: "Ya",
            cancelButtonText: "Tidak",
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url: "{{ url('/cart/delete') }}",
                    type: "GET",
                })
                    .done(function (data) {
                        $("#orderModal").modal("hide");
                        window.location.reload(true);
                    })
                    .fail(function () {
                        console.log("error");
                    });
            }
        });
    }
    function checkout() {
        if ($("#position").val() == "") {
            Swal.fire("Ops!", "{{ __('lang.plisinsert')}}", "error");
            return false;
        }
        Swal.fire({
            title: "{{ __('lang.confirm')}}",
            text: "{{ __('lang.areusuretocheckout')}}",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: "#3085d6",
            cancelButtonColor: "#d33",
            confirmButtonText: "Ya",
            cancelButtonText: "Tidak",
        }).then((result) => {
            if (result.value) {
                $('.preloader').css('display','block');
                $("#orderModal").modal("hide");
                obj = new Object();
                obj.position = $("#position").val();
                obj.tax = $("#tax").val();
                obj.amount = parseInt($("#grand").val()) + parseInt($("#tax").val());
                $.ajax({
                    url: "{{ url('/cart/checkout') }}",
                    type: "POST",
                    data: obj,
                })
                .done(function (data) {
                    $("#qtyTemp").val(0);
                    $('.preloader').css('display','none');
                    Swal.fire("{{ __('lang.information')}}", "{{ __('lang.yourinfoorder')}}", "success").then((result) => {
                        if (result.value) {
                            countCart();
                        }
                    });
                    //$('.preloader').css('display','none');
                })
                .fail(function () {
                    console.log("error");
                    $('.preloader').css('display','none');
                });
            }
        });
    }
    function checkStatus() {
        $.ajax({
            url: "{{ url('/cart/status') }}",
            type: "GET",
        })
        .done(function (data) {
            if (data.status == "Order") {
                $("#orderStatus").addClass("d-none");
                $("#cartStatus").removeClass("d-none");
            } else {
                $("#cartStatus").addClass("d-none");
                $("#orderStatus").removeClass("d-none");
                if (data.status == "Checkout") {
                    var status = "Mohon tunggu";
                } else if (data.status == "Approve") {
                    var status = "Disetujui";
                } else if (data.status == "Process") {
                    var status = "Diproses";
                } else if (data.status == "Delivered") {
                    var status = "Diantar";
                } else if (data.status == "Completed") {
                    if ("{{ getData::getCatalogUsername(myFunction::get_username(),'advance_payment') == 'Y' }}") {
                        var status = "Selesai";
                        Swal.fire({
                            title: "Informasi",
                            text: "Terima kasih atas kunjungan anda di {{ getData::getCatalogUsername(myFunction::get_username(),'catalog_title') }}",
                            icon: "success",
                        }).then((result) => {
                            if (result.value) {
                                window.open("{{ url('/cart/struk') }}" + "/" + data.invoice_number + "/print", "_blank");
                                setTimeout(function () {
                                    closeSession();
                                }, 1000);
                            }
                        });
                    } else {
                        if (data.pending == "N") {
                            var status = "Selesai";
                            Swal.fire({
                                title: "Informasi",
                                text: "Terima kasih atas kunjungan anda di {{ getData::getCatalogUsername(myFunction::get_username(),'catalog_title') }}",
                                icon: "success",
                            }).then((result) => {
                                if (result.value) {
                                    window.open("{{ url('/cart/struk') }}" + "/" + data.invoice_number + "/print", "_blank");
                                    setTimeout(function () {
                                        closeSession();
                                    }, 1000);
                                }
                            });
                        } else {
                            if (data.payment_method > 0) {
                                $("#orderModal").modal("hide");
                                var status = "{{ __('lang.confirmation')}}";
                            } else {
                                var status = "{{ __('lang.payment')}}";
                                showLastPayment();
                            }
                        }
                    }
                } else if (data.status == "Cancel") {
                    var status = "Batal [ " + data.note_order + " ]";
                    closeSession();
                }
                $("#orderStatus").html("Order : " + data.invoice_number + " ( " + status + " )");
                if (data.payment_method > 0 && data.pending == "N") {
                    $("#printOrder").removeClass("d-none");
                } else {
                    $("#printOrder").addClass("d-none");
                }
            }
        })
        .fail(function () {
            console.log("error");
        });
    }
    function showLastPayment() {
        $("#orderModal").modal("show");
        $("#orderList").html("{{ __('lang.pliswait')}}");
        $.ajax({
            url: "{{ url('/cart/payment') }}",
            type: "GET",
        })
        .done(function (data) {
            $('.preloader').css('display','none');
            $("#orderList").html(data);
        })
        .fail(function () {
            console.log("error");
        });
    }
    function paymentMethod() {
        if ($("#position").val() == "") {
            Swal.fire("Ops!", "{{ __('lang.plisinsert')}}", "error");
            return false;
        }
        $('.preloader').css('display','block');
        payment = $("#paymentmethod").val();
        if (payment == 1) {
            $.ajax({
                url: "{{ url('/cart/status') }}",
                type: "GET",
            }).done(function (data) {
                if (data.status == "Completed") {
                    completeLastCheckout();
                } else {
                    checkout();
                }
                $('.preloader').css('display','none');
            });
        } else if (payment == 0) {
            completeLastCheckout();
        } else if (payment == 2) {
            confirmation($("#position").val());
        } else if (payment == 3) {
            paymentGateway();
        }
    }
    function completeLastCheckout() {
        if ($("#position").val() == "") {
            Swal.fire("Ops!", "{{ __('lang.plisinsert')}}", "error");
            return false;
        }
        $('.preloader').css('display','block');
        $("#orderModal").modal("hide");
        $.ajax({
            url: "{{ url('/cart/completelastcheckout') }}",
            type: "GET",
        })
        .done(function (data) {
            $('.preloader').css('display','none');
            Swal.fire("{{ __('lang.information')}}", "{{ __('lang.plispaymentofficer')}}", "success").then((result) => {
                if (result.value) {
                    closeSession();
                }
            });
        })
        .fail(function () {
            console.log("error");
        });
    }
    function confirmation(position) {
        $("#orderModal").modal("show");
        $("#orderList").html("Please wait...");
        $('.preloader').css('display','block');
        obj = new Object();
        obj.position = position;
        $.ajax({
            url: "{{ url('/cart/confirmation') }}",
            type: "POST",
            data: obj,
        })
            .done(function (data) {
                $("#orderList").html(data);
                $('.preloader').css('display','none');
            })
            .fail(function () {
                console.log("error");
            });
    }
    function paymentGateway() {
        $.ajax({
            url: "{{ url('/payment/snap/') }}" + "/" + $("#grand").val(),
            success: function (data) {
                //location = data;
                console.log("token = " + data);
                var resultType = document.getElementById("result-type");
                var resultData = document.getElementById("result-data");
                function changeResult(type, data) {
                    $("#result-type").val(type);
                    $("#result-data").val(JSON.stringify(data));
                    $("#result-position").val($("#position").val());
                    $("#result-tax").val($("#tax").val());
                }
                snap.pay(data, {
                    onSuccess: function (result) {
                        changeResult("success", result);
                        console.log(result.status_message);
                        console.log(result);
                        $.ajax({
                            url: "{{ url('/cart/status') }}",
                            type: "GET",
                        }).done(function (data) {
                            window.open("{{ url('/cart/struk') }}" + "/" + data.invoice_number + "/print", "_blank");
                            $("#qtyTemp").val(0);
                            $("#payment-form").submit();
                        });
                    },
                    onPending: function (result) {
                        changeResult("pending", result);
                        console.log(result.status_message);
                        $("#qtyTemp").val(0);
                        $("#payment-form").submit();
                    },
                    onError: function (result) {
                        changeResult("error", result);
                        console.log(result.status_message);
                        $("#qtyTemp").val(0);
                        $("#payment-form").submit();
                    },
                });
            },
        });
    }
    function paymentAction() {
        payment = $("#paymentmethod").val();
        if (payment == 2) {
            $("#transferinfo").removeClass("d-none");
        } else {
            $("#transferinfo").addClass("d-none");
        }
    }
    function printOrder() {
        $.ajax({
            url: "{{ url('/cart/status') }}",
            type: "GET",
        }).done(function (data) {
            window.open("{{ url('/cart/struk') }}" + "/" + data.invoice_number + "/print", "_blank");
        });
    }
    function addOns(item) {
        $("#orderList").html("Please wait...");
        $.ajax({
            url: "{{ url('/cart/addons') }}" + "/" + item,
            type: "GET",
        })
            .done(function (data) {
                $("#orderModal").modal("hide");
                $("#modalAddons").modal("show");
                $("#addonsData").html(data);
            })
            .fail(function () {
                console.log("error");
            });
    }
</script>