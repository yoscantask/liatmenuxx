<div class="modal" id="groupAddonsModal">
	<div class="modal-dialog modal-sm">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">{{app()->setLocale(Session::get('locale'))}}
                <b class="modal-title">Edit AddOns</b>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
            	<div class="form-group">
            	    <label>{{ __('lang.amount')}}</label>
            	    <input type="number" id="qtyaddons" min="1" max="100" readonly />
            	</div>
            	<div class="form-group">
            	    <label>Add-Ons</label>
            	    <input type="hidden" id="invoicedetailid" />
            	    <input type="hidden" id="groupaddons" />
            	    <div id="addonsItemUpdate" style="font-size: 11px;">
            	      
            	    </div>
            	</div>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnCartCustom" class="btn btn-primary btn-sm" onclick="updateCartAddons()">Save</button>
                <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">{{ __('lang.close')}}</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
	function editAddon(item,detail,group,qty){
	    $("#orderModal").modal('hide');
	    $("#qtyaddons").val(qty);
	    $("#invoicedetailid").val(detail);
	    $("#groupaddons").val(group);
	    $("#groupAddonsModal").modal('show');
	    $('input[type=radio]').prop('checked',false);
	    $('input[type=checkbox]').prop('checked',false);
	    //Addons
	    $.ajax({
	      url: "{{ url('/cart/addons') }}"+'/'+item+'/'+group,
	      type: 'GET',
	    })
	    .done(function(data) {
	      $("#addonsItemUpdate").html(data)
	    })
	    .fail(function() {
	      console.log("error");
	    });
	    //End
	}
	function updateCartAddons(){
	    var arraddonsmultiple=[];
	    $("input:checkbox[name*=addons]:checked").each(function(){
	        arraddonsmultiple.push($(this).val());
	    });
	    var arraddonssingle=[];
	    $("input[type='radio']:checked").each(function(){
	        arraddonssingle.push($(this).val());
	    });
	    if(arraddonsmultiple.length == 0 && arraddonssingle.length == 0){
	    	Swal.fire("Ops!", "Mohon pilih AddOns.", "error");
	    	return false;
	    }
	    obj = new Object;
	    obj.detailid = $("#invoicedetailid").val();
	    obj.group = $("#groupaddons").val();
	    obj.qty = $("#qtyaddons").val();
	    obj.arraddonsmultiple = arraddonsmultiple;
	    obj.arraddonssingle = arraddonssingle;
	    $.ajax({
	        url: "{{ url('/cart/updateaddons') }}",
	        type: 'POST',
	        data: obj,
	    })
	    .done(function() {
	    	$('input[type=radio]').prop('checked',false);
	    	$('input[type=checkbox]').prop('checked',false);
	        $("#groupAddonsModal").modal('hide');
	        countCart();
	        showOrder();
	    })
	    .fail(function() {
	        console.log("error");
	    });
	}
</script>