@extends('layouts.main')
@section('content')
<!-- ***** Main Banner Area Start ***** -->
{{app()->setLocale(Session::get('locale'))}}
@if(!empty($getProfile['sliders']))
<div id="top">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-4">
				<div class="left-content">
					<div class="inner-content">
						<h4>{{ $getProfile['catalog_title'] }}</h4>
						<h6>{{ $getProfile['catalog_tagline'] }}</h6>
						<div class="main-white-button scroll-to-section">
							<a href="#chefs">{{__('lang.lmk')}}</a>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-8">
				<div class="main-banner header-text">
					<div class="Modern-Slider">
						@foreach(json_decode($getProfile['sliders'],true) as $slider)
						<!-- Item -->
						<div class="item">
							<div class="img-fill">
								<img src="{{ asset('/images/'.getData::getSlider($slider,'sliders_image')) }}" alt="{{ getData::getSlider($slider,'sliders_title') }}" style="width: 100%;height: 100%">
							</div>
						</div>
						<!-- // Item -->
						@endforeach
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endif
<!-- ***** Main Banner Area End ***** -->

<section class="section" id="chefs">
	<div class="container">

	@if ($getProfile['view_subcat'] == 'grid')
		<div class="large-screen" id="gridviewnormal">
			@foreach($detail as $kdetail => $vdetail)
			<div class="row">
				<div class="col-lg-12">
					<div class="section-heading" style="margin-bottom: 0">
						@if($kdetail == 0)
						<h6>{{ $getProfile['catalog_title'] }} Menu</h6>
						@endif
						<div class="row">
							<div class="col">
								@if($kdetail > 0)
								<h2 style="margin-top: 0">{{ __('lang.'.$vdetail['category_name']) }}</h2>
								@else
								<h2>{{ __('lang.'.$vdetail['category_name']) }}</h2>
								@endif						
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row mb-5">
				@foreach(getData::getCatalogSubCategory($getProfile['id'],$vdetail['category_id']) as $subcategory)
				@if($subcategory['subcategory_id'] > 0)
				<div class="col-lg-3 col-md-6 mb-4">
					<a href="{{ url('/menu/'.$vdetail['category_slug'].'/'.$subcategory['subcategory_slug']) }}">
						<div class="chef-item">
							<div class="thumb">
								<img src="{{ asset('/images/'.$subcategory['subcategory_image']) }}" alt="{{ $subcategory['subcategory_name'] }}">
							</div>
							<div class="down-content">
								<h4 style="font-size: 1rem">{{ $subcategory['subcategory_name'] }}</h4>
								<span>{{ getData::getTotalItems($getProfile['id'],$subcategory['subcategory_id']) }} Item(s)</span>
								<div class="mt-3">
									<a href="{{ url('/menu/'.$vdetail['category_slug'].'/'.$subcategory['subcategory_slug']) }}" class="btn mybutton btn-xs" style="font-size: 11px;">{{__('lang.lihatselengkapnya')}}</a>
								</div>
							</div>
						</div>
					</a>
				</div>
				@else
				@foreach(getData::getCatalogItems($getProfile['id'],$vdetail['category_id'],'0') as $item)
				<div class="col-lg-3 col-md-6 mb-4">
					<div class="chef-item">
						<a href="{{ url('/item/'.$vdetail['category_slug'].'/'.$item['items_slug']) }}">
							<div class="thumb">
								<div class="single-item">
									<img src="{{ asset('/images/'.$item['item_image_primary']) }}" alt="{{ $item['items_name'] }}">
									@if(!empty($item['item_image_one']) and $item['item_image_one'] != $item['item_image_primary'])
									<img src="{{ asset('/images/'.$item['item_image_one']) }}" alt="{{ $item['items_name'] }}">
									@endif
									@if(!empty($item['item_image_two']) and $item['item_image_two'] != $item['item_image_primary'])
									<img src="{{ asset('/images/'.$item['item_image_two']) }}" alt="{{ $item['items_name'] }}">
									@endif
									@if(!empty($item['item_image_three']) and $item['item_image_three'] != $item['item_image_primary'])
									<img src="{{ asset('/images/'.$item['item_image_three']) }}" alt="{{ $item['items_name'] }}">
									@endif
									@if(!empty($item['item_image_four']) and $item['item_image_four'] != $item['item_image_primary'])
									<img src="{{ asset('/images/'.$item['item_image_four']) }}" alt="{{ $item['items_name'] }}">
									@endif
								</div>
							</div>
	
							<div class="down-content">
								<a href="{{ url('/item/'.$vdetail['category_slug'].'/'.$item['items_slug']) }}">
									<h4 style="font-size: 1rem">{{ $item['items_name'] }}</h4>
									<span>
										<sup>Rp. </sup>
										@if($item['items_discount'] > 0)
										<span class="text-danger" style="text-decoration: line-through;">{{ number_format($item['items_price']) }}</span>
										{{ number_format($item['items_price']-$item['items_discount']) }}
										@else
										{{ number_format($item['items_price']) }}
										@endif
									</span>
								</a>
								<div class="row mt-3">
									@if($item['ready_stock']=='Y')
										@if(getData::getCatalogUsername(myFunction::get_username(),'feature') == 'Full')
											<div class="col-6 directorder_hide" style="display: none">
												<a href="javascript:void(0)" class="btn greenbutton btn-xs btn-transaction" onclick="directOrder('{{ $vdetail['category_slug'] }}','0','{{ $item['items_name'] }}','{{ $item['items_slug'] }}')" style="font-size: 11px;"><i class="fa fa-whatsapp mr-1"></i> Pesan</a>
											</div>
											<div class="col cartorder">
												@if((getData::getAddons($item['id'])->count() > 0))
													<a href="javascript:void(0)" class="btn {{ (Session::get('location'))?'darkbutton':'greybutton' }} btn-xs btn-transaction" onclick="addCartCustom('{{ $item['category_name'] }}','{{ $item['id'] }}','{{ $item['items_name'] }}','{{ $item['items_price'] }}','{{ $item['items_discount'] }}')" style="font-size: 11px;"><i class="fa fa-random mr-1"></i> Pesan
													</a>
												@else
													<a href="javascript:void(0)" class="btn {{ (Session::get('location'))?'darkbutton':'greybutton' }} btn-xs btn-transaction" onclick="addCart('{{ __('lang.'.$vdetail['category_name']) }}','{{ $item['id'] }}','{{ $item['items_name'] }}','{{ $item['items_price'] }}','{{ $item['items_discount'] }}')" style="font-size: 11px;"><i class="fa fa-shopping-cart mr-1"></i> Pesan</a>
												@endif
											</div>
										@else
											<div class="col-12">
												<a href="javascript:void(0)" class="btn darkbutton btn-xs btn-transaction" style="font-size: 11px;">Item tersedia</a>
											</div>
										@endif
									@else
										<div class="col-12">
											<a href="javascript:void(0)" class="btn btn-danger btn-xs btn-transaction" style="font-size: 11px;">Item tidak tersedia</a>
										</div>
									@endif
								</div>
							</div>
						</a>
					</div>
				</div>
				@endforeach
				@endif
				@endforeach
			</div>
			@endforeach
		</div>

		<div class="small-screen" id="gridviewmobile">			
			@foreach($detail as $kdetail => $vdetail)			
			<div class="row">
				<div class="col-lg-12">
					<div class="section-heading" style="margin-bottom: 0">
						@if($kdetail == 0)
						<h6>{{ $getProfile['catalog_title'] }} Menu</h6>
						@endif
						<div class="row">
							<div class="col">
								@if($kdetail > 0)
								<h2 style="margin-top: 0">{{ __('lang.'.$vdetail['category_name']) }}</h2>
								@else
								<h2>{{ __('lang.'.$vdetail['category_name']) }}</h2>
								@endif						
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row" style="
				height: 290px;
				overflow-x: auto;
				-webkit-overflow-scrolling: touch;
				display: grid;
				grid-auto-flow: column;
				">
				@foreach(getData::getCatalogSubCategory($getProfile['id'],$vdetail['category_id']) as $subcategory)
				@if($subcategory['subcategory_id'] > 0)
				<div class="mr-1" style="width: 130px;">
					<a href="{{ url('/menu/'.$vdetail['category_slug'].'/'.$subcategory['subcategory_slug']) }}">
						<div class="chef-item mb-1">
							<div class="thumb">
								<img src="{{ asset('/images/'.$subcategory['subcategory_image']) }}" alt="{{ $subcategory['subcategory_name'] }}">
							</div>
							<div class="down-content">
								<div class="mb-1" style="height: 20px">
									<h4 style="font-size: 12px">{{ $subcategory['subcategory_name'] }}</h4>
								</div>
								<span style="font-size: 9px">{{ getData::getTotalItems($getProfile['id'],$subcategory['subcategory_id']) }} Item(s)</span>
								<div class="mt-1">
									<a href="{{ url('/menu/'.$vdetail['category_slug'].'/'.$subcategory['subcategory_slug']) }}" class="btn mybutton btn-xs" style="font-size: 11px;">{{__('lang.lihatselengkapnya')}}</a>
								</div>
							</div>
						</div>
					</a>
				</div>
				@else
				@foreach(getData::getCatalogItems($getProfile['id'],$vdetail['category_id'],'0') as $item)
				<div class="mr-1" style="width: 130px">
					<div class="chef-item">
						<a href="{{ url('/item/'.$vdetail['category_slug'].'/'.$item['items_slug']) }}">
							<div class="thumb">
								<div class="single-item">
									<img src="{{ asset('/images/'.$item['item_image_primary']) }}" alt="{{ $item['items_name'] }}">
									@if(!empty($item['item_image_one']) and $item['item_image_one'] != $item['item_image_primary'])
									<img src="{{ asset('/images/'.$item['item_image_one']) }}" alt="{{ $item['items_name'] }}">
									@endif
									@if(!empty($item['item_image_two']) and $item['item_image_two'] != $item['item_image_primary'])
									<img src="{{ asset('/images/'.$item['item_image_two']) }}" alt="{{ $item['items_name'] }}">
									@endif
									@if(!empty($item['item_image_three']) and $item['item_image_three'] != $item['item_image_primary'])
									<img src="{{ asset('/images/'.$item['item_image_three']) }}" alt="{{ $item['items_name'] }}">
									@endif
									@if(!empty($item['item_image_four']) and $item['item_image_four'] != $item['item_image_primary'])
									<img src="{{ asset('/images/'.$item['item_image_four']) }}" alt="{{ $item['items_name'] }}">
									@endif
								</div>
							</div>
	
							<div class="down-content">
								<a href="{{ url('/item/'.$vdetail['category_slug'].'/'.$item['items_slug']) }}">
									<div class="mb-1" style="height: 20px">
										<h4 style="font-size: 12px">{{ $item['items_name'] }}</h4>
									</div>
									<span style="font-size: 9px">
										<sup>Rp. </sup>
										@if($item['items_discount'] > 0)
										<span class="text-danger" style="text-decoration: line-through;">{{ number_format($item['items_price']) }}</span>
										{{ number_format($item['items_price']-$item['items_discount']) }}
										@else
										{{ number_format($item['items_price']) }}
										@endif
									</span>
								</a>
								<div class="row mt-1">
									@if($item['ready_stock']=='Y')
										@if(getData::getCatalogUsername(myFunction::get_username(),'feature') == 'Full')
											<div class="col-6 directorder_hide" style="display: none">
												<a href="javascript:void(0)" class="btn greenbutton btn-xs btn-transaction" onclick="directOrder('{{ $vdetail['category_slug'] }}','0','{{ $item['items_name'] }}','{{ $item['items_slug'] }}')" style="font-size: 11px;"><i class="fa fa-whatsapp mr-1"></i> Pesan</a>
											</div>
											<div class="col cartorder">
												@if((getData::getAddons($item['id'])->count() > 0))
													<a href="javascript:void(0)" class="btn {{ (Session::get('location'))?'darkbutton':'greybutton' }} btn-xs btn-transaction" onclick="addCartCustom('{{ $item['category_name'] }}','{{ $item['id'] }}','{{ $item['items_name'] }}','{{ $item['items_price'] }}','{{ $item['items_discount'] }}')" style="font-size: 11px;"><i class="fa fa-random mr-1"></i> Pesan
													</a>
												@else
													<a href="javascript:void(0)" class="btn {{ (Session::get('location'))?'darkbutton':'greybutton' }} btn-xs btn-transaction" onclick="addCart('{{ __('lang.'.$vdetail['category_name']) }}','{{ $item['id'] }}','{{ $item['items_name'] }}','{{ $item['items_price'] }}','{{ $item['items_discount'] }}')" style="font-size: 11px;"><i class="fa fa-shopping-cart mr-1"></i> Pesan</a>
												@endif
											</div>
										@else
											<div class="col-12">
												<a href="javascript:void(0)" class="btn darkbutton btn-xs btn-transaction" style="font-size: 11px;">Item tersedia</a>
											</div>
										@endif
									@else
										<div class="col-12">
											<a href="javascript:void(0)" class="btn btn-danger btn-xs btn-transaction" style="font-size: 11px;">Item tidak tersedia</a>
										</div>
									@endif
								</div>
							</div>
						</a>
					</div>
				</div>
				@endforeach
				@endif
				@endforeach
			</div>
			@endforeach
		</div>			
	@endif

	@if ($getProfile['view_subcat'] == 'list')
		<div id="listviewnormal">
			@foreach($detail as $kdetail => $vdetail)
			<div class="row">
				<div class="col-lg-12">
					<div class="section-heading" style="margin-bottom: 0">
						@if($kdetail == 0)
						<h6>{{ $getProfile['catalog_title'] }} Menu</h6>
						@endif
						<div class="row">
							<div class="col">
								@if($kdetail > 0)
								<h2 style="margin-top: 0">{{ __('lang.'.$vdetail['category_name']) }}</h2>
								@else
								<h2>{{ __('lang.'.$vdetail['category_name']) }}</h2>
								@endif						
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row mb-5">
				@foreach(getData::getCatalogSubCategory($getProfile['id'],$vdetail['category_id']) as $subcategory)
				@if($subcategory['subcategory_id'] > 0)
				<div class="col-lg-6 col-md-10 mb-3">
					<a href="{{ url('/menu/'.$vdetail['category_slug'].'/'.$subcategory['subcategory_slug']) }}">
						<div class="row chef-item" style="margin: -1px">
							<div class="col thumb mt-2" style="max-width: 135px">
								<img src="{{ asset('/images/'.$subcategory['subcategory_image']) }}" alt="{{ $subcategory['subcategory_name'] }}">
							</div>
							<div class="col down-content mr-2">
								<h4 style="font-size: 12px">{{ $subcategory['subcategory_name'] }}</h4>
								<span style="font-size: 10px">{{ getData::getTotalItems($getProfile['id'],$subcategory['subcategory_id']) }} Item(s)</span>
								<div class="mt-2">
									<a href="{{ url('/menu/'.$vdetail['category_slug'].'/'.$subcategory['subcategory_slug']) }}" class="btn mybutton btn-xs" style="font-size: 11px;">{{__('lang.lihatselengkapnya')}}</a>
								</div>
							</div>
						</div>
					</a>
				</div>
				@else
				@foreach(getData::getCatalogItems($getProfile['id'],$vdetail['category_id'],'0') as $item)
				<div class="col-lg-6 col-md-10 mb-4">
					<div class="chef-item" style="margin: -1px">
						<a href="{{ url('/item/'.$vdetail['category_slug'].'/'.$item['items_slug']) }}">
							<div class="col thumb mt-2" style="max-width: 135px">
								<div class="single-item">
									<img src="{{ asset('/images/'.$item['item_image_primary']) }}" alt="{{ $item['items_name'] }}">
									@if(!empty($item['item_image_one']) and $item['item_image_one'] != $item['item_image_primary'])
									<img src="{{ asset('/images/'.$item['item_image_one']) }}" alt="{{ $item['items_name'] }}">
									@endif
									@if(!empty($item['item_image_two']) and $item['item_image_two'] != $item['item_image_primary'])
									<img src="{{ asset('/images/'.$item['item_image_two']) }}" alt="{{ $item['items_name'] }}">
									@endif
									@if(!empty($item['item_image_three']) and $item['item_image_three'] != $item['item_image_primary'])
									<img src="{{ asset('/images/'.$item['item_image_three']) }}" alt="{{ $item['items_name'] }}">
									@endif
									@if(!empty($item['item_image_four']) and $item['item_image_four'] != $item['item_image_primary'])
									<img src="{{ asset('/images/'.$item['item_image_four']) }}" alt="{{ $item['items_name'] }}">
									@endif
								</div>
							</div>	
							<div class="col down-content mr-2">
								<a href="{{ url('/item/'.$vdetail['category_slug'].'/'.$item['items_slug']) }}">
									<h4 style="font-size: 12px">{{ $item['items_name'] }}</h4>
									<span style="font-size: 10px">
										<sup>Rp. </sup>
										@if($item['items_discount'] > 0)
										<span class="text-danger" style="text-decoration: line-through;">{{ number_format($item['items_price']) }}</span>
										{{ number_format($item['items_price']-$item['items_discount']) }}
										@else
										{{ number_format($item['items_price']) }}
										@endif
									</span>
								</a>
								<div class="row mt-3">
									@if($item['ready_stock']=='Y')
										@if(getData::getCatalogUsername(myFunction::get_username(),'feature') == 'Full')
											<div class="col-6 directorder_hide" style="display: none">
												<a href="javascript:void(0)" class="btn greenbutton btn-xs btn-transaction" onclick="directOrder('{{ $vdetail['category_slug'] }}','0','{{ $item['items_name'] }}','{{ $item['items_slug'] }}')" style="font-size: 11px;"><i class="fa fa-whatsapp mr-1"></i> Pesan</a>
											</div>
											<div class="col cartorder">
												@if((getData::getAddons($item['id'])->count() > 0))
													<a href="javascript:void(0)" class="btn {{ (Session::get('location'))?'darkbutton':'greybutton' }} btn-xs btn-transaction" onclick="addCartCustom('{{ $item['category_name'] }}','{{ $item['id'] }}','{{ $item['items_name'] }}','{{ $item['items_price'] }}','{{ $item['items_discount'] }}')" style="font-size: 11px;"><i class="fa fa-random mr-1"></i> Pesan
													</a>
												@else
													<a href="javascript:void(0)" class="btn {{ (Session::get('location'))?'darkbutton':'greybutton' }} btn-xs btn-transaction" onclick="addCart('{{ __('lang.'.$vdetail['category_name']) }}','{{ $item['id'] }}','{{ $item['items_name'] }}','{{ $item['items_price'] }}','{{ $item['items_discount'] }}')" style="font-size: 11px;"><i class="fa fa-shopping-cart mr-1"></i> Pesan</a>
												@endif
											</div>
										@else
											<div class="col-12">
												<a href="javascript:void(0)" class="btn darkbutton btn-xs btn-transaction" style="font-size: 11px;">Item tersedia</a>
											</div>
										@endif
									@else
										<div class="col-12">
											<a href="javascript:void(0)" class="btn btn-danger btn-xs btn-transaction" style="font-size: 11px;">Item tidak tersedia</a>
										</div>
									@endif
								</div>
							</div>
						</a>
					</div>
				</div>
				@endforeach
				@endif
				@endforeach
			</div>
			@endforeach
		</div>
	@endif
	</div>	
</section>

<style>
	.large-screen {}
		@media only screen and (max-width: 700px) {
		.large-screen {display: none;}
		}
		@media only screen and (min-width: 700px) {
		.large-screen {display: block;}
		}
	
	.small-screen {}
		@media only screen and (max-width: 700px) {
		.small-screen {display: block;}
		}
		@media only screen and (min-width: 700px) {
		.small-screen {display: none;}
		}
</style>
@endsection
