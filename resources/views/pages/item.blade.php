@extends('layouts.main')

@section('content')
<section class="section" id="about" style="margin-top: 120px;">
	<div class="container">{{app()->setLocale(Session::get('locale'))}}
		<div class="row mb-3">
			<div class="col-lg-2">
				@if(!empty($subcategory))
				<a href="{{ url('/menu/'.$category['category_slug'].'/'.$subcategory['subcategory_slug']) }}" class="btn mybutton"><i class="fa fa-mail-reply mr-2"></i> {{ __('lang.back')}}</a>
				@else
				<a href="{{ url('/') }}" class="btn mybutton"><i class="fa fa-mail-reply mr-2"></i> {{ __('lang.back')}}</a>
				@endif
			</div>
		</div>
		<div class="row">
			<div class="col-lg-5 col-md-5 col-xs-12">
				<!--Carousel Wrapper-->
				<div id="detail-item">
					<img src="{{ asset('/images/'.$item['item_image_primary']) }}" alt="{{ $item['items_name'] }}">
					@if(!empty($item['item_image_one']) and $item['item_image_one'] != $item['item_image_primary'])
					<img src="{{ asset('/images/'.$item['item_image_one']) }}" alt="{{ $item['items_name'] }}">
					@endif
					@if(!empty($item['item_image_two']) and $item['item_image_two'] != $item['item_image_primary'])
					<img src="{{ asset('/images/'.$item['item_image_two']) }}" alt="{{ $item['items_name'] }}">
					@endif
					@if(!empty($item['item_image_three']) and $item['item_image_three'] != $item['item_image_primary'])
					<img src="{{ asset('/images/'.$item['item_image_three']) }}" alt="{{ $item['items_name'] }}">
					@endif
					@if(!empty($item['item_image_four']) and $item['item_image_four'] != $item['item_image_primary'])
					<img src="{{ asset('/images/'.$item['item_image_four']) }}" alt="{{ $item['items_name'] }}">
					@endif
				</div>
				<!--/.Carousel Wrapper-->
			</div>
			<div class="col-lg-7 col-md-7 col-xs-12">
				<div class="left-text-content">
					<div class="section-heading">
						<h6>{{ $category['category_name'] }}</h6>
						<h2>{{ $item['items_name'] }}</h2>
						<h5>
							<sup>Rp. </sup>
							@if($item['items_discount'] > 0)
							<span class="text-danger" style="text-decoration: line-through;">{{ number_format($item['items_price']) }}</span>
							{{ number_format($item['items_price']-$item['items_discount']) }}
							@else
							{{ number_format($item['items_price']) }}
							@endif
						</h5>
						<div class="row mt-2">
							<div class="col-md-4">
								<div class="mt-1">
									@php
									$subcat = (!empty($subcategory))?$subcategory['subcategory_slug']:'0'
									@endphp
								</div>
							</div>
						</div>
					</div>
					<div class="mt-3">
						{!! $item['items_description'] !!}
					</div>
					<hr>
					<div class="row">
						@if($item['ready_stock']=='Y')
							@if(getData::getCatalogUsername(myFunction::get_username(),'feature') == 'Full')
								<div class="col-md-4 col-6 menudirectorder_hide" style="display: none">
									<button class="btn greenbutton mb-2 btn-transaction" type="button" onclick="directOrder('{{ $category['category_slug'] }}','{{ $subcat }}','{{ $item['items_name'] }}','{{ $item['items_slug'] }}')" style="font-size: 11px;"><i class="fa fa-whatsapp fa-lg mr-2"></i> Pesan Langsung</button>
								</div>
								<div class="col-md-4 col-6">
									@if((getData::getAddons($item['id'])->count() > 0))
										<a href="javascript:void(0)" class="btn {{ (Session::get('location'))?'darkbutton':'greybutton' }} btn-xs btn-transaction" onclick="addCartCustom('{{ $category['category_name'] }}','{{ $item['id'] }}','{{ $item['items_name'] }}','{{ $item['items_price'] }}','{{ $item['items_discount'] }}')" style="font-size: 11px;"><i class="fa fa-random mr-1"></i> Pesan
										</a>
									@else
										<button class="btn darkbutton mb-2 btn-transaction" type="button"  onclick="addCart('{{ $category['category_name'] }}','{{ $item['id'] }}','{{ $item['items_name'] }}','{{ $item['items_price'] }}','{{ $item['items_discount'] }}')" style="font-size: 11px;"><i class="fa fa-shopping-cart fa-lg mr-2"></i> Pesan</button>
									@endif
								</div>
							@endif
						@else
						<div class="col-12">
							<a href="javascript:void(0)" class="btn btn-danger btn-xs btn-transaction" style="font-size: 11px;">Item out of stock</a>
						</div>
						@endif
					</div>
				</div>
			</div>
		</div>
		@if(!empty($item['items_youtube']))
		<div class="row mt-5">
			<div class="col-md-12">
				<iframe class="youtubeembed" src="{{ str_replace('watch?v=','embed/',$item['items_youtube']) }}" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
			</div>
		</div>
		@endif
	</div>
</section>
@endsection
