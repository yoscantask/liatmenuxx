<!-- Modal Edit Form-->
<div class="modal" id="myModalEdit">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">{{app()->setLocale(Session::get('locale'))}}
        <b class="modal-title" id="titleModalEdit"></b>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        <input type="hidden" id="id">
        <div class="form-group">
            <input type="number" id="qtyedit" min="1" max="100">
        </div>
        <div class="form-group">
            <label>Catatan</label>
            <input type="text" id="noteedit" class="form-control">
        </div>
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-primary btn-sm" onclick="updateCart()">Save</button>
        <button type="button" class="btn btn-secondary btn-sm" onclick="showCart()">{{ __('lang.back')}}</button>
      </div>

    </div>
  </div>
</div>
<!-- End -->

<!-- Modal Bell-->

<!-- End -->

<!-- End Payment -->
<div class="modal" id="myPayment">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <!-- Modal Header -->
      <div class="modal-header">
        <b class="modal-title">{{ __('lang.orderlist')}}</b>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div>
        <div id="cartListPayment" style="padding: 10px;">
          
        </div>
      </div>
    </div>
  </div>
</div>
<!-- End -->

<!-- End Payment -->

<!-- End -->