<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="x-ua-compatible" content="ie=edge" />
        <title>{{ $title }}</title>
        <meta name="description" content="{{ $metadescription }}" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta name="_token" content="{!! csrf_token() !!}" />

        <meta property="og:locale" content="id_ID" />
        <meta property="og:type" content="article" />
        <meta property="og:title" content="{{ $title }}" />
        <meta property="og:url" content="{{ \Request::url() }}" />
        <meta property="og:image" content="{{ $metaimage }}" />
        <meta property="og:site_name" content="{{ getData::getCatalogUsername(myFunction::get_username(),'catalog_title') }}" />
        <meta property="og:description" content="{{ $metadescription }}" />

        <link rel="canonical" href="{{ \Request::url() }}" />

        <link rel="manifest" href="site.webmanifest" />
        <link rel="apple-touch-icon" href="icon.png') }}" />
        <!-- Place favicon.ico in the root directory -->

        <link href="https://fonts.googleapis.com/css?family=Poppins:100,200,300,400,500,600,700,800,900&display=swap" rel="stylesheet" />
        <link href="https://fonts.googleapis.com/css2?family=Dancing+Script:wght@400;500;600;700&display=swap" rel="stylesheet" />

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.3/css/bootstrap.min.css" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css" />
        <link rel="stylesheet" type="text/css" href="{{ asset('/css/font-awesome.css?'.time()) }}" />
        <link rel="stylesheet" href="{{ asset('/css/templatemo-klassy-cafe.css?'.time()) }}" />
        <link rel="stylesheet" href="{{ asset('/css/owl-carousel.css') }}" />
        <link rel="stylesheet" href="{{ asset('/css/lightbox.css') }}" />
        <style type="text/css">
            @media (max-width: 500px) {
                #top .left-content h4 {
                    font-size: 1.5rem;
                }
                #top .left-content h6 {
                    font-size: 11px;
                }
                .section-heading h6 {
                    margin-top: 30px;
                }
                .section-heading h2 {
                    font-size: 1.3rem;
                }
                #about .section-heading h2 {
                    padding-right: 0;
                }
                .youtubeembed {
                    width: 100%;
                    height: auto;
                }
                .dots span {
                    padding: 0;
                }
                .dots::after {
                    display: none;
                }
            }
        </style>
        <script src="{{ asset('/js/pusher.min.js') }}"></script>
        <script src="{{ asset('/js/echo.js') }}"></script>
    </head>
    <body>
        <!-- ***** Preloader Start ***** -->
        <div id="preloader">
            <div class="jumper">
                <div></div>
                <div></div>
                <div></div>
            </div>
        </div>
        <!-- ***** Preloader End ***** -->

        <!-- ***** Header Area Start ***** -->
        <header class="header-area header-sticky">{{app()->setLocale(Session::get('locale'))}}
            <div class="container">
                <div class="row">
                    <div class="col-12 text-center">
                        <div class="mt-3">
                            <a href="{{ url('/register') }}" class="logo">
                                <img src="{{ asset('/images/klassy-logo.png') }}" alt="{{ getData::getCatalogUsername(myFunction::get_username(),'catalog_title') }}" />
                            </a>
                        </div>
                        <div id="wrapcart" class="main-white-button mt-2 d-none">
                            <a href="javascript:void(0)" onclick="showCart()" id="cartStatus"><i class="fa fa-shopping-cart fa-lg mr-2"></i> <span id="countcart"></span></a>
                            <a href="javascript:void(0)" onclick="showCart()" id="orderStatus" class="d-none"></a>
                        </div>
                    </div>
                </div>
                <div id="demo"></div>
            </div>
        </header>
        <!-- ***** Header Area End ***** -->

        <section class="section" id="chefs" style="margin-top:100px;">
            <div class="container">
              <div class="row">
                <div class="col-md-8 offset-md-2">
                  <div class="section-heading text-center">
                    <h6>{{ __('lang.welcome')}} LiatMenu</h6>
                    <h2>Register Now</h2>
                  </div>
                  <form action="{{ url('/register') }}" method="post">
                    {!! csrf_field() !!}
                    <input type="hidden" id="affiliate" name="affiliate" value="{{ $affiliate }}" />
                    @if($affiliate != 'None')
                    <div class="mt-3">
                      <label>Affiliate</label>
                      <input type="text" class="form-control" disabled value="{{ $affiliate }}" />
                    </div>
                    @endif
                    <div class="row mt-3">
                      <div class="col-md-4 mt-3">
                        <label>{{ __('lang.name')}}</label>
                        <input type="text" id="name" name="name" class="form-control" />
                        <span class="text-danger">{{ $errors->first('name') }}</span>
                      </div>
                      <div class="col-md-4 mt-3">
                        <label>Email</label>
                        <input type="text" id="email" name="email" class="form-control" />
                        <span class="text-danger">{{ $errors->first('email') }}</span>
                      </div>
                      <div class="col-md-4 mt-3">
                        <label>{{ __('lang.phone')}}</label>
                        <input type="text" id="phone" name="phone" class="form-control" />
                        <span class="text-danger">{{ $errors->first('phone') }}</span>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6 col-sm-6 mt-3">
                        <label>Password</label>
                        <input type="password" id="password" name="password" class="form-control" />
                        <span class="text-danger">{{ $errors->first('password') }}</span>
                      </div>
                      <div class="col-md-6 col-sm-6 mt-3">
                        <label>Re Password</label>
                        <input type="password" id="repassword" name="repassword" class="form-control" />
                        <span class="text-danger">{{ $errors->first('repassword') }}</span>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-3 col-sm-6 mt-3">
                        <label>{{ __('lang.package')}}</label>
                        <select id="package" name="package" class="form-control" style="padding:0;height:42px;" onchange="loadPackage()">
                          <option value="">
                            {{ __('lang.select')}}
                          </option>
                          @foreach($package as $package)
                            <option value="{{ $package['id'] }}">
                              {{ $package['package_name'] }}
                            </option>
                          @endforeach
                        </select>
                        <span class="text-danger">{{ $errors->first('package') }}</span>
                      </div>
                      <div class="col-md-3 col-sm-6 mt-3">
                        <label>{{ __('lang.price')}}</label>
                        <input type="text" id="price" name="price" class="form-control text-right" readonly />
                      </div>
                      <div class="col-md-3 col-sm-6 mt-3">
                        <label>Period ({{ __('lang.month')}})</label>
                        <input type="text" id="expired" name="expired" class="form-control text-center" readonly />
                      </div>
                      <div class="col-md-3 col-sm-6 mt-3">
                        <label>{{ __('lang.numof')}} Catalog</label>
                        <input type="text" id="cataog" name="cataog" class="form-control text-center" readonly />
                      </div>

                    </div>
                    <div class="form-group text-center mt-5">
                      <button type="submit" class="btn mybutton" style="width:auto">Register</button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
        </section>

        <!-- ***** Reservation Us Area Starts ***** -->
        <section class="section" id="reservation">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 offset-md-2 align-self-center">
                        <div class="text-center">
                            <div class="section-heading">
                                <h2>Here You Can Make A Reservation Or Just walkin to our cafe</h2>
                            </div>
                            <p>Donec pretium est orci, non vulputate arcu hendrerit a. Fusce a eleifend riqsie, namei sollicitudin urna diam, sed commodo purus porta ut.</p>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="phone">
                                        <i class="fa fa-phone"></i>
                                        <h4>Phone Numbers</h4>
                                        <span>
                                            <a href="#">080-090-0990</a><br />
                                            <a href="#">080-090-0880</a>
                                        </span>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="message">
                                        <i class="fa fa-envelope"></i>
                                        <h4>Emails</h4>
                                        <span>
                                            <a href="#">hello@company.com</a><br />
                                            <a href="#">info@company.com</a>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- ***** Reservation Area Ends ***** -->

        <!-- ***** Footer Start ***** -->
        <footer style="margin-top: 0;">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 col-xs-12">
                        <div class="right-text-content">
                            <ul class="social-icons">
                                <li>
                                    <a href="#"><i class="fa fa-facebook"></i></a>
                                </li>
                                <li>
                                    <a href="#"><i class="fa fa-twitter"></i></a>
                                </li>
                                <li>
                                    <a href="#"><i class="fa fa-linkedin"></i></a>
                                </li>
                                <li>
                                    <a href="#"><i class="fa fa-instagram"></i></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="logo">
                            <a href="index.html">LiatMenu</a>
                        </div>
                    </div>
                    <div class="col-lg-4 col-xs-12">
                        <div class="left-text-content">
                            <p>© Copyright LiatMenu. Powered by : LiatAja</p>
                        </div>
                    </div>
                </div>
            </div>
        </footer>

        @include('pages.modal')

        <!-- jQuery -->
        <script src="{{ asset('/js/jquery-2.1.0.min.js') }}"></script>

        <!-- Bootstrap -->
        <script src="{{ asset('/js/popper.js') }}"></script>
        <script src="{{ asset('/js/bootstrap.min.js') }}"></script>

        <!-- Plugins -->
        <script src="{{ asset('/js/owl-carousel.js') }}"></script>
        <script src="{{ asset('/js/accordions.js') }}"></script>
        <script src="{{ asset('/js/datepicker.js') }}"></script>
        <script src="{{ asset('/js/scrollreveal.min.js') }}"></script>
        <script src="{{ asset('/js/waypoints.min.js') }}"></script>
        <script src="{{ asset('/js/jquery.counterup.min.js') }}"></script>
        <script src="{{ asset('/js/imgfix.min.js') }}"></script>
        <script src="{{ asset('/js/slick.js') }}"></script>
        <script src="{{ asset('/js/lightbox.js') }}"></script>
        <script src="{{ asset('/js/isotope.js') }}"></script>

        <script src="https://cdn.jsdelivr.net/npm/bootstrap-input-spinner@1.9.7/src/bootstrap-input-spinner.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
        <!-- Global Init -->
        <script src="{{ asset('/js/custom.js') }}"></script>
        @toastr_js
        @toastr_render
        <script>
            $.ajaxSetup({
                headers: {
                    "X-CSRF-Token": $("meta[name=_token]").attr("content"),
                },
            });
            function loadPackage(){
              var id = $("#package").val();
              $.ajax({
                url: "{{ url('/getpackage') }}"+'/'+id,
                type: 'GET',
              })
              .done(function(data) {
                $("#price").val(data.price);
                $("#expired").val(data.duration);
                $("#cataog").val(data.catalog);
              })
              .fail(function() {
                console.log("error");
              });
            }
        </script>
    </body>
</html>
