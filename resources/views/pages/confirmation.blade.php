@php
$grand = 0;
$itemgroup= [];
@endphp
<!-- Modal body -->
<div id="wrapConfirmation">{{app()->setLocale(Session::get('locale'))}}
	<form id="myForm" @submit.prevent="submitForm" method="post" onsubmit="return false;" enctype="multipart/form-data">
		<div class="modal-body">
			@foreach($item as $item)
			<p><b>{{ $item['category'] }}</b></p>
			<div class="ml-2">
				<div class="table-responsive">
					<table style="width: 100%">
						@php
							$total = 0;
						@endphp
						@foreach(getData::getItemCart($item['category']) as $listitem)
							@php
								$price = ($listitem['price']-$listitem['discount']) * $listitem['qty'];
								$total = $total + $price;
								$itemgroup[]= $listitem['item'].' x '.$listitem['qty'];
							@endphp
						<tr>
							<td style="width: 50%" style="border-top: none" class="align-top">
								<p style="line-height: 35px;font-size: 12px">
									{{ $listitem['item'] }}
								</p>
							</td>
							<td style="width:20%;border-top: none" class="text-center align-top">
								<p style="line-height: 35px;font-size: 12px">
									x {{ $listitem['qty'] }}
								</p>
							</td>
							<td style="width:30%;border-top: none" class="text-right align-top">
								<p style="line-height: 35px;font-size: 12px">
									{{ number_format($price) }}
								</p>
							</td>
						</tr>
						@if(!empty($listitem['note']))
							<tr>
								<td style="width:100%;" class="align-top" colspan="3">
									<small class="text-muted" style="font-size: .7rem;font-weight: bold;">* {{ __('lang.notes')}} : {{ $listitem['note'] }}</small>
								</td>
							</tr>
						@endif
						@endforeach
						@php
							$grand = $grand +  $total;
						@endphp
					</table>
				</div>
			</div>
			@endforeach
			@php
				$gettax = ($grand*getData::getCatalogUsername(myFunction::get_username(),'tax')) /100;
			@endphp
			@if(getData::getCatalogUsername(myFunction::get_username(),'tax') > 0)
			<table style="width: 100%" class="mt-3">
				<tr>
					<td style="width: 50%" style="font-size: 12px;border-top: 1px dashed #CCC;border-bottom: 1px dashed #CCC">
						<p style="line-height: 35px;font-size: 12px">
							( Extra ) PPN {{ getData::getCatalogUsername(myFunction::get_username(),'tax') }}%
						</p>
					</td>
					<td style="width: 50%" style="font-size: 12px;border-top: 1px dashed #CCC;border-bottom: 1px dashed #CCC" class="text-right">
						<p style="line-height: 35px;font-size: 12px">
							{{ number_format($gettax) }}
						</p>
					</td>
				</tr>
			</table>
			@endif
			<table style="width: 100%" class="mt-3">
				<tr>
					<td class="text-right" colspan="3" style="font-size: 12px;border-top: 1px dashed #CCC;border-bottom: 1px dashed #CCC">
						<p style="padding: 15px 0;">
							<b>{{ number_format($grand+$gettax) }}</b>
						</p>
					</td>
				</tr>
			</table>
			<div class="form-group mt-3">
				<div class="row">
					<div class="col">
						<label>{{ __('lang.tablenumsample')}}</label>
						<input type="text" id="position" name="position" class="form-control" value="{{ $position }}" {{ (Session::has('myorder'))?'readonly':'' }} >
					</div>
					<div class="col">
						<label>Upload {{ __('lang.proofpayment')}}</label>
						<input type="file" id="transfer_image" name="transfer_image" class="form-control form-control-sm" style="padding: 10px 5px 15px 5px;line-height: 21px;height:42px">
						<span v-if="formErrors['transfer_image']" class="errormsg">@{{ formErrors['transfer_image'][0] }}</span>
					</div>
				</div>
			</div>
			<span id="wacontent" style="visibility: hidden;font-size: 0.1px">
				@foreach($itemgroup as $itemselect)
				{{ $itemselect }}%0a
				@endforeach
			</span>
		</div>

		<!-- Modal footer -->
		<div class="modal-footer">
			<button type="submit" class="btn btn-primary btn-sm">Upload {{ __('lang.proofpayment')}}</button>
			<button type="button" class="btn btn-secondary btn-sm" v-on:click="showCart">{{ __('lang.back')}}</button>
		</div>
	</form>
</div>

<script type="text/javascript">
    new Vue({
        el: "#wrapConfirmation",
        data() {
            return {
                csrf: "",
                formErrors: {},
                notif: [],
            };
        },
        mounted: function () {
            this.csrf = "{{ csrf_token() }}";
            let self = this;
        },
        methods: {
            submitForm: function (e) {
            	if($("#position").val() == ''){
                    Swal.fire("Ops!", "{{ __('lang.plisinsert')}}", "error");
                    return false;
                }

                var form = e.target || e.srcElement;
                var action = "{{ url('/cart/confirmationtransfer') }}";
                var csrfToken = "{{ csrf_token() }}";

                let datas = new FormData();
                datas.append('position', $("#position").val());
                datas.append('transfer_image', document.getElementById('transfer_image').files[0]);
                datas.append('payment_method', 2);
                datas.append('tax', "{{ getData::getCatalogUsername(myFunction::get_username(),'tax') }}");
                axios.post(action, datas, {
                        headers: {
                            "X-CSRF-TOKEN": csrfToken,
                            Accept: "application/json",
                        },
                    })
                    .then((response) => {
                        let self = this;
                        var notif = response.data;
                        var getstatus = notif.status;
                        if (getstatus == "success") {
                        	$("#orderModal").modal("hide");
                        	$("#qtyTemp").val(0)
                        	if ("{{ getData::getCatalogUsername(myFunction::get_username(),'advance_payment') == 'Y' }}") {
                        		Swal.fire("Informasi", "{{ __('lang.thxconfirmpayment')}}", "success").then((result) => {
                        		  if (result.value) {
                        		    countCart();
                        		  }
                        		});
                        	}else{
                        		Swal.fire("Informasi", "{{ __('lang.plispaymentofficer')}}", "success").then((result) => {
                        		    if (result.value) {
                        		        countCart();
                        		    }
                        		});
                        	}
                        }
                    })
                    .catch((error) => {
                        $('.errormsg').css('visibility','visible');
                        this.formErrors = error.response.data.errors;
                    });
            },
            showCart:function(){
            	$("#orderModal").modal('hide');
            	showOrder();
            }
        },
    });
</script>
