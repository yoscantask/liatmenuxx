@php
$grand = 0;
$itemgroup= [];
@endphp
<!-- Modal body -->
<div class="modal-body">{{app()->setLocale(Session::get('locale'))}}
	@if($item->count() > 0)
		@foreach($item as $item)
		<p><b>{{ $item['category'] }}</b></p>
		<div class="ml-2">
			<div class="table-responsive">
				<table style="width: 100%">
					@php
						$total = 0;
						$totaladdons = 0;
					@endphp
					@foreach(getData::getItemCart($item['category']) as $listitem)
						@php
							$price = ($listitem['price']-$listitem['discount']) * $listitem['qty'];
							$total = $total + $price;
							$itemgroup[]= $listitem['item'].' x '.$listitem['qty'];
						@endphp
						<tr>
							<td style="width: 50%" style="border-top: none" class="align-top">
								<p style="line-height: 35px;font-size: 12px">
									@if($listitem['item_status'] == 'Order')
										@if(getData::getInvoiceAddons($listitem['id'])->count() > 0)
											<a  href="javascript:void(0)" onclick="editNote({{ $listitem['id'] }})"><i class="fa fa-pencil mr-1 text-success"></i> </a>
										@else
											@if(getData::getCatalogUsername(myFunction::get_username(),'advance_payment') == 'N')
												<a  href="javascript:void(0)" onclick="editNote({{ $listitem['id'] }})"><i class="fa fa-pencil mr-1 text-success"></i> </a>
											@else
												<a  href="javascript:void(0)" onclick="editItem({{ $listitem['id'] }})"><i class="fa fa-pencil mr-1 text-success"></i> </a>
											@endif
											
										@endif
										<a  href="javascript:void(0)" onclick="removeItem({{ $listitem['id'] }})"><i class="fa fa-remove mr-1" style="color: red"></i> </a>
									@endif
									{{ $listitem['item'] }}
								</p>
							</td>
							<td style="width:20%;border-top: none" class="text-center align-top">
								<p style="line-height: 35px;font-size: 12px">
									x {{ $listitem['qty'] }}
								</p>
							</td>
							<td style="width:30%;border-top: none" class="text-right align-top">
								<p style="line-height: 35px;font-size: 12px">
									{{ number_format($price) }}
								</p>
							</td>
						</tr>
						@if(!empty($listitem['note']))
							<tr>
								<td style="width:100%;" class="align-top" colspan="3">
									<small class="text-muted" style="font-size: .7rem;font-weight: bold;">* {{ __('lang.notes')}} : {{ $listitem['note'] }}</small>
								</td>
							</tr>
						@endif

						@if(getData::getInvoiceAddons($listitem['id'])->count() > 0)
							@foreach(getData::getInvoiceAddons($listitem['id']) as $addondata)
								@php
									$priceaddons = $addondata['addon_qty']*getData::addonsPrice($addondata['single_addon'],$addondata['multiple_addon']);
									$totaladdons = $totaladdons+$priceaddons;
								@endphp
								<tr>
									<td style="width: 50%" style="border-top: none;" class="align-top">
										<p style="line-height: 35px;font-size: 12px;padding-left: 30px;color: #999">
											@if($listitem['item_status'] == 'Order')
												<a  href="javascript:void(0)" onclick="editAddon('{{ $listitem['item_id'] }}','{{ $listitem['id'] }}','{{ $addondata['row_group'] }}','{{ $addondata['addon_qty'] }}')"><i class="fa fa-pencil mr-1 text-success"></i> </a>
												<a  href="javascript:void(0)" onclick="removeAdd('{{ $listitem['id'] }}','{{ $addondata['row_group'] }}')"><i class="fa fa-remove mr-1" style="color: red"></i> </a>
											@endif
											{{ getData::decodeAddons($addondata['single_addon']) }} 
											{{ (!empty($addondata['multiple_addon']) && !empty(getData::decodeAddons($addondata['single_addon'])))?'|':'' }} 
											{{ getData::decodeAddons($addondata['multiple_addon']) }}
										</p>
									</td>
									<td style="width:20%;border-top: none" class="text-center align-top">
										<p style="line-height: 35px;font-size: 12px;color: #999">
											x {{ $addondata['addon_qty'] }}
										</p>
									</td>
									<td style="width:30%;border-top: none" class="text-right align-top">
										<p style="line-height: 35px;font-size: 12px;color: #999">
											{{ number_format($priceaddons) }} 
										</p>
									</td>
								</tr>
							@endforeach
						@endif
						@if(($listitem['qty'] - getData::getInvoiceAddonsSum($listitem['id'])) > 0 && getData::getAddons($listitem['item_id'])->count() > 0)
							<tr>
								<td style="width: 50%" style="border-top: none;" class="align-top">
									<p style="line-height: 35px;font-size: 12px;padding-left: 30px;color: #999">
										@if($listitem['item_status'] == 'Order')
											<a  href="javascript:void(0)" onclick="editAddon('{{ $listitem['item_id'] }}','{{ $listitem['id'] }}','0','{{ $listitem['qty'] - getData::getInvoiceAddonsSum($listitem['id']) }}')"><i class="fa fa-pencil mr-1 text-success"></i> </a>
										@endif
										No Add Ons
									</p>
								</td>
								<td style="width:20%;border-top: none" class="text-center align-top">
									<p style="line-height: 35px;font-size: 12px;color: #999">
										x {{ $listitem['qty'] - getData::getInvoiceAddonsSum($listitem['id']) }}
									</p>
								</td>
								<td style="width:30%;border-top: none" class="text-right align-top">
									<p style="line-height: 35px;font-size: 12px;color: #999">
										0
									</p>
								</td>
							</tr>
						@endif
					@endforeach
					@php
						$grand = $grand +  $total + $totaladdons;
					@endphp
				</table>
			</div>
		</div>
		@endforeach
		@php
			$gettax = ($grand*getData::getCatalogUsername(myFunction::get_username(),'tax')) /100;
		@endphp

		@if(getData::getCatalogUsername(myFunction::get_username(),'tax') > 0 && getData::getItemCart($item['category'])->count() > 0)
		<table style="width: 100%" class="mt-3">
			<tr>
				<td style="width: 50%" style="font-size: 12px;border-top: 1px dashed #CCC;border-bottom: 1px dashed #CCC">
					<p style="line-height: 35px;font-size: 12px">
						( Extra ) PPN {{ getData::getCatalogUsername(myFunction::get_username(),'tax') }}%
					</p>
				</td>
				<td style="width: 50%" style="font-size: 12px;border-top: 1px dashed #CCC;border-bottom: 1px dashed #CCC" class="text-right">
					<p style="line-height: 35px;font-size: 12px">
						{{ number_format(ceil($gettax)) }}
					</p>
				</td>
			</tr>
		</table>
		@endif
		<input type="hidden" id="tax" value="{{ getData::getCatalogUsername(myFunction::get_username(),'tax') }}">
		<input type="hidden" id="grand" value="{{ $grand }}">
		<table style="width: 100%" class="mt-3">
			<tr>
				<td class="text-right" style="font-size: 12px;border-top: 1px dashed #CCC;border-bottom: 1px dashed #CCC">
					<p style="padding: 15px 0;">
						<b>{{ number_format($grand+$gettax) }}</b>
					</p>
				</td>
			</tr>
		</table>

		@if($invoice['status'] == 'Order')
		<div class="form-group mt-3">
			<div class="row">
				<div class="col-md-6">
					<label>{{ __('lang.tablenumsample')}}</label>
					<input type="text" id="position" name="position" class="form-control" autocomplete="off">
				</div>
				@if(getData::getCatalogUsername(myFunction::get_username(),'advance_payment') == 'Y')
					<div class="col-md-6">
						<label>{{ __('lang.paymentmethode')}}Pilih Metode Pembayaran</label>
						<select id="paymentmethod" class="form-control" style="padding: 0 10px;height: 42px;" onchange="paymentAction()">
							<option value="1">{{ __('lang.payonkasir')}}Bayar di Kasir</option>
							@if(getData::getCatalogUsername(myFunction::get_username(),'transfer_payment') == 'Y')
							<option value="2">Bank Transfer</option>
							@endif
							@if(getData::getCatalogUsername(myFunction::get_username(),'payment_gateway') == 'Y')
							<option value="3">Payment Gateway</option>
							@endif
						</select>
					</div>
				@endif
			</div>
		</div>
		@endif
		@if($invoice['status'] == 'Order' && getData::getCatalogUsername(myFunction::get_username(),'transfer_payment') == 'Y')
		<div id="transferinfo" style="width: 100%;position: relative;background: #F5F5F5;border: 1px solid #DDD;padding: 10px;" class="d-none">
			<p><b>{{ __('lang.information')}}</b></p>
			<p style="font-size: .7rem">{{ __('lang.ucanpaytransfer')}}<b>{!! getData::getCatalogUsername(myFunction::get_username(),'bank_info') !!}</b>.</p>
		</div>
		@endif
		<span id="wacontent" style="display: none;font-size: 0.1px">
			@foreach($itemgroup as $itemselect)
			{{ $itemselect }}%0a
			@endforeach
		</span>
	@else
		<label>{{ __('lang.uchartempty')}}</label>
	@endif
</div>

<!-- Modal footer -->
<div class="modal-footer">
	@if($invoice['status'] == 'Order' && $grand > 0)
		<button type="button" class="btn btn-primary btn-sm" onclick="{{ (getData::getCatalogUsername(myFunction::get_username(),'advance_payment') == 'Y')?'paymentMethod':'checkout' }}()">Checkout</button>
		<button type="button" class="btn btn-danger btn-sm" onclick="cancelOrder()">{{ __('lang.cancelorder')}}</button>
	@endif
	<button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">{{ __('lang.close')}}</button>
</div>


