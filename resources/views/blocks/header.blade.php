<header class="header-area header-sticky">
    <div class="container">
        <div class="row">
            <div class="col-12 headerwrap">
                <div class="row">
                    <div class="col">
                        <a href="{{ url('/') }}" class="logo">
                            <img src="{{ asset('/images'.getData::getCatalogUsername(myFunction::get_username(),'catalog_logo')) }}" alt="{{ getData::getCatalogUsername(myFunction::get_username(),'catalog_title') }}">
                        </a>
                    </div>
                    <div class="row mr-1" style="align-content: flex-end">
                        {{app()->setLocale(Session::get('locale'))}}
                        @if (__('lang.idlang') == 'id')            
                                <form action="{{ url('/lang') }}" method="get">
                                    <input id="bhs" name="bhs" type="text" value="id" hidden/>
                                    <button style="width: 40px;height: 25px;margin-bottom: 3px;" type="submit" class="mr-1 btn btn-outline-dark btn-sm">ID</button>
                                </form>
                                <form action="{{ url('/lang') }}" method="get">
                                    <input id="bhs" name="bhs" type="text" value="en" hidden/>
                                    <button style="width: 40px;height: 25px;margin-bottom: 3px" disabled type="submit" class="btn btn-dark btn-sm">EN</button>
                                </form>                
                        @else
                                <form action="{{ url('/lang') }}" method="get">
                                    <input id="bhs" name="bhs" type="text" value="id" hidden/>
                                    <button style="width: 40px;height: 25px;margin-bottom: 3px" disabled type="submit" class="mr-1 btn btn-dark btn-sm">ID</button>
                                </form>
                                <form action="{{ url('/lang') }}" method="get">
                                    <input id="bhs" name="bhs" type="text" value="en" hidden/>
                                    <button style="width: 40px;height: 25px;margin-bottom: 3px" type="submit" class="btn btn-outline-dark btn-sm">EN</button>
                                </form> 
                        @endif
                    </div>
                </div>
                @if(getData::getCatalogUsername(myFunction::get_username(),'feature') == 'Full')
                    @if(!Session::has('location'))
                        <div class="floattrack text-center" style="background: {{ getData::getCatalogUsername(myFunction::get_username(),'theme_color') }}">
                            @if(getData::getCatalogUsername(myFunction::get_username(),'customer_data') == 'Y')
                                <a href="javascript:void(0)" style="text-transform: uppercase;color:#EEE" onclick="setCustomer('accessmenu')"><i class="fa fa-lock fa-lg mr-1"></i> {{ __('lang.menuaccess')}} </a>
                            @else
                                <a href="javascript:void(0)" style="text-transform: uppercase;color:#EEE" data-toggle="modal" data-target="#modalAccess"><i class="fa fa-lock fa-lg mr-1"></i>  {{ __('lang.menuaccess')}} </a>
                            @endif
                        </div>
                    @else
                        <div id="wrapcart" class="floattrack text-center d-none">
                            <input type="hidden" id="qtyTemp">
                            <a href="javascript:void(0)" onclick="showOrder()" id="cartStatus" style="color: #666">
                                <i class="fa fa-shopping-cart fa-lg mr-1"></i> <span id="countcart"></span>
                            </a>
                            <a href="javascript:void(0)" onclick="showOrder()" id="orderStatus" class="d-none" style="color: #666"></a>
                            <a href="javascript:void(0)" onclick="printOrder()" id="printOrder" class="d-none" style="background: #666;color:#FFF;padding: 5px 7px;border-radius: 3px;"><i class="fa fa-print"></i> Cetak</a>
                        </div>
                    @endif
                @endif
                </div>
            </div>
        </div>
        <div id="demo"></div>
    </div>
</header>

<style>
	.large-screen {}
		@media only screen and (max-width: 700px) {
		.large-screen {display: none;}
		}
		@media only screen and (min-width: 700px) {
		.large-screen {display: block;}
		}
	
	.small-screen {}
		@media only screen and (max-width: 700px) {
		.small-screen {display: block;}
		}
		@media only screen and (min-width: 700px) {
		.small-screen {display: none;}
		}
</style>