<section class="section" id="reservation">{{app()->setLocale(Session::get('locale'))}}
    <div class="container">
        <div class="row">
            <div class="col-md-8 offset-md-2 align-self-center">
                <div class="text-center">
                    <div class="section-heading">
                        <h2>{{ __('lang.contactus')}}</h2>
                    </div>
                    <p>{{ __('lang.pliscallme')}}</p>
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="phone">
                                <i class="fa fa-phone"></i>
                                <h4>{{ __('lang.phone')}}</h4>
                                <span>{{ getData::getCatalogUsername(myFunction::get_username(),'phone_contact') }}</span>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="message">
                                <i class="fa fa-envelope"></i>
                                <h4>Emails</h4>
                                <span><a href="#">{{ getData::getCatalogUsername(myFunction::get_username(),'email_contact') }}</a></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<footer style="margin-top: 0">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-xs-12">
                <div class="right-text-content">
                        <ul class="social-icons">
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                            <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                        </ul>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="logo">
                    <a href="{{ url('/') }}">{{ getData::getCatalogUsername(myFunction::get_username(),'catalog_title') }}</a>
                </div>
            </div>
            <div class="col-lg-4 col-xs-12">
                <div class="left-text-content">
                    <p>© Copyright {{ getData::getCatalogUsername(myFunction::get_username(),'catalog_title') }}.
                    <br>Powered by : <a href="https://yoscan.id/" target="_blank" style="color:white;text-decoration: underline;">YoScan</a></p>
                </div>
            </div>
        </div>
    </div>
</footer>