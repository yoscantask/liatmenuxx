<?php
use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;

//language
Route::get('/lang', function(Request $request) {	
	\Session::put('locale', $request->bhs);
	app()->setLocale(Session::get('locale'));
	return redirect()->back();
});

//Route::get('/email','CatalogController@sendEmail');
Route::any('/location/{lat?}/{lon?}', 'CatalogController@setlocation');
Route::post('/bell', 'CatalogController@setBell');
Route::post('/customer', 'CatalogController@saveCustomer');
Route::post('/customerupdate', 'CatalogController@updateCustomer');
Route::get('/close-session', 'CatalogController@closesession');
//Route::any('/register/{affiliate?}', 'RegisterController@register');
Route::get('/getpackage/{id?}', 'RegisterController@getpackage');
Route::get('/testingpusher/{param1?}/{param2?}/{param3}', 'CatalogController@sendNotifTransaction');

Route::group(['prefix'=>'payment'],function () {
	Route::get('/snap/{total?}','PaymentController@token');
	Route::any('/finish','PaymentController@finishPayment');
});

Route::group(['prefix'=>'cart'],function () {
	Route::get('/count', 'CatalogController@countcart');
	Route::get('/list', 'CatalogController@cartlist');
	Route::get('/payment', 'CatalogController@cartpayment');
	Route::post('/confirmation', 'CatalogController@confirmation');
	Route::post('/confirmationtransfer', 'CatalogController@confirmationtransfer');
	Route::post('/checkout', 'CatalogController@checkout');
	Route::get('/completelastcheckout', 'CatalogController@completelastcheckout');
	Route::get('/addons/{item?}/{group?}', 'CatalogController@addons');
	Route::get('/show-item/{id?}', 'CatalogController@show');
	Route::get('/delete-item/{id?}', 'CatalogController@deleteitem');
	Route::get('/delete-addon/{detail?}/{group?}', 'CatalogController@deleteaddon');
	Route::get('/delete', 'CatalogController@deletecart');
	Route::post('/save', 'CatalogController@savecart');
	Route::post('/update', 'CatalogController@updatecart');
	Route::post('/updateaddons', 'CatalogController@updatecartaddons');
	Route::get('/status', 'TransactionController@checkstatus');
	Route::post('/notif', 'CatalogController@notif');
	Route::get('/struk/{inv?}/{download?}', 'CatalogController@generateStruk');
});

// if(\Request::getHttpHost()=='localhost' || \Request::getHttpHost()=='192.168.100.132'){
// 	Route::get('/menu/{category?}/{subcategory?}','CatalogController@menu');
// 	Route::get('/menu/{category?}/{subcategory?}/{item?}','CatalogController@item');
// 	Route::get('/item/{category?}/{item?}','CatalogController@itemdirect');
// 	Route::get('/','CatalogController@index');
// }else{
// 	Route::group(['domain' => '{account}.liatmenu.id'], function() {
// 	    Route::get('/menu/{category?}/{subcategory?}','CatalogController@menu');
// 	    Route::get('/menu/{category?}/{subcategory?}/{item?}','CatalogController@item');
// 	    Route::get('/item/{category?}/{item?}','CatalogController@itemdirect');
// 	    Route::get('/','CatalogController@index');
// 	});
// 	Route::group(['domain' => '{account}.yoscan.id'], function() {
	    // Route::get('/menu/{category?}/{subcategory?}','CatalogController@menu');
	    // Route::get('/menu/{category?}/{subcategory?}/{item?}','CatalogController@item');
	    // Route::get('/item/{category?}/{item?}','CatalogController@itemdirect');
	    // Route::get('/','CatalogController@index');
// 	});
// }

Route::middleware(['valid_domain'])->group(function () {
	Route::get('/menu/{category?}/{subcategory?}','CatalogController@menu');
	Route::get('/menu/{category?}/{subcategory?}/{item?}','CatalogController@item');
	Route::get('/item/{category?}/{item?}','CatalogController@itemdirect');
	Route::get('/','CatalogController@index');
});