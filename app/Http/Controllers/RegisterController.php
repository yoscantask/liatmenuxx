<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use App\Models\Package;

use App\Helper\myFunction;
use App\Helper\getData;

use Session;

class RegisterController extends Controller
{
    public function register(Request $request,$affiliate=null){
      if($request->isMethod('post')){
        $this->validate($request, [
            'name' => 'required|min:3',
            'email' => 'required|email',
            'phone' => 'required|min:10',
            'password' => 'required|min:5',
            'repassword' => 'required_with:password|same:password',
            'package' => 'required',
            'period' => 'min:1|max:12',
        ]);
        if(User::save_data($request)){
            $status='success';
            $message='Your request was successful.';
        }else{
            $status='error';
            $message='Oh snap! something went wrong.';
        }
        toastr()->$status($message);
        return redirect('/register');
      }else{
        $data['affiliate'] = (!empty($affiliate))?$affiliate:'None';
        $data['title']="Register";
        $data['metadescription'] = "Register";
        $data['metaimage'] = "";
        $data['package'] = Package::get();
        return view('pages.register',$data);
      }
    }
    public function getpackage($id=null){
      $query = Package::where('id',$id)->first();
      return $query;
    }
}
