<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Invoice;
use App\Models\InvoiceDetail;
use App\Models\InvoiceAddons;

use myFunction;
use getData;
use Session;

class TransactionController extends Controller
{
    public function checkstatus()
    {
        $invoicedata = Invoice::where('invoice_number', Session::get('myorder'))->first();
        return $invoicedata;
    }
}
