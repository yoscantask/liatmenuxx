<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Category;
use App\Models\SubCategory;
use App\Models\Catalog;
use App\Models\CatalogDetail;
use App\Models\Items;
use App\Models\ItemsDetail;
use App\Models\Invoice;
use App\Models\InvoiceDetail;
use App\Models\InvoiceAddons;
use App\Models\Bell;
use App\Models\Customer;

use myFunction;
use getData;

use Session;
use Hash;

use App\Events\NotifEvent;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

use Image;
use Mail;

class CatalogController extends Controller
{
    public function index($username = null)
    {
        $username = myFunction::get_username();
        if (!empty($username)) {
            $data['getProfile'] = Catalog::where('catalog_username', trim($username))->first();
            if (!empty($data['getProfile'])) {
                if ($data['getProfile']['show_catalog'] == 'Open') {
                    $data['detail'] = CatalogDetail::select('catalogdetail.*', 'category.category_name', 'category.category_slug', 'subcategory.subcategory_slug', 'subcategory.subcategory_name', 'subcategory.subcategory_image')
                        ->leftJoin('category', 'catalogdetail.category_id', '=', 'category.id')
                        ->leftJoin('subcategory', 'catalogdetail.subcategory_id', '=', 'subcategory.id')
                        ->where('catalog_id', $data['getProfile']['id'])
                        ->orderBy('category_position')
                        ->groupBy('category_id')
                        ->get();
                    $data['title'] = $data['getProfile']['catalog_title'];
                    $data['metadescription'] = "Selamat Datang di " . $data['getProfile']['catalog_title'];
                    $data['metaimage'] = asset('/images' . getData::getCatalogUsername(myFunction::get_username(), 'catalog_logo'));
                    return view('pages.home', $data);
                } else {
                    $data['title'] = $data['getProfile']['catalog_title'];
                    $data['metadescription'] = "Selamat Datang di " . $data['getProfile']['catalog_title'];
                    $data['metaimage'] = asset('/images' . getData::getCatalogUsername(myFunction::get_username(), 'catalog_logo'));
                    return view('pages.close', $data);
                }
            } else {
                $url = explode("/", \Request::url());
                $redirect = explode('.', $_SERVER['HTTP_HOST'])[1] . '.id';
                return \Redirect::to('https://' . $redirect . '/register');
            }
        } else {
            return "Empty";
        }
    }
    public function menu($param1 = null, $param2 = null, $param3 = null)
    {
        $username = myFunction::get_username();
        if ($username == $param1) {
            $category = $param2;
            $subcategory = $param3;
        } else {
            $category = $param1;
            $subcategory = $param2;
        }
        $data['getProfile'] = Catalog::where('catalog_username', trim($username))->first();
        if ($data['getProfile']['show_catalog'] == 'Open') {
            $data['category'] = Category::where('category_slug', $category)->first();
            $data['subcategory'] = SubCategory::where('subcategory_slug', $subcategory)->first();
            $data['detail'] = CatalogDetail::select(
                'catalogdetail.*',
                'category.category_name',
                'category.category_slug',
                'subcategory.subcategory_slug',
                'subcategory.subcategory_name',
                'subcategory.subcategory_image',
                //'items.id',
                'items.items_name',
                'items.items_slug',
                'items.item_image_one',
                'items.item_image_two',
                'items.item_image_three',
                'items.item_image_four',
                'items.item_image_primary',
                'items.items_description',
                'items.items_price',
                'items.items_discount',
                'items.ready_stock'
            )
                ->leftJoin('category', 'catalogdetail.category_id', '=', 'category.id')
                ->leftJoin('subcategory', 'catalogdetail.subcategory_id', '=', 'subcategory.id')
                ->leftJoin('items', 'catalogdetail.item', '=', 'items.id')
                ->where('catalog_id', getData::getCatalogUsername(myFunction::get_username(),'id'))
                ->where('category_slug', $category)
                ->where('subcategory_slug', $subcategory)
                ->orderBy('item_position')
                ->get();
            $data['title'] = $data['category']['category_name'] . ' - ' . $data['subcategory']['subcategory_name'];
            $data['metadescription'] = $data['category']['category_name'] . ' - ' . $data['subcategory']['subcategory_name'];
            $data['metaimage'] = asset('/images/' . $data['subcategory']['subcategory_image']);
            return view('pages.menu', $data);
        } else {
            $data['title'] = $data['getProfile']['catalog_title'];
            $data['metadescription'] = "Selamat Datang di " . $data['getProfile']['catalog_title'];
            $data['metaimage'] = asset('/images' . getData::getCatalogUsername(myFunction::get_username(), 'catalog_logo'));
            return view('pages.close', $data);
        }
    }
    public function item($param1 = null, $param2 = null, $param3 = null, $param4 = null)
    {
        $username = myFunction::get_username();
        if ($username == $param1) {
            $category = $param2;
            $subcategory = $param3;
            $item = $param4;
        } else {
            $category = $param1;
            $subcategory = $param2;
            $item = $param3;
        }
        $data['getProfile'] = Catalog::where('catalog_username', trim($username))->first();
        if ($data['getProfile']['show_catalog'] == 'Open') {
            $data['category'] = Category::where('category_slug', $category)->first();
            $data['subcategory'] = SubCategory::where('subcategory_slug', $subcategory)->first();
            $data['item'] = Items::where('items_slug', $item)->first();
            $data['detail'] = CatalogDetail::select(
                'catalogdetail.*',
                'category.category_name',
                'category.category_slug',
                'subcategory.subcategory_slug',
                'subcategory.subcategory_name',
                'subcategory.subcategory_image',
                //'items.id',
                'items.items_name',
                'items.items_slug',
                'items.item_image_primary',
                'items.items_description',
                'items.items_price',
                'items.items_discount',
                'items.ready_stock'
            )
                ->leftJoin('category', 'catalogdetail.category_id', '=', 'category.id')
                ->leftJoin('subcategory', 'catalogdetail.subcategory_id', '=', 'subcategory.id')
                ->leftJoin('items', 'catalogdetail.item', '=', 'items.id')
                ->where('category_slug', $category)
                ->where('subcategory_slug', $subcategory)
                ->orderBy('item_position')
                ->get();
            $data['title'] = $data['category']['category_name'] . ' - ' . $data['subcategory']['subcategory_name'] . ' - ' . $data['item']['items_name'];
            $data['metadescription'] = $data['item']['items_description'];
            $data['metaimage'] = asset('/images/' . $data['item']['item_image_primary']);
            return view('pages.item', $data);
        } else {
            $data['title'] = $data['getProfile']['catalog_title'];
            $data['metadescription'] = "Selamat Datang di " . $data['getProfile']['catalog_title'];
            $data['metaimage'] = asset('/images' . getData::getCatalogUsername(myFunction::get_username(), 'catalog_logo'));
            return view('pages.close', $data);
        }
    }
    public function itemdirect($param1 = null, $param2 = null, $param3 = null)
    {
        $username = myFunction::get_username();
        if ($username == $param1) {
            $category = $param2;
            $item = $param3;
        } else {
            $category = $param1;
            $item = $param2;
        }
        $data['getProfile'] = Catalog::where('catalog_username', trim($username))->first();
        if ($data['getProfile']['show_catalog'] == 'Open') {
            $data['category'] = Category::where('category_slug', $category)->first();
            $data['item'] = Items::where('items_slug', $item)->first();
            $data['title'] = $data['category']['category_name'] . ' - ' . $data['item']['items_name'];
            $data['metadescription'] = $data['item']['items_description'];
            $data['metaimage'] = asset('/images/' . $data['item']['item_image_primary']);
            return view('pages.item', $data);
        } else {
            $data['title'] = $data['getProfile']['catalog_title'];
            $data['metadescription'] = "Selamat Datang di " . $data['getProfile']['catalog_title'];
            $data['metaimage'] = asset('/images' . getData::getCatalogUsername(myFunction::get_username(), 'catalog_logo'));
            return view('pages.close', $data);
        }
    }
    public function savecart(Request $request)
    {
        if (!Session::has('myorder')) {
            Session::put('myorder', date('YmdHis'));
        }
        $invoiceid = Session::get('myorder');
        if(getData::getCatalogUsername(myFunction::get_username(),'advance_payment') == 'Y'){
            if (Invoice::save_data($request, $invoiceid)) {
                //$this->sendNotifTransaction($invoiceid,'New Item','None');
                $status = 'success';
                $message = 'Your request was successful.';
            } else {
                $status = 'error';
                $message = 'Oh snap! something went wrong.';
            }
        }else{
            if(Invoice::save_data_last($request, $invoiceid)){
                //$this->sendNotifTransaction($invoiceid,'New Item','None');
                $status='success';
                $message='Your request was successful.';
            }else{
                $status='error';
                $message='Oh snap! something went wrong.';
            }
        }

        
        return ['status' => $status, 'message' => $message];
    }
    public function cartlist()
    {
        $inv = Session::get('myorder');
        if(!empty($inv)){
            $data['invoice'] = Invoice::where('invoice_number', $inv)->first();
            $data['item'] = InvoiceDetail::where('invoiceid', $data['invoice']['id'])
                ->groupBy('category')
                ->orderBy('id')
                ->get();
            return view('pages.cart', $data);
        }
    }
    public function cartpayment()
    {
        $data['invoice'] = Invoice::where('invoice_number', Session::get('myorder'))->first();
        $data['item'] = InvoiceDetail::where('invoiceid', $data['invoice']['id'])
            ->groupBy('category')
            ->orderBy('id')
            ->get();
        return view('pages.last_payment', $data);
    }
    public function confirmation(Request $request)
    {
      $inv = Session::get('myorder');
      if(!empty($inv)){
        $data['invoice'] = Invoice::where('invoice_number', $inv)->first();
        $data['item'] = InvoiceDetail::where('invoiceid', $data['invoice']['id'])
            ->groupBy('category')
            ->orderBy('id')
            ->get();
        $data['position']=$request->input('position');
        return view('pages.confirmation', $data);
      }
    }
    public function confirmationtransfer(Request $request)
    {
        $this->validate($request, [
            'transfer_image' => 'required|max:1000|mimes:jpeg,jpg,png'
        ]);
        if(Invoice::confirmation_data($request)){
            $status='success';
            $message='Your request was successful.';
        }else{
            $status='error';
            $message='Oh snap! something went wrong.';
        }
        return ['status' => $status, 'message' => $message];
    }
    public function countcart()
    {
        $inv = Session::get('myorder');
        if(!empty($inv)){
            $invoice = Invoice::where('invoice_number', $inv)->first();
            if(!empty($invoice)){
                $countitem = InvoiceDetail::where('invoiceid', $invoice['id'])->sum('qty');
                return $countitem;
            }else{
                return null;
            }
        }else{
            return null;
        }
    }
    public function checkout(Request $request)
    {
        if (Invoice::checkout_data($request)) {
            $this->sendNotifTransaction(Session::get('myorder'),'Checkout','Checkout');
            $status = 'success';
            $message = 'Your request was successful.';
        } else {
            $status = 'error';
            $message = 'Oh snap! something went wrong.';
        }
        return ['status' => $status, 'message' => $message];
    }
    public function completelastcheckout()
    {
        if (Invoice::completelastcheckout()) {
            $status = 'success';
            $message = 'Your request was successful.';
        } else {
            $status = 'error';
            $message = 'Oh snap! something went wrong.';
        }
        return ['status' => $status, 'message' => $message];
    }
    public function deleteitem($id = null)
    {
        if (InvoiceDetail::delete_item($id)) {
            $status = 'success';
            $message = 'Your request was successful.';
        } else {
            $status = 'error';
            $message = 'Oh snap! something went wrong.';
        }
        return ['status' => $status, 'message' => $message];
    }
    public function deleteaddon($detail = null,$group = null)
    {
        if (InvoiceAddons::delete_addon($detail,$group)) {
            $status = 'success';
            $message = 'Your request was successful.';
        } else {
            $status = 'error';
            $message = 'Oh snap! something went wrong.';
        }
        return ['status' => $status, 'message' => $message];
    }
    public function deletecart()
    {
        if (Invoice::delete_data()) {
            Session::forget('myorder');
            Session::forget('location');
            $status = 'success';
            $message = 'Your request was successful.';
        } else {
            $status = 'error';
            $message = 'Oh snap! something went wrong.';
        }
        return ['status' => $status, 'message' => $message];
    }
    public function show($id = null)
    {
        $query = InvoiceDetail::find($id);
        return $query;
    }
    public function updatecart(Request $request)
    {
        if (InvoiceDetail::update_data($request)) {
            $status = 'success';
            $message = 'Your request was successful.';
        } else {
            $status = 'error';
            $message = 'Oh snap! something went wrong.';
        }
        return ['status' => $status, 'message' => $message];
    }
    public function updatecartaddons(Request $request)
    {
        if (InvoiceAddons::update_data($request)) {
            $status = 'success';
            $message = 'Your request was successful.';
        } else {
            $status = 'error';
            $message = 'Oh snap! something went wrong.';
        }
        return ['status' => $status, 'message' => $message];
    }
    
    public function closesession()
    {
        Session::forget('myorder');
        Session::forget('location');
    }
    public function notif(Request $request)
    {
        return event(new NotifEvent(['invoice' => $request->input('invoice'), 'status' => $request->input('status'), 'note' => $request->input('note')]));
    }
    public function setlocation(Request $request,$lat, $lon)
    {
        if($request->isMethod('post')){
            if (Hash::check($request->input('password'), getData::getCatalogUsername(myFunction::get_username(),'catalog_password'))) {
                Session::put('location', ['lat' => $lat, 'lon' => $lon]);
                return true;
            }else{
                return false;
            }
        }else{
            Session::put('location', ['lat' => $lat, 'lon' => $lon]);
        }
    }
    public function setBell(Request $request)
    {
        if (Bell::call_cashier($request)) {
            $ch = curl_init();
            if(\Request::getHttpHost()=='localhost'){
                curl_setopt($ch, CURLOPT_URL, \URL::to('/cms/bellnotif'));
            }else{
                curl_setopt($ch, CURLOPT_URL, 'https://admin.yoscan.id/bellnotif');
            }
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, ['catalog' => getData::getCatalogUsername(myFunction::get_username(),'id'),'invoice' => $request->input('catalog'),'info' => "Panggilan", 'position' => $request->input('position')]);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            $output = curl_exec($ch);
            curl_close($ch);
        }
    }
    public function saveCustomer(Request $request)
    {
        $this->validate($request, [
            'customer_phone' => 'required|min:8|numeric'
        ]);
        $customer = Customer::where('catalog_id',trim($request->input('catalog')))->where('customer_phone',trim($request->input('customer_phone')))->first();
        if(empty($customer)){
            if (Customer::save_data($request));
            return "Registered";
        }else{
            if(empty($customer['customer_email'])){
                return "Registered";
            }else{
                return "Available";
            }
        }
    }
    public function updateCustomer(Request $request)
    {
        $this->validate($request, [
            'customer_name' => 'required|min:2',
            'customer_email' => 'required|email'
        ]);
        if (Customer::update_data($request));
        return true;
    }
    public function generateStruk($inv,$action)
    {
        $data['invoice'] = Invoice::where('invoice_number', $inv)->first();
        $data['item'] = InvoiceDetail::where('invoiceid', $data['invoice']['id'])
            ->groupBy('category')
            ->orderBy('id')
            ->get();
        $data['action']=$action;
        if($action == 'print'){
            return view('pages.struk', $data);
        }else{
            $temppath = public_path().'/images/struk/struk_'.time().'.png';
            $options = [
              'width' => 300,
              'height' => (!empty($customheight))?800:600,
              'quality' => 100
            ];
            $conv = new \Anam\PhantomMagick\Converter();
            $conv->addPage(view('pages.struk',$data))
            ->portrait()
            ->toPng($options)
            ->save($temppath);

            $img = Image::make($temppath);
            $img->fit(350,(!empty($customheight))?800:600);
            $img->save(public_path().'/images/struk/struk_'.$inv.'.png');
            unlink($temppath);
            return redirect(URL::to('/struk/struk_'.$inv.'.png'));
        }
    }
    public function addons($item,$group)
    {
        $data['addons'] = ItemsDetail::select('items_detail.*',
                            'category.category_name',
                            'items.items_name',
                            'items.items_price')
                            ->leftJoin('items','items_detail.addon','=','items.id')
                            ->leftJoin('category','items_detail.category_id','=','category.id')
                            ->where('items.item_type','Add')
                            ->where('items.ready_stock','Y')
                            ->where('item_id',$item)
                            ->groupBy('category_id')
                            ->get();
        $data['item']=$item;
        $data['group']=$group;
        return view('pages.addons', $data);
    }
    public function sendEmail(){
        $invoice = Invoice::where('id',4)->first(); // Jangan dibawa

        $item = InvoiceDetail::where('invoiceid', $invoice['id'])
            ->groupBy('category')
            ->orderBy('id')
            ->get();
        $disp = array(
            'email'=>getData::getCatalogUsername(myFunction::get_username(),'email_contact'),
            'name'=>getData::getCatalogUsername(myFunction::get_username(),'catalog_title'),
            'namapengirim'=>'YoScan Support',
            'emailpengirim'=>'support@yoscan.id',
            'subject'=>"Checkout Invoice ".$invoice['invoice_number']
        );
        $content = array(
            'invoice' => $invoice,
            'item' => $item
        );
        Mail::send('pages.email.transaction', $content, function($message) use ($disp)
        {
           $message->from($disp['emailpengirim'], $disp['namapengirim']);
           $message->to($disp['email'], $disp['name'])->subject($disp['subject']);
        });
    }
    public function sendNotifTransaction($param=null,$info=null,$status=null){
        $ch = curl_init();
        if(\Request::getHttpHost()=='localhost'){
            curl_setopt($ch, CURLOPT_URL, \URL::to('/cms/notif/'.getData::getCatalogUsername(myFunction::get_username(),'id') . '/' . $param . '/' . $info . '/' .$status));
        }else{
            curl_setopt($ch, CURLOPT_URL, 'https://admin.yoscan.id/notif/'. getData::getCatalogUsername(myFunction::get_username(),'id') . '/' . $param . '/' . $info . '/' .$status);
        }
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($ch);
        curl_close($ch);
    }
}