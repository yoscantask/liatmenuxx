<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Invoice;
use App\Models\InvoiceDetail;
use App\Models\InvoiceAddons;

use App\Helper\myFunction;
use App\Helper\getData;

use Veritrans;
use Midtrans;
use Session;
use Auth;

class PaymentController extends Controller
{
	public function __construct(){
	    Midtrans::$serverKey = getData::getCatalogUsername(myFunction::get_username(),'server_key');
	    Midtrans::$isProduction = false;
	}
	public function token($total) 
	{
		$inv = Session::get('myorder');
		$invoice = Invoice::where('invoice_number',$inv)->first();
		$itemdata = InvoiceDetail::where('invoiceid',$invoice['id'])->get();
    	$addondata = InvoiceAddons::where('invoiceid',$invoice['id'])
    	    							->groupBy('row_group')
    	    							->orderBy('id','desc')
    	    							->get();

		$array=[];
		foreach($itemdata as $data){
			$array[]=[
						'id'=>"item-".$data['item_id'],
						'price'=>($data['price']-$data['discount']),
						'quantity'=>$data['qty'],
						'name'=>$data['item']
					];
		}

		$addons=[];
		if(!empty($addondata)){
			foreach($addondata as $addondata){
				if((!empty($addondata['multiple_addon']) && !empty(getData::decodeAddons($addondata['single_addon'])))){
					$divid = ' | ';
				}else{
					$divid = '';
				}
				$addons[]=[
							'id'=>"addon-".$addondata['id'],
							'price'=>$addondata['addon_qty']*getData::addonsPrice($addondata['single_addon'],$addondata['multiple_addon']),
							'quantity'=>1,
							'name'=>"AddOns - ".getData::decodeAddons($addondata['single_addon']).$divid.getData::decodeAddons($addondata['multiple_addon'])
						];
			}
		}else{
			$addons[]=[];
		}

		$gettax= ($total * getData::getCatalogUsername(myFunction::get_username(),'tax'))/100;
		if($gettax > 0){
			$tax = [[
							'id'=>'Tax',
							'price'=>$gettax,
							'quantity'=>1,
							'name'=>"( Extra ) PPN ".getData::getCatalogUsername(myFunction::get_username(),'tax')."%"
						]];
		}else{
			$tax =[[
							'id'=>'Tax',
							'price'=>0,
							'quantity'=>1,
							'name'=>"PPN : 0%"
						]];;
		}
		$merge = array_merge($array,$addons,$tax);
	    $midtrans = new Midtrans;
	    $transaction_details = array(
	        'order_id'          => $invoice['invoice_number'].' - '.time(),
	        'gross_amount'  => $total + $gettax
	    );
	    // Populate items
	    $items = $merge;
	    // Populate customer's billing address
	    // $billing_address = array(
	    //     'first_name'        => "Andri",
	    //     'last_name'         => "Setiawan",
	    //     'address'           => "Karet Belakang 15A, Setiabudi.",
	    //     'city'                  => "Jakarta",
	    //     'postal_code'   => "51161",
	    //     'phone'                 => "081322311801",
	    //     'country_code'  => 'IDN'
	    // );

	    // Populate customer's shipping address
	    // $shipping_address = array(
	    //     'first_name'    => "John",
	    //     'last_name'     => "Watson",
	    //     'address'       => "Bakerstreet 221B.",
	    //     'city'              => "Jakarta",
	    //     'postal_code' => "51162",
	    //     'phone'             => "081322311801",
	    //     'country_code'=> 'IDN'
	    // );
	    // Populate customer's Info
	    $customer_details = array(
	        'first_name'            => "Sakti",
	        'last_name'             => "Yasin",
	        'email'                     => "saktie.banua@gmail.com",
	        'phone'                     => "082219155447",
	        //'billing_address' => $billing_address,
	        //'shipping_address'=> $shipping_address
	    );
	    // Data yang akan dikirim untuk request redirect_url.
	    $transaction_data = array(
	        'transaction_details'=> $transaction_details,
	        'item_details'           => $items,
	        //'customer_details'   => $customer_details
	    );
	    try
	    {
	        $snap_token = $midtrans->getSnapToken($transaction_data);
	        //return redirect($vtweb_url);
	        echo $snap_token;
	    } 
	    catch (Exception $e) 
	    {   
	        return $e->getMessage;
	    }
	}
	public function finishPayment(Request $request) 
	{
		if($request->isMethod('post')){
			$result = $request->input('result_data');
	        $result = json_decode($result,true);
	        if($result['status_code']=='400' or $result['status_code']=='401' or $result['status_code']=='402' or $result['status_code']=='404' or $result['status_code']=='406' or $result['status_code']=='410' or $result['status_code']=='412'){
	            $status = 'error';
	            $message = 'Pembayaran gagal.';
	        }else{
	        	if (Invoice::payment_gateway_data($request)) {
	        	    $ch = curl_init();
	        	    curl_setopt($ch, CURLOPT_URL, 'https://admin.liataja.id/notif/' . Session::get('myorder') . '/Checkout');
	        	    //curl_setopt($ch, CURLOPT_URL, \URL::to('/cms/notif/'.Session::get('mycart').'/Checkout'));
	        	    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
	        	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	        	    $output = curl_exec($ch);
	        	    curl_close($ch);

	        	    // Session::forget('myorder');
	        	    // Session::forget('location');
	        	    return redirect('/');
	        	    $status = 'success';
	        	    $message = 'Your request was successful.';
	        	} else {
	        	    $status = 'error';
	        	    $message = 'Oh snap! something went wrong.';
	        	}
	        }
			return ['status' => $status, 'message' => $message];
		}else{
			return "Ayee";
		}
	}
}