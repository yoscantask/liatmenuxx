<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\Catalog;

class ValidDomainMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // $get_domain = $request->server->get('SERVER_NAME');
        // $get_domain = $request->getHost();
        $get_domain = $request->getHttpHost();
        
        if($get_domain == 'localhost'){
            $catalog = Catalog::findOrFail(1);
        }
        else{
            $catalog = Catalog::where('custom_domain', trim($get_domain))->first();

            if(!$catalog){
                $get_domain = explode('.', $_SERVER['HTTP_HOST'])[0];
                $catalog = Catalog::where('catalog_username', trim($get_domain))->first();

                if(!$catalog){
                    return \Redirect::to(config('app.admin_domain'));
                    // abort(404);
                }
            }
        }
        
        $request->attributes->set('domain_name', $catalog->custom_domain);
        $request->attributes->set('sub_domain_name', $catalog->catalog_username);

        // Append domain and tenant to the Request object
        // for easy retrieval in the application.
        // $request->merge([
        //     'domain' => $domain,
        //     'tenant' => $tenant
        // ]);
        // // or
        // $request->attributes->set('customer_data', $data);
        
        // if ($tenant) {
        //     View::share('tenantColor', $tenant->color);
        //     View::share('tenantName', $tenant->name);
        // }

        return $next($request);
    }
}
