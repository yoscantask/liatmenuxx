<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use App\Http\Requests;

use App\Helper\myFunction;
use App\Helper\getData;

use App\Models\Invoice;
use App\Models\InvoiceAddons;
use App\Models\Items;
use App\Models\InvoiceRecipe;
use App\Models\Recipe;

use Image;
use Input;
use Auth;
use Session;

class InvoiceDetail extends Model
{
    protected $table = 'invoicedetail';

    public static function delete_item($id){
    	try {
    	    DB::transaction(function () use ($id) {
                $query = InvoiceDetail::where('id',$id)->first();
                $invoice = Invoice::where('id',$query['invoiceid'])->first();

                $ch = curl_init(); 
                curl_setopt($ch, CURLOPT_URL, \URL::to('/cms/notif/'.$invoice['invoice_number'].'/None'));
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
                $output = curl_exec($ch); 
                curl_close($ch);

                $addons = InvoiceAddons::where('invoicedetailid',$id)->get();
                foreach($addons as $vaddons){
                    InvoiceRecipe::where('reff_id',$vaddons['id'])->where('note_recipe','AddOn')->delete();
                }
                InvoiceDetail::where('id',$id)->delete();
                InvoiceRecipe::where('reff_id',$id)->where('note_recipe','Main')->delete();
    	    });
    	 }
    	catch(\Exception $e) {
    	    return false;
    	}
    	return true;
    }
    public static function update_data($request){
    	try {
    	    DB::transaction(function () use ($request) {
                $data=$request->all();
                $query = InvoiceDetail::where('id',$request->input('id'))->first();
                $invoice = Invoice::where('id',$query['invoiceid'])->first();

                $ch = curl_init(); 
                curl_setopt($ch, CURLOPT_URL, \URL::to('/cms/notif/'.$invoice['invoice_number'].'/None'));
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
                $output = curl_exec($ch); 
                curl_close($ch);

    	    	InvoiceDetail::where('id',$request->input('id'))->update([
                        'qty'=>$request->input('qty'),
                        'note'=>$request->input('note')
                ]);

                //Recipe Stock Items
                $recipeitem = Recipe::where('parent_id',$query['item_id'])->get();
                foreach($recipeitem as $vrecipeitem){
                    InvoiceRecipe::where('reff_id',$request->input('id'))->where('item_id',$vrecipeitem['item_id'])->where('note_recipe','Main')->delete();
                    $recipe=new InvoiceRecipe;
                    $recipe->id=myFunction::id('invoicerecipe','id');
                    $recipe->reff_id=$request->input('id');
                    $recipe->recipe_id=$vrecipeitem['id'];
                    $recipe->item_id=$vrecipeitem['item_id'];
                    $recipe->recipe_qty=$vrecipeitem['serving_size']*$request->input('qty');
                    $recipe->note_recipe="Main";
                    $recipe->save();
                }
                //End

    	    });
    	 }
    	catch(\Exception $e) {
    	    return false;
    	}
    	return true;
    }
}
