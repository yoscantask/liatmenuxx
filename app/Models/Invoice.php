<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use App\Http\Requests;

use App\Helper\myFunction;
use App\Helper\getData;

use App\Models\InvoiceDetail;
use App\Models\InvoiceAddons;
use App\Models\Items;
use App\Models\InvoiceRecipe;
use App\Models\Recipe;

use Image;
use Input;
use Auth;
use Session;
use Mail;

class Invoice extends Model
{
    protected $table = 'invoice';

    public static function save_data($request,$invoice){
        try {
            DB::transaction(function () use ($request,$invoice) {
                $data=$request->all();
                $getinvoice = Invoice::where('invoice_number',$invoice)->first();
                if(empty($getinvoice)){
                    $invoiceid = myFunction::id('invoice','id');
                    $var=new Invoice;
                    $var->id=$invoiceid;
                    $var->catalog_id=trim($data['catalog']);
                    $var->invoice_number=$invoice;
                    $var->via=trim($data['via']);
                    $var->status='Order';
                    if(getData::getCatalogUsername(myFunction::get_username(),'advance_payment') == 'N'){
                        $var->pending='Y';
                    }
                    $var->save();
                }else{
                    $invoiceid = $getinvoice['id'];
                }
                $getdetail = InvoiceDetail::where('invoiceid',$invoiceid)->where('item',$data['item'])->first();
                if(empty($getdetail)){
                    $iddetail = myFunction::id('invoicedetail','id');
                    $detail=new InvoiceDetail;
                    $detail->id=$iddetail;
                    $detail->invoiceid=$invoiceid;
                    $detail->category=$data['category'];
                    $detail->item_id=$data['item_id'];
                    $detail->item=$data['item'];
                    $detail->price=$data['price'];
                    $detail->discount=$data['discount'];
                    $detail->qty=$data['qty'];
                    $detail->note=trim($data['note']);
                    $detail->item_status='Order';
                    $detail->save();
                }else{
                    $iddetail = $getdetail['id'];
                    InvoiceDetail::where('id',$getdetail['id'])->update([
                                                                'qty'=>$getdetail['qty']+$data['qty'],
                                                                'note'=>(!empty($data['note']))?trim($data['note']):$getdetail['note']
                                                            ]);
                }

                //Recipe Stock Items
                $recipeitem = Recipe::where('parent_id',$data['item_id'])->get();
                foreach($recipeitem as $vrecipeitem){
                    $recipe=new InvoiceRecipe;
                    $recipe->id=myFunction::id('invoicerecipe','id');
                    $recipe->reff_id=$iddetail;
                    $recipe->recipe_id=$vrecipeitem['id'];
                    $recipe->item_id=$vrecipeitem['item_id'];
                    $recipe->recipe_qty=$vrecipeitem['serving_size']*$data['qty'];
                    $recipe->note_recipe="Main";
                    $recipe->save();
                }
                //End
                
                $grouprow=date('YmdHis');
                // Addons
                if(!empty($data['arraddonsmultiple']) || !empty($data['arraddonssingle'])){
                    $addonsid = myFunction::id('invoiceaddons','id');

                    $singleadd = [];
                    if(!empty($data['arraddonssingle'])){
                        foreach($data['arraddonssingle'] as $arraddonssingle){
                            //Recipe Stock Single Addons
                            $itemsingle = explode('-', $arraddonssingle)[1];
                            $recipeitemsingle = Recipe::where('parent_id',$itemsingle)->get();
                            foreach($recipeitemsingle as $vrecipeitemsingle){
                                $recipesingle=new InvoiceRecipe;
                                $recipesingle->id=myFunction::id('invoicerecipe','id');
                                $recipesingle->reff_id=$addonsid;
                                $recipesingle->recipe_id=$vrecipeitemsingle['id'];
                                $recipesingle->item_id=$vrecipeitemsingle['item_id'];
                                $recipesingle->recipe_qty=$vrecipeitemsingle['serving_size']*$data['qty'];
                                $recipesingle->note_recipe="AddOn";
                                $recipesingle->save();
                            }
                            //End
                            $singleadd[]=$arraddonssingle;
                        }
                        $singleaddons = implode(',',$singleadd);
                    }else{
                        $singleaddons = null;
                    }

                    $multipleadd = [];
                    if(!empty($data['arraddonsmultiple'])){
                        foreach($data['arraddonsmultiple'] as $arraddonsmultiple){
                            //Recipe Stock Multiple Addons
                            $itemmultiple = explode('-', $arraddonsmultiple)[1];
                            $recipeitemmultiple = Recipe::where('parent_id',$itemmultiple)->get();
                            foreach($recipeitemmultiple as $vrecipeitemmultiple){
                                $recipemultiple=new InvoiceRecipe;
                                $recipemultiple->id=myFunction::id('invoicerecipe','id');
                                $recipemultiple->reff_id=$addonsid;
                                $recipemultiple->recipe_id=$vrecipeitemmultiple['id'];
                                $recipemultiple->item_id=$vrecipeitemmultiple['item_id'];
                                $recipemultiple->recipe_qty=$vrecipeitemmultiple['serving_size']*$data['qty'];
                                $recipemultiple->note_recipe="AddOn";
                                $recipemultiple->save();
                            }
                            //End
                            $multipleadd[]=$arraddonsmultiple;
                        }
                        $multipleaddons = implode(',',$multipleadd);
                    }else{
                        $multipleaddons = null;
                    }

                    $check = InvoiceAddons::where('invoiceid',$invoiceid)
                                                    ->where('invoicedetailid',$iddetail)
                                                    ->where('single_addon',$singleaddons)
                                                    ->where('multiple_addon',$multipleaddons)
                                                    ->first();   
                    if(empty($check)){
                        $multiple=new InvoiceAddons;
                        $multiple->id=$addonsid;
                        $multiple->invoiceid=$invoiceid;
                        $multiple->invoicedetailid=$iddetail;
                        $multiple->row_group=$grouprow;
                        $multiple->single_addon=$singleaddons;
                        $multiple->multiple_addon=$multipleaddons;
                        $multiple->addon_qty=$data['qty'];
                        $multiple->save();
                    }else{
                        InvoiceAddons::where('row_group',$check['row_group'])->update(['addon_qty'=>$check['addon_qty']+$data['qty']]);
                    }
                }
                //End

            });
         }
        catch(\Exception $e) {
            return false;
        }
        return true;
    }
    public static function save_data_last($request,$invoice){
        try {
            DB::transaction(function () use ($request,$invoice) {
                $data=$request->all();
                $invoicedata = $invoice;
                $getinvoice = Invoice::where('invoice_number',$invoicedata)->first();
                if(empty($getinvoice)){
                    $invoiceid = myFunction::id('invoice','id');
                    $var=new Invoice;
                    $var->id=$invoiceid;
                    $var->catalog_id=trim($data['catalog']);
                    $var->invoice_number=$invoicedata;
                    $var->via=trim($data['via']);
                    $var->status='Order';
                    $var->pending='Y';
                    $var->invoice_type='Temporary';
                    $var->save();
                }else{
                    $invoiceid = $getinvoice['id'];
                }

                for($i=0;$i<$data['qty'];$i++){
                    $iddetail = myFunction::id('invoicedetail','id');
                    $detail=new InvoiceDetail;
                    $detail->id=$iddetail;
                    $detail->invoiceid=$invoiceid;
                    $detail->category=$data['category'];
                    $detail->item_id=$data['item_id'];
                    $detail->item=$data['item'];
                    $detail->price=$data['price'];
                    $detail->discount=$data['discount'];
                    $detail->qty=1;
                    $detail->note=trim($data['note']);
                    $detail->item_status='Order';
                    $detail->save();

                    //Recipe Stock Items
                    $recipeitem = Recipe::where('parent_id',$data['item_id'])->get();
                    foreach($recipeitem as $vrecipeitem){
                        $recipe=new InvoiceRecipe;
                        $recipe->id=myFunction::id('invoicerecipe','id');
                        $recipe->reff_id=$iddetail;
                        $recipe->recipe_id=$vrecipeitem['id'];
                        $recipe->item_id=$vrecipeitem['item_id'];
                        $recipe->recipe_qty=$vrecipeitem['serving_size']*1;
                        $recipe->note_recipe="Main";
                        $recipe->save();
                    }
                    //End

                    $grouprow=date('YmdHis');
                    // Addons
                    if(!empty($data['arraddonsmultiple']) || !empty($data['arraddonssingle'])){
                        $addonsid = myFunction::id('invoiceaddons','id');

                        $singleadd = [];
                        if(!empty($data['arraddonssingle'])){
                            foreach($data['arraddonssingle'] as $arraddonssingle){
                                //Recipe Stock Single Addons
                                $itemsingle = explode('-', $arraddonssingle)[1];
                                $recipeitemsingle = Recipe::where('parent_id',$itemsingle)->get();
                                foreach($recipeitemsingle as $vrecipeitemsingle){
                                    $recipesingle=new InvoiceRecipe;
                                    $recipesingle->id=myFunction::id('invoicerecipe','id');
                                    $recipesingle->reff_id=$addonsid;
                                    $recipesingle->recipe_id=$vrecipeitemsingle['id'];
                                    $recipesingle->item_id=$vrecipeitemsingle['item_id'];
                                    $recipesingle->recipe_qty=$vrecipeitemsingle['serving_size']*1;
                                    $recipesingle->note_recipe="AddOn";
                                    $recipesingle->save();
                                }
                                //End
                                $singleadd[]=$arraddonssingle;
                            }
                            $singleaddons = implode(',',$singleadd);
                        }else{
                            $singleaddons = null;
                        }

                        $multipleadd = [];
                        if(!empty($data['arraddonsmultiple'])){
                            foreach($data['arraddonsmultiple'] as $arraddonsmultiple){
                                //Recipe Stock Multiple Addons
                                $itemmultiple = explode('-', $arraddonsmultiple)[1];
                                $recipeitemmultiple = Recipe::where('parent_id',$itemmultiple)->get();
                                foreach($recipeitemmultiple as $vrecipeitemmultiple){
                                    $recipemultiple=new InvoiceRecipe;
                                    $recipemultiple->id=myFunction::id('invoicerecipe','id');
                                    $recipemultiple->reff_id=$addonsid;
                                    $recipemultiple->recipe_id=$vrecipeitemmultiple['id'];
                                    $recipemultiple->item_id=$vrecipeitemmultiple['item_id'];
                                    $recipemultiple->recipe_qty=$vrecipeitemmultiple['serving_size']*1;
                                    $recipemultiple->note_recipe="AddOn";
                                    $recipemultiple->save();
                                }
                                //End
                                $multipleadd[]=$arraddonsmultiple;
                            }
                            $multipleaddons = implode(',',$multipleadd);
                        }else{
                            $multipleaddons = null;
                        }

                        $check = InvoiceAddons::where('invoiceid',$invoiceid)
                                                        ->where('invoicedetailid',$iddetail)
                                                        ->where('single_addon',$singleaddons)
                                                        ->where('multiple_addon',$multipleaddons)
                                                        ->first();   
                        if(empty($check)){
                            $multiple=new InvoiceAddons;
                            $multiple->id=$addonsid;
                            $multiple->invoiceid=$invoiceid;
                            $multiple->invoicedetailid=$iddetail;
                            $multiple->row_group=$grouprow;
                            $multiple->single_addon=$singleaddons;
                            $multiple->multiple_addon=$multipleaddons;
                            $multiple->addon_qty=1;
                            $multiple->save();
                        }else{
                            InvoiceAddons::where('row_group',$check['row_group'])->update(['addon_qty'=>$check['addon_qty']+1]);
                        }
                    }
                    //End
                }
            });
         }
        catch(\Exception $e) {
            return false;
        }
        return true;
    }
    public static function checkout_data($request){
        try {
            DB::transaction(function () use ($request) {
                $numinvoice = getData::generateInvoice();
                if(getData::getCatalogUsername(myFunction::get_username(),'advance_payment') == 'Y'){
                    Invoice::where('invoice_number',Session::get('myorder'))->update([
                        'invoice_number' => $numinvoice,
                        'status'=>'Checkout',
                        'position'=>($request->has('position'))?$request->input('position'):$request->input('result_position'),
                        'payment_method'=>1,
                        'tax'=>$request->input('tax'),
                        //'amount'=>$request->input('amount')+(($request->input('amount')*$request->input('tax'))/100),
                    ]);
                    Session::forget('myorder');
                    Session::put('myorder', $numinvoice);
                    $invoice = Invoice::where('invoice_number',$numinvoice)->first();
                }else{
                    $inv = Session::get('myorder');
                    Invoice::where('invoice_number',$inv)->update([
                        'invoice_number' => $numinvoice,
                        'status'=>'Checkout',
                        'position'=>($request->has('position'))?$request->input('position'):$request->input('result_position'),
                        'payment_method'=>'',
                        'tax'=>$request->input('tax'),
                    ]);
                    Session::forget('myorder');
                    Session::put('myorder', $numinvoice);
                    $invoice = Invoice::where('invoice_number',$numinvoice)->first();
                }
                InvoiceDetail::where('invoiceid',$invoice['id'])->update(['item_status'=>'Checkout']);

                //Send Email
                // $item = InvoiceDetail::where('invoiceid', $invoice['id'])
                //     ->groupBy('category')
                //     ->orderBy('id')
                //     ->get();
                // $disp = array(
                //     'email'=>getData::getCatalogUsername(myFunction::get_username(),'email_contact'),
                //     'name'=>getData::getCatalogUsername(myFunction::get_username(),'catalog_title'),
                //     'namapengirim'=>'LiatMenu Support',
                //     'emailpengirim'=>'support@liatmenu.id',
                //     'subject'=>"Order Invoice ".$invoice['invoice_number']
                // );
                // $content = array(
                //     'invoice' => $invoice,
                //     'item' => $item
                // );
                // Mail::send('pages.email.transaction', $content, function($message) use ($disp)
                // {
                //   $message->from($disp['emailpengirim'], $disp['namapengirim']);
                //   $message->to($disp['email'], $disp['name'])->subject($disp['subject']);
                // });
                //End Email
                
                Invoice::sendNotif($invoice['invoice_number'],'Checkout',$invoice['position']);
            });
         }
        catch(\Exception $e) {
            return false;
        }
        return true;
    }
    public static function completelastcheckout(){
        try {
            DB::transaction(function () {
                $inv = Session::get('myorder');
                Invoice::where('invoice_number',$inv)->update(['payment_method'=>1]);
                $invoice = Invoice::where('invoice_number',$inv)->first();

                Invoice::sendNotif($invoice['invoice_number'],'Payment Info',$invoice['position']);

                //Send Email
                $item = InvoiceDetail::where('invoiceid', $invoice['id'])
                    ->groupBy('category')
                    ->orderBy('id')
                    ->get();
                $disp = array(
                    'email'=>getData::getCatalogUsername(myFunction::get_username(),'email_contact'),
                    'name'=>getData::getCatalogUsername(myFunction::get_username(),'catalog_title'),
                    'namapengirim'=>'LiatMenu Support',
                    'emailpengirim'=>'support@liatmenu.id',
                    'subject'=>"Order Invoice ".$invoice['invoice_number']
                );
                $content = array(
                    'invoice' => $invoice,
                    'item' => $item
                );
                Mail::send('pages.email.transaction', $content, function($message) use ($disp)
                {
                   $message->from($disp['emailpengirim'], $disp['namapengirim']);
                   $message->to($disp['email'], $disp['name'])->subject($disp['subject']);
                });
                //End Email
            });
         }
        catch(\Exception $e) {
            return false;
        }
        return true;
    }
    public static function confirmation_data($request){
        try {
            DB::transaction(function () use ($request) {
                
                $imageName = time().'.'.$request->transfer_image->getClientOriginalExtension();
                $request->transfer_image->move(public_path('images/confirmation'), $imageName);
                $urlimage = url('/').'/images/confirmation/'.$imageName;

                if(getData::getCatalogUsername(myFunction::get_username(),'advance_payment') == 'Y'){
                    $invoice = Invoice::where('invoice_number',Session::get('myorder'))->first();
                    Invoice::where('invoice_number',Session::get('myorder'))->update([
                                    'status'=>'Checkout',
                                    'position'=>$request->input('position'),
                                    'transfer_image'=>$urlimage,
                                    'payment_method'=>2,
                                    'tax'=>$request->input('tax'),
                                ]);
                }else{
                    $inv = Session::get('myorder');
                    $payment_method = 2;
                    Invoice::where('invoice_number',$inv)->update([
                                'position'=>$request->input('position'),
                                'transfer_image'=>$urlimage,
                                'payment_method'=>$payment_method,
                                'tax'=>$request->input('tax'),
                            ]);
                    $invoice = Invoice::where('invoice_number',$inv)->first();
                }
                Invoice::sendNotif($invoice['invoice_number'],'Payment Info',$invoice['position']);
                //Send Email
                $item = InvoiceDetail::where('invoiceid', $invoice['id'])
                    ->groupBy('category')
                    ->orderBy('id')
                    ->get();
                $disp = array(
                    'email'=>getData::getCatalogUsername(myFunction::get_username(),'email_contact'),
                    'name'=>getData::getCatalogUsername(myFunction::get_username(),'catalog_title'),
                    'namapengirim'=>'LiatMenu Support',
                    'emailpengirim'=>'support@liatmenu.id',
                    'subject'=>"Order Invoice ".$invoice['invoice_number']
                );
                $content = array(
                    'invoice' => $invoice,
                    'item' => $item
                );
                Mail::send('pages.email.transaction', $content, function($message) use ($disp)
                {
                   $message->from($disp['emailpengirim'], $disp['namapengirim']);
                   $message->to($disp['email'], $disp['name'])->subject($disp['subject']);
                });
                //End Email
            });
         }
        catch(\Exception $e) {
            return false;
        }
        return true;
    }
    public static function payment_gateway_data($request){
        try {
            DB::transaction(function () use ($request) {
                if(getData::getCatalogUsername(myFunction::get_username(),'advance_payment') == 'Y'){
                    $invoice = Invoice::where('invoice_number',Session::get('myorder'))->first();
                    Invoice::where('invoice_number',Session::get('myorder'))->update([
                        'status'=>'Checkout',
                        'position'=>($request->has('position'))?$request->input('position'):$request->input('result_position'),
                        'payment_method'=>3,
                        'tax'=>$request->input('result_tax'),
                    ]);
                }else{
                    $inv = Session::get('myorder');
                    $payment_method = 3;
                    Invoice::where('invoice_number',$inv)->update([
                        'position'=>($request->has('position'))?$request->input('position'):$request->input('result_position'),
                        'payment_method'=>$payment_method,
                        'tax'=>$request->input('result_tax'),
                        'pending'=>'N',
                    ]);
                    $invoice = Invoice::where('invoice_number',$inv)->first();
                    $invoicedetail = InvoiceDetail::where('invoiceid',$invoice['id'])->get();
                    foreach($invoicedetail as $value){
                        $item = Items::where('id',$value['item_id'])->first();
                        Items::where('id',$value['item_id'])->update(['sell'=>$item['sell']+$value['qty']]);
                    }
                }
                Invoice::sendNotif($invoice['invoice_number'],'Payment Info',$invoice['position']);
                //Send Email
                $item = InvoiceDetail::where('invoiceid', $invoice['id'])
                    ->groupBy('category')
                    ->orderBy('id')
                    ->get();
                $disp = array(
                    'email'=>getData::getCatalogUsername(myFunction::get_username(),'email_contact'),
                    'name'=>getData::getCatalogUsername(myFunction::get_username(),'catalog_title'),
                    'namapengirim'=>'LiatMenu Support',
                    'emailpengirim'=>'support@liatmenu.id',
                    'subject'=>"Order Invoice ".$invoice['invoice_number']
                );
                $content = array(
                    'invoice' => $invoice,
                    'item' => $item
                );
                Mail::send('pages.email.transaction', $content, function($message) use ($disp)
                {
                   $message->from($disp['emailpengirim'], $disp['namapengirim']);
                   $message->to($disp['email'], $disp['name'])->subject($disp['subject']);
                });
                //End Email
            });
         }
        catch(\Exception $e) {
            return false;
        }
        return true;
    }
    public static function delete_data(){
        try {
            DB::transaction(function () {
                $invoice = Invoice::where('invoice_number',Session::get('myorder'))->first();
                $invdetail = InvoiceDetail::where('invoiceid',$invoice['id'])->get();
                foreach($invdetail as $vinvdetail){
                    $addons = InvoiceAddons::where('invoicedetailid',$vinvdetail['id'])->get();
                    foreach($addons as $vaddons){
                        InvoiceRecipe::where('reff_id',$vaddons['id'])->where('note_recipe','AddOn')->delete();
                    }
                    InvoiceDetail::where('id',$vinvdetail['id'])->delete();
                    InvoiceRecipe::where('reff_id',$vinvdetail['id'])->where('note_recipe','Main')->delete();
                }
                Invoice::where('invoice_number',Session::get('myorder'))->delete();
            });
         }
        catch(\Exception $e) {
            return false;
        }
        return true;
    }
    public static function sendNotif($invoice,$info,$position){
        $ch = curl_init();
        if(\Request::getHttpHost()=='localhost'){
            curl_setopt($ch, CURLOPT_URL, \URL::to('/cms/newtransaction'));
        }else{
            curl_setopt($ch, CURLOPT_URL, 'https://admin.yoscan.id/newtransaction');
        }
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, ['catalog' => getData::getCatalogUsername(myFunction::get_username(),'id'), 'invoice' => $invoice, 'info' => $info, 'position' => $position]);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($ch);
        curl_close($ch);
    }
}
