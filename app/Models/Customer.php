<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use App\Http\Requests;

use Input;
use Auth;
use Session;
use getData;
use myFunction;

class Customer extends Model
{
    protected $table = 'customer';

    public static function save_data($request){
        try {
            DB::transaction(function () use ($request) {
                $data=$request->all();
                $var=new Customer;
                $var->id=myFunction::id('customer','id');
                $var->catalog_id=trim($data['catalog']);
                $var->customer_phone=trim($data['customer_phone']);
                $var->save();
            });
         }
        catch(\Exception $e) {
            return false;
        }
        return true;
    }
    public static function update_data($request){
        try {
            DB::transaction(function () use ($request) {
                $data=$request->all();
                Customer::where('catalog_id',trim($data['catalog']))
                        ->where('customer_phone',trim($data['customer_phone']))
                        ->update(['customer_name'=>$data['customer_name'],'customer_email'=>$data['customer_email']]);
            });
         }
        catch(\Exception $e) {
            return false;
        }
        return true;
    }
}
