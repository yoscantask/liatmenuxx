<?php
namespace App\Helper;
use Illuminate\Support\Facades\DB;
use App\User;
use App\Models\Sliders;
use App\Models\Catalog;
use App\Models\CatalogDetail;
use App\Models\Category;
use App\Models\SubCategory;
use App\Models\Items;
use App\Models\ItemsDetail;
use App\Models\Invoice;
use App\Models\InvoiceDetail;
use App\Models\InvoiceAddons;

use Auth;
use Session;
use myFunction;

class getData{
	public static function getUser($id=null,$field=null){
		$query = User::where('id',$id)->first();
		return $query[$field];
	}
	public static function getCategory($id=null,$field=null){
		$query = Category::where('id',$id)->first();
		return $query[$field];
	}
	public static function getItem($id=null,$field=null){
		$query = Items::where('id',$id)->first();
		return $query[$field];
	}
	public static function getCatalogUsername($username=null,$field=null){
		$query = Catalog::where('catalog_username',$username)->first();
		return $query[$field];
	}
	public static function getCatalog($id=null,$field=null){
		$query = Catalog::where('id',$id)->first();
		return $query[$field];
	}
	public static function getSubCategory($id=null,$field=null){
		$query = SubCategory::where('id',$id)->first();
		return $query[$field];
	}
	public static function getSlider($id=null,$field=null){
		$query = Sliders::where('id',$id)->first();
		return $query[$field];
	}
	public static function checkSliderCatalog($sliderid=null,$id=null){
		$query = Catalog::where('id',$id)->first();
		$array = json_decode($query['sliders'],true);
		if(count((array)$array) > 0){
			foreach($array as $value){
				if($value == $sliderid){
					return true;
					break;
				}
			}
		}else{
			return false;
		}
	}
	public static function getCatalogSubCategory($catalog=null,$category=null){
		$query = CatalogDetail::select('catalogdetail.*',
		'category.category_name',
		'subcategory.subcategory_slug',
		'subcategory.subcategory_name',
		'subcategory.subcategory_image'
		)
		->leftJoin('category','catalogdetail.category_id','=','category.id')
		->leftJoin('subcategory','catalogdetail.subcategory_id','=','subcategory.id')
		->where('catalog_id',$catalog)
		->where('category_id',$category)
		->orderBy('subcategory_position')
		->groupBy('subcategory_id')
		->get();
		return $query;
	}
	public static function getCatalogItems($catalog=null,$category=null,$subcategory=null){
		$query = CatalogDetail::select('catalogdetail.*',
		'category.category_name',
		'category.category_slug',
		'subcategory.subcategory_name',
		'subcategory.subcategory_slug',
		//'items.id',
		'items.items_name',
		'items.items_slug',
		'items.item_image_one',
		'items.item_image_two',
		'items.item_image_three',
		'items.item_image_four',
		'items.item_image_primary',
		'items.items_description',
		'items.ready_stock',
		'items.items_price',
		'items.items_discount'
		)
		->leftJoin('category','catalogdetail.category_id','=','category.id')
		->leftJoin('subcategory','catalogdetail.subcategory_id','=','subcategory.id')
		->leftJoin('items','catalogdetail.item','=','items.id')
		->where('catalog_id',$catalog)
		->where('category_id',$category)
		->where('subcategory_id',$subcategory)
		->orderBy('item_position')
		->get();
		return $query;
	}
	public static function getCatalogSubCategoryItems($catalog=null,$category=null,$subcategory=null){
		$query = CatalogDetail::select('catalogdetail.*',
		'category.category_name',
		'subcategory.subcategory_name',
		//'items.id',
		'items.items_name',
		'items.ready_stock',
		)
		->leftJoin('category','catalogdetail.category_id','=','category.id')
		->leftJoin('subcategory','catalogdetail.subcategory_id','=','subcategory.id')
		->leftJoin('items','catalogdetail.item','=','items.id')
		->where('catalog_id',$catalog)
		->where('category_id',$category)
		->where('subcategory_id',$subcategory)
		->orderBy('item_position')
		->get();
		return $query;
	}
	public static function getTotalItems($catalog,$subcategory){
		$query = CatalogDetail::where('catalog_id',$catalog)->where('subcategory_id',$subcategory)->count();
		return $query;
	}
	public static function getItemCart($category=null){
		$inv = Session::get('myorder');
		if(!empty($inv)){
			$invoice = Invoice::where('invoice_number',$inv)->first();
			$query = InvoiceDetail::where('category',$category)
									->where('invoiceid',$invoice['id'])
									->orderBy('item_id')
									->get();
			return $query;
		}
	}
	public static function getItemCartInvoice($invoice=null,$category=null){
		$query = InvoiceDetail::where('category',$category)
								->where('invoiceid',$invoice)
								->orderBy('item_id')
								->get();
		return $query;
	}
	public static function getItemCartStruk($inv=null,$category=null){
		$invoice = Invoice::where('invoice_number',$inv)->first();
		$query = InvoiceDetail::where('category',$category)->where('invoiceid',$invoice['id'])->orderBy('id')->get();
		return $query;
	}
	public static function getInvoice($field){
		$inv = Session::get('myorder');
		if(!empty($inv)){
			$query = Invoice::where('invoice_number',$inv)->first();
			if(!empty($query)){
				return $query[$field];
			}
		}
	}
	public static function generateInvoice(){
		$query = Invoice::select('invoice.*','catalog.*')
						->leftJoin('catalog','invoice.catalog_id','catalog.id')
						->where('status','<>','Order')
						->where('catalog_username',myFunction::get_username())
						->count();

		if(getData::getCatalogUsername(myFunction::get_username(),'advance_payment') == 'Y'){
            $code='INV';
        }else{
            $code='ORD';
        }

		if (!empty($query)) {
		   $invoice = $code.getData::getCatalogUsername(myFunction::get_username(),'id').'-'.str_pad($query + 1, 8, "0", STR_PAD_LEFT);
		} else {
		   $invoice = $code.getData::getCatalogUsername(myFunction::get_username(),'id').'-'.str_repeat(0,7).'1';
		}
		return $invoice;
	}
	public static function getAddons($item){
		$query = ItemsDetail::select('items_detail.*',
		                'category.category_name',
		                'items.items_name',
		                'items.items_price')
		                ->leftJoin('items','items_detail.addon','=','items.id')
		                ->leftJoin('category','items_detail.category_id','=','category.id')
		                ->where('items.item_type','Add')
		                ->where('items.ready_stock','Y')
		                ->where('item_id',$item)
		                ->groupBy('category_id')
		                ->get();
		return $query;
	}
	public static function getItemAddons($item=null,$category=null){
		$query = ItemsDetail::select('items_detail.*',
    											'category.category_name',
    											'items.items_name',
    											'items.items_price'
    									)
    									->leftJoin('category','items_detail.category_id','=','category.id')
    									->leftJoin('items','items_detail.addon','=','items.id')
    									->where('item_id',$item)
    									->where('category_id',$category)
    									->get();
		return $query;
	}
	public static function getInvoiceAddons($detail=null){
		$query = InvoiceAddons::where('invoicedetailid',$detail)
    							->groupBy('row_group')
    							->orderBy('id','desc')
    							->get();
		return $query;
	}
	public static function getInvoiceAddonsSum($detail=null){
		$query = InvoiceAddons::where('invoicedetailid',$detail)
    							->sum('addon_qty');
		return $query;
	}
	public static function getInvoiceAddonsQty($group=null){
		$query = InvoiceAddons::where('row_group',$group)->groupBy('row_group')->first();
		return $query['qty_group'];
	}
	public static function getInvoiceAddonsPrice($group=null){
		$query = InvoiceAddons::where('row_group',$group)->get();
		$price = 0;
		foreach ($query as $key => $value) {
			$price = $price + ($value['addon_qty']*$value['addon_price']);
		}
		return $price;
	}
	public static function getAddOnByGroup($group){
		$query = InvoiceAddons::select('invoiceaddons.*',
    								'items.items_name',
    								'category.category_name',
    							)
    							->leftJoin('category','invoiceaddons.category_id','=','category.id')
    							->leftJoin('items','invoiceaddons.addon_id','=','items.id')
    							->where('row_group',$group)
    							->groupBy('category_id')
    							->orderBy('addon_price')
    							->get();
		return $query;
	}
	public static function getAddOnByCategory($group,$category){
		$query = InvoiceAddons::select('invoiceaddons.*',
    								'items.items_name',
    								'category.category_name',
    							)
    							->leftJoin('category','invoiceaddons.category_id','=','category.id')
    							->leftJoin('items','invoiceaddons.addon_id','=','items.id')
    							->where('row_group',$group)
    							->where('category_id',$category)
    							->groupBy('addon_id')
    							->get();
    	$data=[];
    	foreach ($query as $key => $value) {
    		$data[]=$value['items_name'];
    	}
		return implode(', ',$data);
	}
	public static function checkAddSingle($group,$category,$addon,$price){
		$maingroup = InvoiceAddons::where('row_group',$group)->first();
		$single = $category.'-'.$addon.'-'.$price;
		foreach(explode(',', $maingroup['single_addon']) as $val){
			if($val == $single){
				return true;
			}
		}
	}
	public static function checkAddMultiple($group,$category,$addon,$price){
		$maingroup = InvoiceAddons::where('row_group',$group)->first();
		$multiple = $category.'-'.$addon.'-'.$price;
		foreach(explode(',', $maingroup['multiple_addon']) as $val){
			if($val == $multiple){
				return true;
			}
		}
	}
	public static function decodeAddons($param){
		if(!empty($param)){
			$array=explode(',', $param);
			$data=[];
			foreach($array as $val){
				$category = explode('-', $val)[0];
				$addon = explode('-', $val)[1];
				$price = explode('-', $val)[2];
				$data[]=getData::getCategory($category,'category_name').' : '.getData::getItem($addon,'items_name');
			}
			return implode(', ', $data);
		}
	}
	public static function addonsPrice($single,$multiple){
		
		$pricesingle=0;
		if(!empty($single)){
			$arraysingle=explode(',', $single);
			foreach($arraysingle as $valsingle){
				$pricesingle=$pricesingle+explode('-', $valsingle)[2];
			}
		}

		$pricemultiple=0;
		if(!empty($multiple)){
			$arraymultiple=explode(',', $multiple);
			foreach($arraymultiple as $valmultiple){
				$pricemultiple=$pricemultiple+explode('-', $valmultiple)[2];
			}
		}
		
		return $pricesingle+$pricemultiple;
	}
}
