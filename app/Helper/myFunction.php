<?php
namespace App\Helper;
use Illuminate\Support\Facades\DB;
use Auth;
use App\User;
use File;
use Carbon\Carbon;

class myFunction{
    public static function id($table, $field)
    {
        $data = DB::table($table)->max($field);
        return $data + 1;
    }
    public static function getAssets(){
        // if(!empty($_SERVER['HTTP_HOST'])){
        //  $hostName = $_SERVER['HTTP_HOST']; 
            
        //  $url=$hostName.dirname($_SERVER['PHP_SELF']); 
        //  return $url;
        // }
        if(!empty($_SERVER['HTTP_HOST'])){
            $hostName = $_SERVER['HTTP_HOST']; 
            $protocol = strtolower(substr($_SERVER["SERVER_PROTOCOL"],0,5))=='https://'?'https://':'http://';
            $url=$protocol.$hostName.dirname($_SERVER['PHP_SELF']); 
            return $url;
        }
    }
    public static function baseURL(){
        
        // $getslash = explode("/",dirname($_SERVER['PHP_SELF']));
        // if(!empty($getslash[1])){
        //     $baseurl=$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF']);
        // }else{
        //     $baseurl=$_SERVER['HTTP_HOST'];
        // }
        
        // return $baseurl;
        $protocol = strtolower(substr($_SERVER["SERVER_PROTOCOL"],0,5))=='https://'?'https://':'http://';
        $getslash = explode("/",dirname($_SERVER['PHP_SELF']));
        if(!empty($getslash[1])){
            $baseurl=$protocol.$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF']);
        }else{
            $baseurl=$protocol.$_SERVER['HTTP_HOST'];
        }
        
        return $baseurl;
    }
    public static function validurl($url){
        if (!preg_match("/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i",$url)) {
            $valid = 'false';
        }else{
            $valid = 'true';
        }
        return $valid;
    }
    public static function loadPathAsset(){
        if(\Request::getHttpHost()=='localhost' || \Request::getHttpHost()=='192.168.100.132'){
            $path='../sources/images/'.Auth::user()->id;
        }else{
            $path='../sources/images/'.Auth::user()->id;
        }
        return $path;
    }
    public static function pathAsset(){
        $path=realpath(dirname('../../'))."/public/images";
        // if(\Request::getHttpHost()=='localhost'){
        //     $path=realpath(dirname('../../'))."/sources/images";
        // }else{
        //     $path=realpath(dirname('../../'))."/public/images";
        // }
        return $path;
    }
    public static function getProtocol(){
        $protocol = strtolower(substr($_SERVER["SERVER_PROTOCOL"],0,5))=='https'?'https':'http';
        if(\Request::getHttpHost()=='localhost' || \Request::getHttpHost()=='192.168.100.132'){
            return $protocol.'://'.\Request::getHost()."/gampangjualan/liataja/images";
        }else{
            return $protocol.'://'.\Request::getHost().'/images';
        }
    }
    public static function getPhotoProfile(){
        $mainpath = myFunction::pathAsset();
        $subpath = 'users/'.Auth::user()->id.'/images/profil_pict';
        $path = $mainpath.'/'.$subpath;
        $query = User::where('id',Auth::user()->id)->first();
        $filephoto = basename(parse_url($query['profil_pict'])['path']);
        if(File::exists($path.'/'.basename(parse_url($filephoto)['path']))){
            $photo = str_replace('gampangjualan.my.id','gampangjualan.id',myFunction::getProtocol().'/'.$subpath.'/'.$filephoto);
        }else{
            $photo = $query['profil_pict'];
        }
        return $photo.'?'.time();
    }
    public static function generateCodeDate($date,$id){
        if($date==null){
            return "-";
        }else{
            $fulldate=explode(" ",$date);
            $mydate=substr($date,8,2);
            $month=substr($date,5,2);
            $year=substr($date,0,4);
            return str_pad($id, 8, "0",  STR_PAD_LEFT);
            //return $mydate.''.$month.''.$year.'-'.str_pad($id, 8, "0",  STR_PAD_LEFT);
        }
    }
    public static function thumb_youtube($v)
    {
        $thumb = 'https://img.youtube.com/vi/' . $v . '/0.jpg';
        return $thumb;
    }
    public static function check_diff_login()
    {
        $datework = Carbon::parse(\Auth::user()->created_at);
        $now = Carbon::now();
        return $datework->diffInDays($now);
    }
    public static function get_username()
    {
        // $getslash = explode("/",\Request::url());
        // if(\Request::getHttpHost()=='localhost' || \Request::getHttpHost()=='192.168.100.132'){
        //     return 'ceudedeh';
        //     //return $getslash[5];
        // }else{
        //     return explode('.', $_SERVER['HTTP_HOST'])[0];
        // }

        $get_domain = \Request::getHttpHost();
        
        if($get_domain == 'localhost'){
            return 'ceudedeh';
        }
        else{
            $catalog = \App\Models\Catalog::where('custom_domain', trim($get_domain))->first();

            if(!$catalog){
                $get_domain = explode('.', $_SERVER['HTTP_HOST'])[0];
                $catalog = \App\Models\Catalog::where('catalog_username', trim($get_domain))->first();
            }

            return $catalog->catalog_username;
        }
    }
    public static function payment_type($type)
    {
        switch($type){
            case 1:
                return "Cash";
                break;
            case 2:
                return "Bank Transfer";
                break;
            case 3:
                return "Payment Gateway";
                break;
        }
    }
}