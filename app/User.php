<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use App\Http\Requests;

use App\Helper\myFunction;
use App\Helper\getData;

use App\Models\Package;
use App\Models\Register;

use Image;
use Input;
use Auth;
use Session;
use Hash;

use Carbon\Carbon;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public static function save_data($request){
    	try {
    	    DB::transaction(function () use ($request) {
    	    	$data=$request->all();
                $userid = myFunction::id('users','id');
                $username = explode('@',trim($data['email']));
                $var=new User;
                $var->id=$userid;
                $var->username=$username[0];
                $var->name=trim($data['name']);
                $var->password=Hash::make(trim($data['password']));
                $var->email=trim($data['email']);
                $var->phone=trim($data['phone']);
                $var->level='Member';
                $var->active='Y';
                $var->owner=1;
                $var->save();

                $package = Package::where('id',$data['package'])->first();
                $reg=new Register;
                $reg->id=myFunction::id('register','id');
                $reg->user_id=$userid;
                $reg->package_id=trim($data['package']);
                $reg->package_name=$package['package_name'];
                $reg->price=$package['price'];
                $reg->confirmation='Y';
                $reg->status='Approved';
                $reg->affiliate=$data['affiliate'];
                $reg->expired=Carbon::now()->addMonths($package['duration'])->toDateString();
                $reg->save();

    	    });
    	 }
    	catch(\Exception $e) {
    	    return false;
    	}
    	return true;
    }
}
